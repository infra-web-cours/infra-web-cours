<?php
$_config['trombi']['rgpd']['ip']=array(
	'157.159',
	'137.194',
);
$_config['trombi']['protocole']='3';
$_config['trombi']['host']='ldap.int-evry.fr';
$_config['trombi']['port']='389';
$_config['trombi']['DN_people']='ou=People,dc=int-evry,dc=fr';
$_config['trombi']['DN_structure']='ou=Structures,dc=int-evry,dc=fr';
$_config['trombi']['ldapUser']='cn=crmp,ou=system,dc=int-evry,dc=fr';
$_config['trombi']['ldapPassword']='labmm_ldap';
$_config['trombi']['photo']['url']='trombi.tem-tsp.eu/photo.php?uid=%s';
$_config['trombi']['photo']['url']='trombi.imtbs-tsp.eu/photo.php?uid=%s';
$_config['trombi']['photo']['width']='62';
$_config['trombi']['photo']['height']='80';

$_config['trombi']['miniCV']['url']='http://cvtheque.tem-tsp.eu';

$_config['trombi']['filtres']['service']='(eduPersonOrgUnitDN=%s)';
$_config['trombi']['filtres']['personnel']='(|(employeetype=permanent*)(employeetype=mascotte*)(employeetype=apprenti*)(employeetype=postdoc*))';
$_config['trombi']['filtres']['enseignant']='(eduPersonAffiliation=faculty)';
$_config['trombi']['filtres']['administratif']='(&(|(employeetype=permanent*)(employeetype=mascotte*)(employeetype=apprenti*)(employeetype=postdoc*))(!(eduPersonAffiliation=faculty)))';
$_config['trombi']['filtres']['administratif2']='(&(|(employeetype=permanent*)(employeetype=mascotte*)(employeetype=apprenti*))(!(eduPersonAffiliation=faculty)))';
$_config['trombi']['filtres']['postdoc']='(employeetype=postdoc*)';
$_config['trombi']['filtres']['thesard']='(employeetype=thesard*)';
$_config['trombi']['filtres']['stagiaire']='(employeetype=stagiaire)';
$_config['trombi']['filtres']['vacataire']='(employeetype=vacataire*)';

$_config['trombi']['filtres']['actif']='(&(!(employeeType=sortant))(!(intepersuseretat=sortant)))';

$_config['trombi']['champs']['people']=array(
	'supannlisterouge',
	'uid',
	'mail',
	'displayname',
	'cn',
	'givenname',
	'departmentnumber',
	'businesscategory',
	'telephonenumber',
	'roomnumber',
	'l',
	'labeleduri',
	'supannactivite',
	'sn',
	'givenName',
	'employeetype'
);
$_config['trombi']['champs']['structure']=array(
	'sn',
	'title',
	'manager',
	'secretary',
	'dn',
	'description'
);
$_lang['fr']['trombi_titre']='Annuaire';
$_lang['en']['trombi_titre']='Directory';
$_lang['fr']['trombi_discipline']='Discipline :';
$_lang['en']['trombi_discipline']='Discipline:';
$_lang['fr']['trombi_telephone']='T&eacute;l&eacute;phone :';
$_lang['en']['trombi_telephone']='Phone number:';
$_lang['fr']['trombi_bureau']='Bureau :';
$_lang['en']['trombi_bureau']='Office:';
$_lang['fr']['trombi_site']='Site web :';
$_lang['en']['trombi_site']='Web site:';
$_lang['fr']['trombi_minicv']='mini CV';
$_lang['en']['trombi_minicv']='mini CV';
$_lang['fr']['trombi_titre_secretariat']='Secr&eacute;tariat';
$_lang['en']['trombi_titre_secretariat']='Secretary';
$_lang['fr']['trombi_titre_permanent']='Membres de l\'&eacute;quipe';
$_lang['en']['trombi_titre_permanent']='Team\'s members';

$_lang['fr']['trombi_titre_enseignant']='Enseignants chercheurs';
$_lang['en']['trombi_titre_enseignant']='Faculty';
$_lang['fr']['trombi_titre_administratif']='Administratifs et non permanents';
$_lang['en']['trombi_titre_administratif']='Administrative and non permanents';
$_lang['fr']['trombi_titre_administratif_ARTEMIS']='Ingénieurs de recherche';
$_lang['en']['trombi_titre_administratif_ARTEMIS']='Research engineers';
$_lang['fr']['trombi_titre_postdoc']='Post-docs';
$_lang['en']['trombi_titre_postdoc']='Post Doctoral Fellows';

$_lang['fr']['trombi_titre_thesards']='Th&eacute;sards';
$_lang['en']['trombi_titre_thesards']='PhD Students';
$_lang['fr']['trombi_titre_stagiaires']='Stagiaires';
$_lang['en']['trombi_titre_stagiaires']='Internships';

?>
