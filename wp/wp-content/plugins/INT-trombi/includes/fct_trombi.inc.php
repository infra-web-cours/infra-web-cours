<?php
function ldapConnect($_config=null){
	if($_config==null){
		Global $_config;
	}
	if($ds=@ldap_connect($_config["trombi"]["host"],$_config["trombi"]["port"])){
		ldap_set_option($ds, LDAP_OPT_PROTOCOL_VERSION, $_config["trombi"]["protocole"]);
		return $ds;
	}
	return false;
}
function ldapBind($ds,$user,$pwd){
	return @ldap_bind($ds,$user,$pwd); 
}

function ldapSortPeople($a,$b){
	if($a["sn"]>$b["sn"])return 1;
	if($a["sn"]<$b["sn"])return -1;
	if($a["givenname"]>$b["givenname"])return 1;
	return -1;
}

function ldapGetInfoDpt($ds,$dpt,$_config=null){
	if($_config==null){
		Global $_config;
	}
	$service=array();
	$filtre="(sn=".$dpt.")";
	$sr=ldap_search($ds,$_config["trombi"]["DN_structure"],$filtre);
	$info=ldap_get_entries($ds, $sr);
	$service["sn"]=$info[0]["sn"][0];
	$service["title"]=$info[0]["title"][0];
	$service["dn"]=$info[0]["dn"];
	$service["description"]=htmlentities(utf8_decode($info[0]["description"][0]));
	for($j=0;$j<$info[0]["secretary"]["count"];$j++){
		if(!in_array($info[0]["secretary"][$j],$info[0]["manager"]) && trim($info[0]["secretary"][$j])!="") $service["secretary"][]=preg_replace("/uid=(.*),ou=people,dc=int-evry,dc=fr/i","$1",$info[0]["secretary"][$j]);
	}
	for($j=0;$j<$info[0]["manager"]["count"];$j++){
		if(trim($info[0]["manager"][$j])!="")$service["manager"][]=preg_replace("/uid=(.*),ou=people,dc=int-evry,dc=fr/i","$1",$info[0]["manager"][$j]);
	}
	return $service;
}
function ldapGetPeople($ds,$filtre,$_config=null){
	if($_config==null){
		Global $_config;
	}
	$people=array();
	$sr=ldap_search($ds,$_config["trombi"]["DN_people"], $filtre,$_config["trombi"]["champs"]["people"]);
	$info = ldap_get_entries($ds, $sr);
	for($indInfo=0;$indInfo<$info["count"];$indInfo++){
		for($indChamp=0;$indChamp<count($_config["trombi"]["champs"]["people"]);$indChamp++){
			$champ=$_config["trombi"]["champs"]["people"][$indChamp];
			if($info[$indInfo][$champ]["count"]<=1)$people[$info[$indInfo]["uid"][0]][$champ]=$info[$indInfo][$champ][0];
			else {
				$people[$info[$indInfo]["uid"][0]][$champ]=$info[$indInfo][$champ];
				unset($people[$info[$indInfo]["uid"][0]][$champ]["count"]);
			}
		}
		unset($info[$indInfo]);
	}
	uasort($people,"ldapSortPeople");
	return $people;
}

function ldapFormatTel($tel){
	$patterns=array(
		'/ /'							=> '',
		'/^0/'							=> '+33',
		'/^(\+33?)(\d{1})(\d{2})(\d{2})(\d{2})(\d{2})$/'	=> '$1 $2 $3 $4 $5 $6',
	);
	$tel=preg_replace('/ /', '', $tel);
	if(strlen($tel)>9){
		return preg_replace(array_keys($patterns), array_values($patterns), $tel);
	}
	else return '-';
}

function ldapFormatSalle($bureau, $lieu=''){
	if(preg_match("/ABCDE/i",$bureau) || $bureau=="") return "-";
	else{
	 $bureau=mb_strtoupper($bureau);
	 $bureau=preg_replace("/([A-Z]*)([0-9\-]*)/","$1 $2",$bureau).(!empty($lieu)?' ('.$lieu.')':'');
	 return $bureau;
	}
}

function prepareHTML($str){
	return htmlentities($str,ENT_COMPAT,'UTF-8');
}

function testMiniCV($url){
	$ch=curl_init();
	curl_setopt($ch,CURLOPT_URL,$url);
	curl_setopt($ch,CURLOPT_NOBODY,true);
	curl_exec($ch);
	$status=curl_getinfo($ch,CURLINFO_HTTP_CODE);
	curl_close($ch);
	if($status=="404") return false;
}
function testVisible($user, $ips){
	if(strcasecmp($user['supannlisterouge'], 'false')===0){
		return true;
	}
	foreach((array)$ips as $ip){
		$pattern='/^'.preg_quote($ip).'\./';
		if(preg_match($pattern, $_SERVER['REMOTE_ADDR'])){
			return true;
		}
	}
	return false;
}
function ldapFiche($user,$type="",$_config=null,$_lang=null,$mini_cv=array()){
	if($_config==null){
		Global $_config;
	}
	if($_lang==null){
		Global $_lang;
	}
	$visible=testVisible($user, $_config['trombi']['rgpd']['ip']);
	if(!$visible){
		return;
	}
	$contenu="";
	$contenu.='<div class="ldapFiche">';
	$contenu.='<div class="ldapPhoto">';
	$protocole=($_SERVER["HTTPS"] == "on"?'https':'http');
	$contenu.='<img src="'.$protocole.'://'.sprintf($_config['trombi']['photo']['url'],$user["uid"]).'" border="0" width="'.$_config['trombi']['photo']['width'].'" height="'.$_config['trombi']['photo']['height'].'" />';
	$contenu.='</div>';
	$contenu.='<div class="ldapNom">'.prepareHTML($user["displayname"]).'</div>';
	$contenu.='<div class="ldapInfo">';
  $mmm = str_replace("@", "_at_", $user["mail"]);
	$contenu.='<p><a href="mailto:'.$mmm.'">'.$mmm.'</a></p>';
	if($type!="stagiaire" && $type!="thesard"){
		if(!is_array($user["businesscategory"]))$contenu.='<p>'.prepareHTML($user["businesscategory"]).'</p>';
		else{
			$contenu.='<ul>';
			for($indBC=0;$indBC<count($user["businesscategory"]);$indBC++) $contenu.='<li>'.prepareHTML($user["businesscategory"][$indBC]).'</li>';
			$contenu.='</ul>';
		}
	}
	if($user["supannactivite"]!='')$contenu.='<p><span class="ldapTitre">'.$_lang[$_config["lang"]]["trombi_discipline"].'</span> '.prepareHTML($user["supannactivite"]).'</p>';
	if($user["telephonenumber"]!='')$contenu.='<p><span class="ldapTitre">'.$_lang[$_config["lang"]]["trombi_telephone"].'</span> '.ldapFormatTel($user["telephonenumber"]).'</p>';
	if($user["roomnumber"]!='')$contenu.='<p><span class="ldapTitre">'.$_lang[$_config["lang"]]["trombi_bureau"].'</span> '.ldapFormatSalle($user["roomnumber"], $user['l']).'</p>';
	if(isset($mini_cv[$user['uid']]))
		$cv=$mini_cv[$user['uid']];
	elseif(isset($mini_cv[strtolower($user['cn'])]))
		$cv=$mini_cv[strtolower($user['cn'])];
	elseif(isset($mini_cv[strtolower($user['sn'])]))
		$cv=$mini_cv[strtolower($user['sn'])];
	else
		$cv='';
	if($cv!=''){
		$contenu.='<p><span class="ldapMiniCV"><a href="'.$cv.'" target="_blank">'.$_lang[$_config["lang"]]["trombi_minicv"].'</a></span></p>';
	}
/*	$miniCV=$_config["miniCV"].$user["uid"].'.html';
	if(file_exists($miniCV))$contenu.='<p><span class="ldapMiniCV"><a href="'.$miniCV.'" target="_blank">'.$_lang[$_config["lang"]]["trombi_minicv"].'</a></span></p>';
	elseif($_config["urlMiniCV"]!=''){
		$miniCV=$_config["urlMiniCV"].$user["uid"].'.html';
		if(testMiniCV($miniCV))$contenu.='<p><span class="ldapMiniCV"><a href="'.$_config["urlMiniCV"].$user["uid"].'.html'.'" target="_blank">'.$_lang[$_config["lang"]]["trombi_minicv"].'</a></span></p>';
	}
*/	
	if($user["labeleduri"]!='' && !preg_match('/www.it-sudparis.eu/',$user["labeleduri"]))$contenu.='<p><span class="ldapTitre">'.$_lang[$_config["lang"]]["trombi_site"].'</span> <a href="'.$user["labeleduri"].'" target="_blank">'.$user["labeleduri"].'</a></p>';
	$contenu.='</div>';
	$contenu.='</div>'."\n";
	return $contenu;
}
function wsGetListeCv($url,$dpt){
	$cvs=array();
	$urlListe=$url.'/liste.php?dpt='.strtolower($dpt);
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, $urlListe);
	curl_setopt($ch, CURLOPT_POST, true);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	$reponse=curl_exec($ch);
	if(curl_errno($ch)==CURLE_OK){
		$xml = new DOMDocument();
		$xml->preserveWhiteSpace=false;
		$xml->loadXML($reponse);
		$cvXml=$xml->getElementsByTagName('cv');
		for($i=0;$i<$cvXml->length;$i++){
			$user=preg_replace('/^(.*).htm(l?)$/','$1',$cvXml->item($i)->nodeValue);
			$user=strtolower($user);
			$cvs[$user]=$url.'/'.strtolower($dpt).'/'.$cvXml->item($i)->nodeValue;
		}
	}
	curl_close($ch);
	return $cvs;
}
?>
