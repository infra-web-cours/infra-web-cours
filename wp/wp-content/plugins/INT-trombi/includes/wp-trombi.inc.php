<?php
include(plugin_dir_path( __FILE__ ).'config_trombi.inc.php');
include_once(plugin_dir_path( __FILE__ ).'fct_trombi.inc.php');
$contenu='<div id="trombi">';
$_config["affiche"]["manager"]=(isset($_config["affiche"]["manager"])?$_config["affiche"]["manager"]:true);
$_config["affiche"]["secretary"]=(isset($_config["affiche"]["secretary"])?$_config["affiche"]["secretary"]:true);
$_config["affiche"]["personnel"]=(isset($_config["affiche"]["personnel"])?$_config["affiche"]["personnel"]:true);
$_config["affiche"]["enseignant"]=(isset($_config["affiche"]["enseignant"])?$_config["affiche"]["enseignant"]:false);
$_config["affiche"]["administratif"]=(isset($_config["affiche"]["administratif"])?$_config["affiche"]["administratif"]:false);
$_config["affiche"]["postdoc"]=(isset($_config["affiche"]["postdoc"])?$_config["affiche"]["postdoc"]:false);
$_config["affiche"]["thesard"]=(isset($_config["affiche"]["thesard"])?$_config["affiche"]["thesard"]:true);
$_config["affiche"]["stagiaire"]=(isset($_config["affiche"]["stagiaire"])?$_config["affiche"]["stagiaire"]:true);
$_config["affiche"]["miniCV"]=(isset($_config["affiche"]["miniCV"])?$_config["affiche"]["miniCV"]:false);

$joker=($_config["sousArbo"]?'*':'');

$ldapFiltres=array();
$ldapFiltres["personnel"]="(&".$_config['trombi']['filtres']['service'].$_config["trombi"]["filtres"]["personnel"]."%s)";
$ldapFiltres["enseignant"]="(&".$_config['trombi']['filtres']['service'].$_config["trombi"]["filtres"]["enseignant"]."%s)";
$ldapFiltres["administratif"]="(&".$_config['trombi']['filtres']['service'].$_config["trombi"]["filtres"]["administratif"]."%s)";
$ldapFiltres["administratif2"]="(&".$_config['trombi']['filtres']['service'].$_config["trombi"]["filtres"]["administratif2"]."%s)";
$ldapFiltres["postdoc"]="(&".$_config['trombi']['filtres']['service'].$_config["trombi"]["filtres"]["postdoc"]."%s)";
$ldapFiltres["thesard"]="(&".$_config['trombi']['filtres']['service'].$_config["trombi"]["filtres"]["thesard"]."%s)";
$ldapFiltres["stagiaire"]="(&".$_config['trombi']['filtres']['service'].$_config["trombi"]["filtres"]["stagiaire"]."%s)";

$miniCVs=array();
if($_config["affiche"]["miniCV"]){
	$miniCVs=wsGetListeCv($_config['trombi']['miniCV']['url'],$_config["service"]);
}

if($ldapDS=ldapConnect($_config)){
	if(ldapBind($ldapDS,$_config["trombi"]["ldapUser"],$_config["trombi"]["ldapPassword"])){
		$ldapFiltreManager="";
		$ldapFiltreSecretary="";
		$ldapFiltreNot=$_config['trombi']['filtres']['actif'];
		$ldapService=ldapGetInfoDpt($ldapDS,$_config["service"],$_config);
		if($_config["affiche"]["manager"] && count((array)$ldapService["manager"])>0){
			for($indManager=0;$indManager<count((array)$ldapService["manager"]);$indManager++){
				$ldapFiltreManager.="(uid=".$ldapService["manager"][$indManager].")";
			}
			$ldapFiltreManager=(count($ldapService["manager"])>1?"(|".$ldapFiltreManager.")":$ldapFiltreManager);
			$ldapEquipe=ldapGetPeople($ldapDS,$ldapFiltreManager,$_config);
			while(list($kEquipe,$vEquipe)=each($ldapEquipe))$contenu.=ldapFiche($vEquipe,"",$_config,$_lang,$miniCVs);
		}
		if($_config["affiche"]["secretary"] && count((array)$ldapService["secretary"])>0){
			for($indSecretary=0;$indSecretary<count($ldapService["secretary"]);$indSecretary++){
				$ldapFiltreSecretary.="(uid=".$ldapService["secretary"][$indSecretary].")";
			}
			$ldapFiltreSecretary=(count($ldapService["secretary"])>1?"(|".$ldapFiltreSecretary.")":$ldapFiltreSecretary);
			$ldapEquipe=ldapGetPeople($ldapDS,$ldapFiltreSecretary,$_config);
			while(list($kEquipe,$vEquipe)=each($ldapEquipe))$contenu.=ldapFiche($vEquipe,"",$_config,$_lang,$miniCVs);
		}
		
		$ldapFiltreNot.=($ldapFiltreManager!=''?'(!'.$ldapFiltreManager.')':'');
		$ldapFiltreNot.=($ldapFiltreSecretary!=''?'(!'.$ldapFiltreSecretary.')':'');

		if($_config["affiche"]["personnel"]){
			$ldapFiltre=sprintf($ldapFiltres["personnel"],$joker.$ldapService["dn"],$ldapFiltreNot);
			$ldapEquipe=ldapGetPeople($ldapDS,$ldapFiltre,$_config);
			if(count($ldapEquipe)>0)$contenu.='<h3>'.$_lang[$_config["lang"]]["trombi_titre_permanent"].'</h3>';
			while(list($kEquipe,$vEquipe)=each($ldapEquipe))$contenu.=ldapFiche($vEquipe,"",$_config,$_lang,$miniCVs);
		}
		else{
			if($_config["affiche"]["enseignant"]){
				$ldapFiltre=sprintf($ldapFiltres["enseignant"],$joker.$ldapService["dn"],$ldapFiltreNot);
				$ldapEquipe=ldapGetPeople($ldapDS,$ldapFiltre,$_config);
				if(count($ldapEquipe)>0)$contenu.='<h3>'.$_lang[$_config["lang"]]["trombi_titre_enseignant"].'</h3>';
				while(list($kEquipe,$vEquipe)=each($ldapEquipe))$contenu.=ldapFiche($vEquipe,"",$_config,$_lang,$miniCVs);
			}
			if($_config["affiche"]["postdoc"]){
				$ldapFiltre=sprintf($ldapFiltres["postdoc"],$joker.$ldapService["dn"],$ldapFiltreNot);
				$ldapEquipe=ldapGetPeople($ldapDS,$ldapFiltre,$_config);
				if(count($ldapEquipe)>0)$contenu.='<h3>'.$_lang[$_config["lang"]]["trombi_titre_postdoc"].'</h3>';
				while(list($kEquipe,$vEquipe)=each($ldapEquipe))$contenu.=ldapFiche($vEquipe,"",$_config,$_lang,$miniCVs);
			}
			if($_config["affiche"]["administratif"]){
				if($_config["affiche"]["postdoc"]){
					$ldapFiltre=sprintf($ldapFiltres["administratif2"],$joker.$ldapService["dn"],$ldapFiltreNot,$miniCVs);
				}
				else{
					$ldapFiltre=sprintf($ldapFiltres["administratif"],$joker.$ldapService["dn"],$ldapFiltreNot,$miniCVs);
				}
				$ldapEquipe=ldapGetPeople($ldapDS,$ldapFiltre,$_config);
				if(count($ldapEquipe)>0)$contenu.='<h3>'.$_lang[$_config["lang"]][($_config["service"]=="ARTEMIS"?"trombi_titre_administratif_ARTEMIS":"trombi_titre_administratif")].'</h3>';
				while(list($kEquipe,$vEquipe)=each($ldapEquipe))$contenu.=ldapFiche($vEquipe,"",$_config,$_lang,$miniCVs);
			}
		}
		if($_config["affiche"]["thesard"]){
			$ldapFiltre=sprintf($ldapFiltres["thesard"],$joker.$ldapService["dn"],'');
			$ldapEquipe=ldapGetPeople($ldapDS,$ldapFiltre,$_config);
			if(count($ldapEquipe)>0)$contenu.='<h3>'.$_lang[$_config["lang"]]["trombi_titre_thesards"].'</h3>';
			while(list($kEquipe,$vEquipe)=each($ldapEquipe))$contenu.=ldapFiche($vEquipe,"thesard",$_config,$_lang,$miniCVs);
		}

		if($_config["affiche"]["stagiaire"]){
			$ldapFiltre=sprintf($ldapFiltres["stagiaire"],$joker.$ldapService["dn"],'');
			$ldapEquipe=ldapGetPeople($ldapDS,$ldapFiltre,$_config);
			if(count($ldapEquipe)>0)$contenu.='<h3>'.$_lang[$_config["lang"]]["trombi_titre_stagiaires"].'</h3>';
			while(list($kEquipe,$vEquipe)=each($ldapEquipe))$contenu.=ldapFiche($vEquipe,"stagiaire",$_config,$_lang,$miniCVs);
		}
		unset($ldapEquipe);
		unset($ldapService);
	}
	else echo ldap_error($ldapDS);
}
$contenu.='</div>';
//echo $contenu;
?>
