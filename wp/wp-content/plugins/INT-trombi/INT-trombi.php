<?php
/*
Plugin Name: INT-trombi
Description: Affiche le trombinoscope d'un département sur une page ou un article.
Version: 1.3
Author: Eric Bourhis
*/

function INT_trombi_admin_head(){
	global $typenow;
	// check user permissions
	if (!current_user_can('edit_posts') && !current_user_can('edit_pages')){
		return;
	}
	// verify the post type
	if(!in_array($typenow, array('post', 'page'))){
		return;
	}
	// check if WYSIWYG is enabled
	if ( get_user_option('rich_editing') == 'true') {
		add_filter('mce_external_plugins', 'INT_trombi_mce_external_plugins');
		add_filter('mce_buttons', 'INT_trombi_mce_buttons');
	}
}
add_action('admin_head', 'INT_trombi_admin_head');
function INT_trombi_mce_external_plugins($plugin_array){
	$plugin_array['INT_trombi_button'] = plugins_url( '/js/INT-trombi.js', __FILE__ );
	return $plugin_array;
}
function INT_trombi_mce_buttons($buttons) {
	array_push($buttons, 'INT_trombi_button');
	return $buttons;
}

function INT_trombi_wp_enqueue_scripts(){
	wp_enqueue_style( 'INT-trombi-style', plugins_url('css/styles.css', __FILE__) );
}
add_action( 'wp_enqueue_scripts','INT_trombi_wp_enqueue_scripts');
add_action( 'admin_enqueue_scripts','INT_trombi_wp_enqueue_scripts');

function INT_trombi_shortcode($atts){
	global $post;
	extract(shortcode_atts( array(
		'service' => null,
		'dpt' => null,
		'lg' => 'fr',
		'personnel' => 1,
		'manager' => 1,
		'secretary' => 1,
		'thesard' => 1,
		'stagiaire' => 1,
		'enseignant' => 0,
		'administratif' => 0,
		'postdoc' => 0,
		'minicv' => 0,
		'arbo' => 0,
		'maj' =>false
	), $atts));
	$_config['service']=(!is_null($service)?$service:$dpt);
	$_config['sousArbo']=$arbo==1;
	$_config['affiche']['personnel']=$personnel==1;
	$_config['affiche']['manager']=$manager==1;
	$_config['affiche']['secretary']=$secretary==1;
	$_config['affiche']['thesard']=$thesard==1;
	$_config['affiche']['stagiaire']=$stagiaire==1;
	$_config['affiche']['enseignant']=$enseignant==1;
	$_config['affiche']['administratif']=$administratif==1;
	$_config['affiche']['postdoc']=$postdoc==1;
	$_config['affiche']['miniCV']=$minicv==1;
	$_config['lang']=$lg;
	include (plugin_dir_path( __FILE__ ).'includes/wp-trombi.inc.php');
	if(!$maj)$post->TSP_plugin=true;
	return $contenu;
}
add_shortcode('trombi', 'INT_trombi_shortcode');
?>
