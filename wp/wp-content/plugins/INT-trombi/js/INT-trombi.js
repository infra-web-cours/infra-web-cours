(function() {
	tinymce.PluginManager.add('INT_trombi_button', function( editor, url ) {
		editor.addButton( 'INT_trombi_button', {
			title: 'Trombinoscope',
			icon: 'icon INT-trombi-button',
			onclick: function() {
				editor.windowManager.open( {
					title: 'Insérer un trombinoscope',
					body: [
					{
						type: 'listbox',
						name: 'dpt',
						label: 'Département* : ',
						values: [
							{text: '',		value: ''},
							{text: '-- TSP --',	value: ''},
							{text: 'ARTEMIS',	value: 'ARTEMIS'},
							{text: 'CITI',		value: 'CITI'},
							{text: 'EPH',		value: 'EPH'},
							{text: 'INF',		value: 'INF'},
							{text: 'LOR',		value: 'LOR'},
							{text: 'RS2M',		value: 'RS2M'},
							{text: 'RST',		value: 'RST'},
							{text: '-- TEM --',	value: ''},
							{text: 'DEFI',		value: 'DEFI'},
							{text: 'DSI',		value: 'DSI'},
							{text: 'LSH',		value: 'LSH'},
							{text: 'MMS',		value: 'MMS'}]
					},
					{
						type: 'listbox',
						name: 'lg',
						label: 'Langue d\'affichage : ',
						values: [
							{text: 'Français',		value: 'fr'},
							{text: 'Anglais',		value: 'en'}]
					},
					{
						type: 'checkbox',
						name: 'manager',
						label: 'Placer le directeur en tête de liste',
					},
					{
						type: 'checkbox',
						name: 'secretary',
						label: 'Placer le secrétariat en tête de liste',
					},
					{
						type: 'container',
						html: '<strong>- Affichage du personnel -</strong>',
					},
					{
						type: 'checkbox',
						name: 'personnel',
						label: 'Tout le personnel (CDD, CDI, post-doc)',
					},
					{
						type: 'container',
						html: '<strong>ou</strong>',
					},
					{
						type: 'checkbox',
						name: 'enseignant',
						label: 'Les enseignants-chercheurs',
					},
					{
						type: 'checkbox',
						name: 'administratif',
						label: 'Le personnel administratif et techniques',
					},
					{
						type: 'checkbox',
						name: 'postdoc',
						label: 'Les post-docs',
					},
					{
						type: 'checkbox',
						name: 'thesard',
						label: 'Les thésards',
					},
					{
						type: 'checkbox',
						name: 'stagiaire',
						label: 'Les stagiaires',
					},
					{
						type: 'container',
						html: '<strong>-- Options --</strong>',
					},
					{
						type: 'checkbox',
						name: 'minicv',
						label: 'Afficher les mini CV (s\'ils existent)',
					},
					{
						type: 'checkbox',
						name: 'arbo',
						label: 'Afficher les sous-services (s\'ils existent)',
					},
					],
					onsubmit: function(e){
						var param='';
						if(e.data.dpt==''){
							alert('Merci de sélectionner un département');
							return false;
						}
						if(e.data.manager===true) e.data.manager=1;
						if(e.data.secretary===true) e.data.secretary=1;
						if(e.data.personnel===true) e.data.personnel=1;
						if(e.data.enseignant===true) e.data.enseignant=1;
						if(e.data.administratif===true) e.data.administratif=1;
						if(e.data.postdoc===true) e.data.postdoc=1;
						if(e.data.thesard===true) e.data.thesard=1;
						if(e.data.stagiaire===true) e.data.stagiaire=1;
						if(e.data.minicv===true) e.data.minicv=1;
						if(e.data.arbo===true) e.data.arbo=1;
						if(e.data.maj===true) e.data.maj=1;
						for(var i in e.data){
							if(e.data[i]!=''){
								param+=' '+i+'="'+e.data[i]+'"';
							}
						}
						editor.insertContent( '[trombi' + param + ']');
					}
				});
			}
		});
	});
})();
