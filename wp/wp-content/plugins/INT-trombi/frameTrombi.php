<html>
<head>
<script type="text/javascript">
/* <![CDATA[ */
function genereCodeTrombi(){
	var trombi_service=document.getElementById("trombi_service").value;
	var win = window.dialogArguments || opener || parent || top;
	var trombi_lang=document.getElementById("trombi_lang").value;
	var trombi_aff=new Array();
	trombi_aff["personnel"]=document.getElementById("trombi_personnel").checked?'1':'0';
	trombi_aff["manager"]=document.getElementById("trombi_manager").checked?'1':'0';
	trombi_aff["secretary"]=document.getElementById("trombi_secretary").checked?'1':'0';
	trombi_aff["thesard"]=document.getElementById("trombi_thesard").checked?'1':'0';
	trombi_aff["stagiaire"]=document.getElementById("trombi_stagiaire").checked?'1':'0';
	trombi_aff["enseignant"]=document.getElementById("trombi_enseignant").checked?'1':'0';
	trombi_aff["administratif"]=document.getElementById("trombi_administratif").checked?'1':'0';
	trombi_aff["postdoc"]=document.getElementById("trombi_postdoc").checked?'1':'0';
	trombi_aff["miniCV"]=document.getElementById("trombi_miniCV").checked?'1':'0';
	var params='service=\"'+trombi_service+'\" lg=\"'+trombi_lang+'\"';
	for(var ind in trombi_aff){
		params+=' '+ind+'=\"'+trombi_aff[ind]+'\"';
	}
	win.send_to_editor('[trombi '+params+']');
	return false;
}
/* ]]> */
</script>

<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<style type="text/css">
TBODY TH{
	margin: 0px;
	padding: 0px;
	text-align: right;
	vertical-align: top;
}
TBODY TH.titre{
	text-align: center;
	padding-top: 6px;
}
</style>
</head>
<body>
<form method="post" action="" onSubmit="return genereCodeTrombi();">
	<table>
	<tbody>
		<tr>
			<th>Langue d'affichage :</th>
			<td><select name="trombi_lang" id="trombi_lang">
				<option value="fr">Français</option>
				<option value="en">Anglais</option>
			</select></td>
		</tr>
		<tr>
			<th>Service du trombinoscope :</th>
			<td>
			<select name="trombi_service" id="trombi_service">
				<option value="">--- TSP ---</option>
				<option value="ARTEMIS">ARTEMIS</option>
				<option value="CITI">CITI</option>
				<option value="EPH">EPH</option>
				<option value="INF">INF</option>
				<option value="LOR">LOR</option>
				<option value="RS2M">RS2M</option>
				<option value="RST">RST</option>
				<option value="">--- TEM ---</option>
				<option value="DEFI">DEFI</option>
				<option value="DSI">DSI</option>
				<option value="LSH">LSH</option>
				<option value="MMS">MMS</option>
			</select>
			</td>
		</tr>
		<tr>
		<th colspan="2" class="titre">Options</th>
		</tr>
		<tr>
			<th>Populations à afficher :</th>
			<td>
				<ul>
					<li><input type="checkbox" checked name="trombi_manager" id="trombi_manager" value="1" />Le chef du service</li>
					<li><input type="checkbox" checked name="trombi_secretary" id="trombi_secretary" value="1" />Le secrétariat du service</li>
					<li><input type="checkbox" checked name="trombi_personnel" id="trombi_personnel" value="1" />Tout le personnel (CDD, CDI, post-doc)
					<br/><b>ou :</b>
						<ul>
							<li><input type="checkbox" name="trombi_enseignant" id="trombi_enseignant" value="1" />Les enseignants-chercheurs</li>
							<li><input type="checkbox" name="trombi_administratif" id="trombi_administratif" value="1" />Les administratifs et techniques</li>
							<li><input type="checkbox" name="trombi_postdoc" id="trombi_postdoc" value="1" />Les post-docs</li>
						</ul>
					</li>
					<li><input type="checkbox" checked name="trombi_thesard" id="trombi_thesard" value="1" />Les thésard</li>
					<li><input type="checkbox" checked name="trombi_stagiaire" id="trombi_stagiaire" value="1" />Les stagiaires</li>
				</ul>
			</td>
		</tr>
		<tr>
			<th>Afficher les mini CV (s'ils existent)</th>
			<td><input type="checkbox" name="trombi_miniCV" id="trombi_miniCV" value="1" />Afficher</td>
		</tr>
		<tr>
	    <td colspan="2" align="center"><input type="submit" name="Valider" /></td>
		</tr>
		</tbody>
	</table>
</form>
</body>
</html>


