<?php
  /**
  * Plugin Name: Seminars and Jobs
   * Plugin URI: 
   * Description: A simple plugin to manage research seminars and job announces
   * Version: 1.0
   * Author: Gaël Thomas
   * Author URI: 
   */

define( 'PLUGIN_DIR', dirname(__FILE__).'/' );  

/* add the configuration box just after the title for seminar and ctp */
add_action('edit_form_after_title', function() {
  global $post, $wp_meta_boxes;
  do_meta_boxes(get_current_screen(), 'after_title', $post);
  unset($wp_meta_boxes['post']['after_title']);
});

/* common part to display seminar of job */ 
class tsp_common_widget extends WP_Widget {
	function __construct($id, $name, $description) {
		parent::__construct($id, $name, $description);
	}

	public function initial_title() {
		return __('Unknow title');
	}

	public function initial_nb_posts() {
		return 3;
	}

	public function form($instance) {
		$title = isset($instance['title']) ? $instance['title'] : $this->initial_title();
		$nb_posts = isset($instance['nb_posts']) ? $instance['nb_posts'] : $this->initial_nb_posts();
		$nb_posts = $nb_posts == -1 ? 'all' : $nb_posts;

?>
		<p>
			<label for="<?= $this->get_field_id('title'); ?>"><?php _e('Title:'); ?></label>
			<input class="widefat" 
						 id="<?= $this->get_field_id('title'); ?>" 
						 name="<?= $this->get_field_name('title'); ?>" 
						 type="text" 
						 value="<?= esc_attr($title); ?>"/>
		</p>
		<p>
			<label for="<?= $this->get_field_id('nb_posts'); ?>"><?php _e('Limit (or all):'); ?></label>
			<input class="widefat" 
						 id="<?= $this->get_field_id('nb_posts'); ?>" 
						 name="<?= $this->get_field_name('nb_posts'); ?>" 
						 type="text" 
						 value="<?= esc_attr($nb_posts); ?>"/>
		</p>
<?php

    return '';
	}

  public function update($new_instance, $old_instance) {
		$instance = array();

		$instance['title'] = !empty($new_instance['title']) ? sanitize_text_field($new_instance['title']) : '';
		$nb_posts = !empty($new_instance['nb_posts']) ? $new_instance['nb_posts'] : 'all';
		$instance['nb_posts'] = $nb_posts === 'all' ? -1 : $nb_posts;

		return $instance;
	}
}

include 'seminar.php';
include 'job.php';

/*
 *   Extend search query for seminars and jobs
 */
function mv_meta_in_search_query($pieces, $args) {
  global $job_types;
  global $wpdb;

  if(!empty($args->query['s'])) { // only run on search query.
    //print_r('Original: <br>'); print_r($pieces);
    $keywords        = explode(' ', sanitize_text_field(get_query_var('s')));
    $escaped_percent = $wpdb->placeholder_escape(); // WordPress escapes "%" since 4.8.3 so we can't use percent character directly.
    $query           = "";

    foreach ($keywords as $word) {
      foreach(array('abstract', 'bio') as $key)
        $query .= " (m.meta_key = '$key' AND m.meta_value LIKE '{$escaped_percent}{$word}{$escaped_percent}') OR ";

      for($i=0; $i<count($job_types); $i++)
        if(strtolower($job_types[$i]) === strtolower($word))
          $query .= " ({$wpdb->posts}.post_type = 'job_cpt' AND m.meta_key = 'type' AND m.meta_value = $i) OR ";
    }

    if(!empty($query)) { // append necessary WHERE and JOIN options.
      $pieces['where'] = str_replace( "((({$wpdb->posts}.post_title LIKE '{$escaped_percent}", "( {$query} (({$wpdb->posts}.post_title LIKE '{$escaped_percent}", $pieces['where'] );
      $pieces['join'] = $pieces['join'] . " LEFT OUTER JOIN {$wpdb->postmeta} AS m ON ({$wpdb->posts}.ID = m.post_id) ";
    }

    $pieces['groupby'] = "{$wpdb->posts}.ID";

    //print_r('<br>Result: <br>'); print_r($pieces);
  }

  return $pieces;
}

add_filter('posts_clauses', 'mv_meta_in_search_query', 20, 2);

/* configuration page */
add_action('admin_menu', function(  ) {
  add_options_page( 'Seminars options', 'Seminars', 'manage_options', 'settings-api-page', 'tsp_inf_options_page' );
});

add_action('admin_init', function() {
  register_setting( 'tsp_inf_plugin', 'tsp_inf_settings' );
  add_settings_section(
    'tsp_inf_plugin_section',
    'Seminar configuration',
    null,
    'tsp_inf_plugin'
  );

  add_settings_field(
    'seminar_name',
    'Seminar name',
    'seminar_name_render',
    'tsp_inf_plugin',
    'tsp_inf_plugin_section'
  );
  
  add_settings_field(
    'seminar_mailing',
    'Seminar mailing',
    'seminar_mailing_render',
    'tsp_inf_plugin',
    'tsp_inf_plugin_section'
  );
});

function seminar_name_render(  ) {
  $options = get_option('tsp_inf_settings');
?>
  <input type='text' name='tsp_inf_settings[seminar_name]' value='<?php echo $options['seminar_name']; ?>'>
<?php
}

function seminar_mailing_render(  ) {
  $options = get_option('tsp_inf_settings');
?>
  <input type='text' name='tsp_inf_settings[seminar_mailing]' value='<?php echo $options['seminar_mailing']; ?>'>
<?php
}

function tsp_inf_options_page(  ) {
?>
  <form action='options.php' method='post'>
    <?php
    settings_fields( 'tsp_inf_plugin' );
    do_settings_sections( 'tsp_inf_plugin' );
    submit_button();
    ?>
  </form>
<?php
}

?>
