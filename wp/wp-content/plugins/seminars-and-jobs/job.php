<?php


$job_types = array('Full professor',
                   'Associate professor', 
                   'Postdoc', 
                   'Engineer', 
                   'PhD thesis',
                   'Internship');

/* create the ctp */
add_action( 'init', function() {
  register_post_type('job_cpt',
				// CPT Options
        array(
            'labels' => array(
                'name' => __( 'Jobs' ),
                'singular_name' => __( 'Job' )
            ),
            'public' => true,
            'has_archive' => true,
            'rewrite' => array('slug' => 'jobs_cpt'),
            'show_in_rest' => true,
						'supports' => array( 'title', 'editor' ),
        )
  );
});

/* meta_box configuration */
add_action('add_meta_boxes', function() {
  add_meta_box('jobmb_id',     __('Details'), 'job_meta_box_callback',     'job_cpt',     'after_title', 'high');
});

function job_meta_box_callback($post) {
	// Add a nonce field so we can check for it later.
	wp_nonce_field('job_save_meta_box_data', 'job_meta_box_nonce');

	$state = get_post_meta($post->ID, 'state', true);
?>
  <style type='text/css'>#jobmb_id { margin-top: 2rem; }</style>

	<table class="form-table">
		<tr>
			<td style="width: 150px"><label for="job_state">State</label></td>
			<td>
				<select name="job_state" id="job_state">
					<?php 
          foreach(array('Open', 'Closed') as $state) {
					  echo '<option value=' . $state;
            echo get_post_meta($post->ID, 'state', true) == $state ? ' selected>' : '>';
						echo $state;
						echo '</option>';
					} ?>
				</select>
			</td>
		</tr>

		<tr style="border-top: 1px solid #ccd0d4">
			<td style="width: 150px"><label for="job_type">Job type</label></td>
			<td>
				<select name="job_type" id="job_type">
					<?php 
          global $job_types;
          $n = job_type_index($post);
          for($i=0; $i<count($job_types); $i++) {
					  echo '<option value=' . $i; 
            echo $n == $i ? ' selected>' : '>';
						echo $job_types[$i];
						echo '</option>';
					} ?>
				</select>
			</td>
    </tr>
  </table>

<?php
}

/* save the configuration */
add_action('save_post', function($post_id) {
	if(!isset( $_POST['job_meta_box_nonce']) || 
     !wp_verify_nonce($_POST['job_meta_box_nonce'], 'job_save_meta_box_data') ||
	   (defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE) ||
     !current_user_can('edit_post', $post_id))
       return;

	update_post_meta($post_id, 'state', sanitize_text_field($_POST['job_state']));
	update_post_meta($post_id, 'type', sanitize_text_field($_POST['job_type']));
});

/* format */
function job_type_index($post) {
  $n = get_post_meta($post->ID, 'type', true);
  return $n === '' ? 4 : $n;
}

function format_job_title($post) {
  global $job_types;
  return $job_types[job_type_index($post)] . ' - ' . get_the_title($post) . 
         (get_post_meta(get_post()->ID, 'state', true) === 'Closed' ? ' (closed position)' : '');
}

/* shortcodes */
function job_get_state($post) {
  $state = get_post_meta($post->ID, 'state', true);
  return strtolower($state ? $state : 'Open');
}

function job_list($n, $state_filter) {
  global $job_types;

	$query = new WP_Query(array( 
    'post_type'      => 'job_cpt',
    'post_status'    => 'publish',
		'order'          => 'DESC',
		'posts_per_page' => $n,
  ));

  $list = array();

  foreach($job_types as $type) 
    $list[] = '';

	if($query->have_posts()) {
		while($query->have_posts()) {
			$query->the_post();
			$post = get_post();
      $state = get_post_meta($post->ID, 'state', true);
      $state = strtolower($state ? $state : 'Open');

      if($state === 'all' || $state === $state_filter) {
        $list[job_type_index($post)] .= '<li><a href="' . get_permalink($post) . '">' . 
                                        format_job_title($post) . '</a></li>';
      }
    }
  }

  $res = '';
  foreach($list as $element) 
    $res .= $element;

	$res = empty($res) ? '' : '<ul>' . $res . '</ul>';

	wp_reset_postdata();

  return $res;
}

add_shortcode('jobs', function($a) {
	$attrs = shortcode_atts( array(
    'n' => -1,
    'state' => 'open',
  ), $a );

  $res = job_list($attrs['n'], $attrs['state']);
  
  return empty($res) ? "<p>Sorry, we currently don't have opened positions.</p>" : $res;
});


/* widget */
class job_widget extends tsp_common_widget {
	function __construct() {
		parent::__construct('job_widget',
												__('Jobs'),
												array('description' => __('Display the last jobs')));
	}

	public function initial_title() {
		return __('Jobs');
	}

	public function initial_nb_posts() {
		return -1;
	}

	public function widget($args, $instance) {
		$title = apply_filters('widget_title', 
                           array_key_exists('title', $instance) ? 
                           $instance['title'] : 'Last seminars');
		$nb_posts = array_key_exists('nb_posts', $instance) ? $instance['nb_posts'] : initial_nb_posts();

    $list = job_list($nb_posts, 'open');

    if(!empty($list)) {
			echo $args['before_widget'];

			if (!empty( $title ) )
				echo $args['before_title'] . $title . $args['after_title'];

      echo $list;
			echo $args['after_widget'];
		}
	}
}

add_action('widgets_init', function() { register_widget(new job_widget()); });


/*
 *   Modify next_post_link/previous_post_link
 *     - only display opened jobs
 *     - link jobs by type and then post_date instead of only post_date
 *     - link seminars by when instead of post_date
 */


function job_get_adjacent_with_meta_keys_join($join) {
  global $wpdb;
  if(is_singular('job_cpt'))
    return $join . 
           " INNER JOIN $wpdb->postmeta AS m1 ON p.ID = m1.post_id" .
           " INNER JOIN $wpdb->postmeta AS m2 ON p.ID = m2.post_id";
  else
    return $join;
}

add_filter('get_previous_post_join',  'job_get_adjacent_with_meta_keys_join');
add_filter('get_next_post_join',      'job_get_adjacent_with_meta_keys_join');

add_filter('get_previous_post_where', function($where) {
  global $post;
  if(is_singular('job_cpt'))
    return "WHERE p.post_type = 'job_cpt' " .
           "AND ( p.post_status = 'publish' OR p.post_status = 'private' ) " .
           "AND m1.meta_key = 'state' and m1.meta_value = '" . get_post_meta($post->ID, 'state', true) . "' " .
           "AND m2.meta_key = 'type'" .
           "AND ((m2.meta_value = '" . get_post_meta($post->ID, 'type', true) . "' " .
           "      AND p.post_date < '" . $post->post_date . "')" .
           "     OR CAST(m2.meta_value AS UNSIGNED) > " . get_post_meta($post->ID, 'type', true) . ")";
  else
    return $where;
});

add_filter('get_next_post_where',     function($where) {
  global $post;
  if(is_singular('job_cpt'))
    return "WHERE p.post_type = 'job_cpt' " .
           "AND ( p.post_status = 'publish' OR p.post_status = 'private' ) " .
           "AND m1.meta_key = 'state' and m1.meta_value = '" . get_post_meta($post->ID, 'state', true) . "' " .
           "AND m2.meta_key = 'type'" .
           "AND ((m2.meta_value = '" . get_post_meta($post->ID, 'type', true) . "' " .
           "      AND p.post_date > '" . $post->post_date . "')" .
           "     OR CAST(m2.meta_value AS UNSIGNED) < " . get_post_meta($post->ID, 'type', true) . ")";
  else
    return $where;
});

add_filter('get_previous_post_sort', function($sort) {
  if(is_singular('job_cpt'))
    return "ORDER BY CAST(m2.meta_value AS UNSIGNED) ASC, p.post_date DESC LIMIT 1";
  else
    return $sort;
});

add_filter('get_next_post_sort', function($sort) {
  if(is_singular('job_cpt'))
    return "ORDER BY CAST(m2.meta_value AS UNSIGNED) DESC, p.post_date ASC LIMIT 1";
  else
    return $sort;
});

//add_filter('the_content', function($content) {
//  global $post;
//  if ($post->post_type == 'job_cpt') {
//  }
//  return $content;
//});

?>
