<?php

add_action('wp_print_styles', function() {
	wp_register_style('fontawesome', 'https://use.fontawesome.com/releases/v5.10.0/css/all.css');
	wp_enqueue_style( 'fontawesome');
});

function fa_icon($name) {
  return '<span style="display: inline-block; text-align: center; font-size: 0.9em; width: 1.4em;"><i class="fa ' . $name . '" aria-hidden="true"></i></span>';
}

$seminar_types_det = array('an', 'a', 'a');
$seminar_types = array('Invited talk', 'Reading group', 'Team work', 'Other');

/* timezones: in the database, we have timestamps in utc */
function tz() {
  $tz_string = get_option('timezone_string');
  return new DateTimeZone($ts_string ? $tz_string : 'Europe/Paris');
}
  
function parse_time($when_local_string) {
	$date_local = DateTime::createFromFormat('Y-m-d H:i', $when_local_string, tz());
	if($date_local) {
		return $date_local->getTimestamp();
  } else {
    return null;
  }
}

function utc_to_local($ts) {
  $date = new DateTime();
  $date->setTimestamp($ts);
  $tz_shift = tz()->getOffset($date);
  return $ts ? $ts + $tz_shift : $ts;
}

function get_duration($post, $default=45*60) {
  $duration = get_post_meta($post->ID, 'duration', true);
  return $duration ? $duration : $default;
}

function get_when_local($post) {
  return utc_to_local(get_when_utc($post));
}

function get_when_utc($post) {
  return get_post_meta($post->ID, 'when', true);
}

/*activation */
register_activation_hook( __FILE__, function() { 
  global $wp_rewrite;
  $wp_rewrite->flush_rules(false);
});

/* create the ctp */
add_action( 'init', function() {
  register_post_type('seminar_cpt',
				// CPT Options
        array(
            'labels' => array(
                'name' => __( 'Seminars' ),
                'singular_name' => __( 'Seminar' )
            ),
            'public' => true,
            'has_archive' => true,
            'rewrite' => array('slug' => 'seminars_cpt'),
            'show_in_rest' => true,
						'supports' => array( 'title' ),
        )
  );
});


/* add a configuration box (after_title defined in tsp-inf.php) */
add_action('add_meta_boxes', function() {
  add_meta_box('seminarmb_id', __('Details'), 'seminar_meta_box_callback', 'seminar_cpt', 'after_title', 'high');
});

function seminar_meta_box_callback($post) {
	global $seminar_types;

	// Add a nonce field so we can check for it later.
	wp_nonce_field( 'seminar_save_meta_box_data', 'seminar_meta_box_nonce' );

	$when_local = get_when_local($post);
	$when_local = $when_local ? $when_local : ceil(utc_to_local(time()) / (30*60)) * (30*60);
  
	$fields = array(
		array('name'  => 'Mandatory for the master students', 
					'id'    => 'seminar_mandatory',
					'type'  => 'checkbox',
					'other' => get_post_meta($post->ID, 'mandatory', true) ? 'checked' : ''),
		array('name'  => 'Day', 
					'id'    => 'seminar_day',
					'type'  => 'date',
					'value' => date('Y-m-d', $when_local)),
		array('name'  => 'Time', 
					'id'    => 'seminar_time',
					'type'  => 'time',
					'value' => date('H:i', $when_local)),
		array('name'  => 'Duration', 
					'id'    => 'seminar_duration',
					'type'  => 'time',
					'value' => date('H:i', get_duration($post))),
		array('name'  => 'Where', 
					'id'    => 'seminar_where',
					'type'  => 'text',
					'other' => 'style="width: 100%"',
					'value' => get_post_meta($post->ID, 'where', true)),
		array('name'  => 'Visio', 
					'id'    => 'seminar_visio',
					'type'  => 'text',
					'other' => 'style="width: 100%"',
					'value' => get_post_meta($post->ID, 'visio', true)),
		array('name'  => 'Speaker', 
					'id'    => 'seminar_speaker',
					'type'  => 'text',
					'other' => 'style="width: 100%"',
					'value' => get_post_meta($post->ID, 'speaker', true)),
		array('name'  => 'Venue', 
					'id'    => 'seminar_venue',
					'type'  => 'text',
					'other' => 'style="width: 100%"',
					'value' => get_post_meta($post->ID, 'venue', true)),
		array('name'  => 'Paper URL', 
					'id'    => 'seminar_url',
					'type'  => 'text',
					'other' => 'style="width: 100%"',
					'value' => get_post_meta($post->ID, 'url', true)),
		array('name'  => 'Abstract', 
					'id'    => 'seminar_abstract',
					'type'  => 'wp_editor',
					'value' => get_post_meta($post->ID, 'abstract', true)),
		array('name'  => 'Bio', 
					'id'    => 'seminar_bio',
					'type'  => 'wp_editor',
					'value' => get_post_meta($post->ID, 'bio', true)),
		array('name'  => 'Video', 
					'id'    => 'seminar_video',
					'type'  => 'text',
					'other' => 'style="width: 100%"',
					'value' => get_post_meta($post->ID, 'video', true)),
		array('name'  => 'Slides', 
					'id'    => 'seminar_slides',
					'type'  => 'text',
					'other' => 'style="width: 100%"',
					'value' => get_post_meta($post->ID, 'slides', true)),
		array('name'  => 'Mail sent to the mailing list', 
					'id'    => 'seminar_mail_sent',
					'type'  => 'checkbox',
					'other' => get_post_meta($post->ID, 'mail_sent', true) ? 'checked' : ''),
	); 
?>
  <style type='text/css'>#seminarmb_id { margin-top: 2rem; }</style>
  
	<table class="form-table">
		<tr>
			<td style="width: 150px"><label for="seminar_type">Seminar type</label></td>
			<td>
				<select name="seminar_type" id="seminar_type">
					<?php for($i=0; $i<count($seminar_types); $i++) {
					  echo '<option value=' . $i; 
            echo get_post_meta($post->ID, 'type', true) == $i ? ' selected>' : '>';
						echo $seminar_types[$i];
						echo '</option>';
					} ?>
				</select>
			</td>
		</tr>
	<?php foreach ($fields as $key => $field) {
		$formid = '"seminar_' . $field['name'] . '"';
	?>

		<tr style="border-top: 1px solid #ccd0d4">
			<td style="width: 150px"><label for="<?= $formid ?>"><?= $field['name'] ?></label></td>

	<?php if($field['type'] === 'wp_editor') { ?>
	     <td><?php wp_editor($field['value'], $field['id'], 
													 array('textarea_rows' => 10,
																 'wpautop' => false,
																 'textarea_name' => $field['id'])); ?></td>
	<?php } else { ?>
		  <td><input type="<?= $field['type'] ?>"
								 name="<?= $field['id'] ?>"
								 id="<?= $formid ?>"
								 value="<?= $field['value'] ?>"
								 <?= array_key_exists('other', $field) ? ' ' . $field['other'] : ''; ?> /></td>

	<?php } ?>
	   </tr>
	<?php
  }

	echo '</table>';
}

/* save the configuration of the seminar */
add_action('save_post', function($post_id) {
	if(!isset( $_POST['seminar_meta_box_nonce']) || 
     !wp_verify_nonce($_POST['seminar_meta_box_nonce'], 'seminar_save_meta_box_data') ||
	   (defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE) ||
     !current_user_can('edit_post', $post_id))
       return;
	
	$when_local = sanitize_text_field($_POST['seminar_day']) . ' ' . sanitize_text_field($_POST['seminar_time']);
  $ts = parse_time($when_local);

	if($ts)
		update_post_meta($post_id, 'when', $ts);
	else
		delete_post_meta($post_id, 'when');

  $parts = explode(':', $_POST['seminar_duration']);
  if(count($parts) == 2)
    update_post_meta($post_id, 'duration', ($parts[0]*60 + $parts[1]) * 60);
  else
    delete_post_meta($post_id, 'duration');
  
	update_post_meta($post_id, 'type', sanitize_text_field($_POST['seminar_type']));
	update_post_meta($post_id, 'mandatory', isset($_POST['seminar_mandatory']));
	update_post_meta($post_id, 'where', sanitize_text_field($_POST['seminar_where']));
	update_post_meta($post_id, 'speaker', sanitize_text_field($_POST['seminar_speaker']));
	update_post_meta($post_id, 'venue', sanitize_text_field($_POST['seminar_venue']));
	update_post_meta($post_id, 'url', sanitize_text_field($_POST['seminar_url']));
	update_post_meta($post_id, 'abstract', wp_kses_post($_POST['seminar_abstract']));
	update_post_meta($post_id, 'bio', wp_kses_post($_POST['seminar_bio']));
	update_post_meta($post_id, 'visio', sanitize_text_field($_POST['seminar_visio']));
	update_post_meta($post_id, 'video', sanitize_text_field($_POST['seminar_video']));
	update_post_meta($post_id, 'slides', sanitize_text_field($_POST['seminar_slides']));
	update_post_meta($post_id, 'mail_sent', isset($_POST['seminar_mail_sent']));
});

function is_past($post) {
	$when_utc = get_when_utc($post);
  return $when_utc ? $when_utc + 24*60*60 < time() : false;
}
  
function get_visio($post) {
  return is_past($post) ? null : get_post_meta($post->ID, 'visio', true);
}
  
function is_seminar_mandatory($post) {
  $res = get_post_meta($post->ID, "mandatory", true);
  return $res ? $res : false;
}

function seminar_sender() {
  $user = wp_get_current_user();
  return $user->user_email ? $user->user_email : get_bloginfo('admin_email');
}

function seminar_mailing() {
  $options = get_option('tsp_inf_settings');
  $mailing = $options['seminar_mailing'];
  return $mailing ? $mailing : get_bloginfo('admin_email');
}
  
/* format */
function format_when($post, $before = '', $after = '') {
	$when_local = get_when_local($post);
	return $when_local ? $before . date('j/n/Y \a\t G\hi', $when_local) . $after : '';
}

function do_where_link($post, $where, $withLink) {
	$visio = get_visio($post);
  return $visio && $withLink ? '<a href="' . $visio . '">' . $where . '</a>' : $where;
}

function format_where($post, $isTable, $withLink) {
	$where = get_post_meta($post->ID, 'where', true);
	$visio = get_visio($post);
  $res = '';

  if($where) {
    $res = do_where_link($post, $where, false);
    if($visio)
      $res .= ' (<a href="' . $visio . '">visio link</a>)';
    $res = $isTable ? $res : ' at ' . $res;
  } else if(get_post_meta($post->ID, 'visio', true)) {
    $res = do_where_link($post, $isTable ? 'Visio' : 'visio', $visio && $withLink);
    $res = $isTable ? $res : ' in ' . $res;
  }

  return $res;
}

function format_seminar_short($post, $before = '', $after = '', $withLink = false, $withLocation = true) {
	$speaker = get_post_meta($post->ID, 'speaker', true);
	$venue = get_post_meta($post->ID, 'venue', true);

  $res = '';

  $strs = is_past($post) ?
          array(' presented ',
                ' took place ',
                ' made a presentation ',
                'We had a presentation') :
          array(' will present ',
                ' will take place ',
                ' will make a presentation ',
                'We will jave a presentation');
  
  if($withLocation) {
	  $res .= format_where($post, false, $withLink);
	  $res .= format_when($post, ' the ');
  }

  if($post->post_title) {
	  $title = '"' . $post->post_title . '"' . ($venue ? ' (' . $venue . ')' : '');
	  if($speaker)
		  $res = $speaker . $strs[0] . $title . $res;
	  else if($res !== '')
		  $res = 'The presentation ' . $title . ($res ? $strs[1] . $res : $res);
  } else {
	  if($speaker)
		  $res = $speaker . $strs[2] . $res;
	  else if($res !== '')
		  $res = $strs[3] . $res;
  }
  
	if($res !== '')
		$res = $before . $res . $after;

	return $res;
}
  
function format_seminar_full($post) {
  global $seminar_types;
	$abstract = get_post_meta($post->ID, 'abstract', true);
	$bio = get_post_meta($post->ID, 'bio', true);

  $res = '';

  $pre = is_past($post) ? '' : '<a href="' . get_feed_link('calendar') . '?id=' . $post->ID . '">' . fa_icon('fa-calendar') . '</a>';
  $pre = '<p>' . $pre . $seminar_types[get_post_meta($post->ID, 'type', true)] . ': ';
	$res .= format_seminar_short($post, $pre, '.</p>', true);

  $video = get_post_meta($post->ID, 'video', true);
  $slides = get_post_meta($post->ID, 'slides', true);

  if($video) {
    $res .= '<p>You can find the video of the presentation <a href="' . $video . '">here</a>';
    if($slides)
      $res .= ' and the slides <a href="' . $slides . '">here</a>';
    $res .= '.</p>';
  } else if($slides)
    $res .= '<p>You can find the slides of the presentation <a href="' . $slides . '">here</a>.</p>';
  
  if($abstract)
		$res .= '<h4>Abstract</h4><p>' . $abstract . '</p>';
	if($bio)
		$res .= '<h4>Bio</h4><p>' . $bio . '</p>';

  if(!is_past($post))
    $res .= add_seminar_form($post);
  
	return $res;
}

function seminar_name() {
  $options = get_option('tsp_inf_settings');
  $seminar_name = $options['seminar_name'];
  return $seminar_name ? $seminar_name : get_bloginfo('name') . ' Seminar';
}
  
/* shortcodes */
function get_seminars($n=-1, $id=false, $from=-1, $to=null, $order='DESC') {
  $query = array('post_type'      => 'seminar_cpt',
                 'post_status'    => 'publish',
		             'meta_key'       => 'when',
                 'orderby'        => 'meta_value_num',
		             'order'          => $order,
		             'posts_per_page' => $n);

  if($id)
    $query['p'] = $id;

  if($from == -1)
    $from = time() - 1*365.25*24*60*60;
  
  if($from) {
    if($to) {
      $query['meta_value'] = array($from, $to);
      $query['meta_compare'] = 'BETWEEN';
    } else {
      $query['meta_value'] = $from;
      $query['meta_compare'] = '>=';
    }
  } else if($to) {
    $query['meta_value'] = $to;
    $query['meta_compare']   = '<=';
  }
  
	return new WP_Query($query);
}

// https://www.inf.telecom-sudparis.eu/pds/wp-json/seminars/data
add_action('rest_api_init', function () {
  register_rest_route('seminars', '/data', array(
    'methods'  => 'GET',
    'callback' => 'seminars_json',
  ));
});
  
function seminars_json($data) {
	global $seminar_types;
	$query = get_seminars(-1, false, false, flase);

  $res = [];
  
	if($query->have_posts()) {
		while($query->have_posts()) {
      $query->the_post();
			$post = get_post();

      $seminar = [];
      $seminar["title"] = get_the_title();
      $when = get_when_local($post);
      
      foreach([ "type", "mandatory", "where", "speaker", "venue", "url", "abstract", "bio", "visio", "video", "slides", "mail_sent" ] as $cur)
        $seminar[$cur] = get_post_meta($post->ID, $cur, true);

      $seminar["type"] = $seminar_types[$seminar["type"]];
      $seminar["day"] = date("j/n/Y", $when);
      $seminar["start"] = date("G:i", $when);
      $seminar["end"] = date("G:i", $when + get_post_meta($post->ID, "duration", true));
      
      array_push($res, $seminar);
    }
  }

  return rest_ensure_response($res);
}
  
  
add_shortcode('seminars', function($a) {
	global $seminar_types;

	$attrs = shortcode_atts( array(
    'n' => '-1',
    'from' => false,
    'to' => false,
    'by-year' => false
  ), $a );

  $byYear = $attrs['by-year'];
  
  $from = -1;
  if($attrs['from']) {
    if($attrs['from'] == -1)
      $from = null;
    else {
      $ts = parse_time($attrs['from'] . ' 00:00');
		  if($ts)
        $from = $ts;
    }
  }

  $to = null;
  if($attrs['to']) {
	  $date = DateTime::createFromFormat('Y-m-d H:i', $attrs['to'] . ' 23:59');
		if($date)
      $to = local_to_utex($date->getTimestamp());
  }

  $res = '';

  if(is_user_logged_in())
    $res .= '<p>Click <a href="' . get_bloginfo('url') .
            '/wp-admin/post-new.php?post_type=seminar_cpt">here</a> to add a new seminar.</p>';
  
	$query = get_seminars($attrs['n'], false, $from, $to);
  $cur_year = false;
  
	if($query->have_posts()) {
		while($query->have_posts()) {
      $query->the_post();
			$post = get_post();
      $when_local = get_when_local($post);

      if($when_local) {
        $year = date('Y', $when_local);

        if($cur_year !== $year) {
          if($cur_year && $byYear)
            $res .= '</table>';
          if($byYear)
            $res .= '<h3> Seminars in ' . $year . '</h3>';
          if(!$cur_year || $byYear) {
		        $res .= '<table class="seminar">';
		        $res .= '  <tr>';
		        $res .= '    <th style="border-left: none; border-top: none; border-right: none; min-width: 4.5em;"></th>';
		        $res .= '    <th>When</th>';
            //		$res .= '    <th>Where</th>';
		        $res .= '    <th style="min-width: 4em;">Type</th>';
		        $res .= '    <th>Speakers</th>';
		        $res .= '    <th style="min-width: 0em;">Title</th>';
		        $res .= '  </tr>';
          }
          
          $cur_year = $year;
        }
      
			  $url = get_post_meta($post->ID, 'url', true);
        $venue = get_post_meta($post->ID, 'venue', true);
        $video = get_post_meta($post->ID, 'video', true);
        $visio = get_visio($post);
        $url = get_post_meta($post->ID, 'url', true);
        $slides = get_post_meta($post->ID, 'slides', true);
			  $title = get_the_title() . ($venue ? ' (' . $venue . ')' : '');
			  $title = $visio ? '<a href="' . $visio . '">' . $title . '</a>' : $title;
      
			  $res .= '<tr>';
			  $res .= '<td style="text-align: center;">' .
                '<a href="' . get_permalink($post) . '">' . fa_icon('fa-link') . '</a>' .
               (is_past($post) ? '' :
                '<a href="' . get_feed_link('calendar') . '?id=' . $post->ID . '">' . fa_icon('fa-calendar') . '</a>') .
               ($url ? '<a target="_blank" href="' . $url . '">' . fa_icon('fa-file') . '</a>' : '') .
               ($video ? '<a target="_blank" href="' . $video . '">' . fa_icon('fa-film') . '</a>' :
                ($visio ? '<a target="_blank" href="' . $visio . '">' . fa_icon('fa-play') . '</a>'  : '')) .
               ($slides ? '<a target="_blank" href="' . $slides . '">' . fa_icon('fa-download') . '</a>' : '') .
               (is_seminar_mandatory($post) ? fa_icon('fa-graduation-cap') : '') .
                '</td>';
			  $res .= '<td>' . format_when($post) . '</td>';
        //			$res .= '<td>' . format_where($post, true, true) . '</td>';
			  $res .= '<td>' . $seminar_types[get_post_meta($post->ID, 'type', true)] . '</td>';
			  $res .= '<td>' . get_post_meta($post->ID, 'speaker', true) . '</td>';
			  $res .= '<td>' . $title . '</td>';
			  $res .= '</tr>';
      }
    }

    if($cur_year)
		  $res .= '</table>';

    $res .= add_seminar_form();
  }

	wp_reset_postdata();

	return $res;
});

/* seminar widget */
class seminar_widget extends tsp_common_widget {
	function __construct() {
		parent::__construct('seminar_widget',
												__('Seminars'),
												array('description' => __('Display the last seminars')));
	}

	public function initial_title() {
		return __('Last seminars');
	}

	public function widget($args, $instance) {
		$title = apply_filters('widget_title', 
                           array_key_exists('title', $instance) ? 
                           $instance['title'] : 'Last seminars');
		$nb_posts = array_key_exists('nb_posts', $instance) ? $instance['nb_posts'] : -1;

    $now_utc = time();
    
		$query = get_seminars(-1, false);
    $sorted = array();

    while($query->have_posts()) {
      $query->the_post();
      $post = get_post();
	    $when_utc = get_when_utc($post);

      if($when_utc)
        $sorted[$when_utc] = $post;
    }

    krsort($sorted, SORT_NUMERIC);

    $cur = 0;
    $start = 0;
    $end = 0;
    
    foreach($sorted as $k => $v) {
      if($now_utc <= $k) { /* after $now_utc */
        $end ++;
        if($end - $start > $nb_posts)
          $start ++;
      } else {
        if($end - $start < $nb_posts)
          $end ++;
      }
    }

    if($end > $start) {
      $res = array_slice($sorted, $start, $end - $start);

			echo $args['before_widget'];

			//if title is present
			if (!empty( $title ) )
				echo $args['before_title'] . $title . $args['after_title'];
			//output

			echo '<ul>';

      foreach($res as $post) {
				echo format_seminar_short($post, '<li><a href="' . get_permalink($post) . '">', '</a></li>');
			}
			echo '</ul>';

		  echo $args['after_widget'];
    }
    
		wp_reset_postdata();
	}
}

add_action('widgets_init', function() { register_widget(new seminar_widget()); });

/*
 *   Modify next_post_link/previous_post_link
 *     - link seminars by when instead of post_date
 */

/* load the meta keys */
function seminar_get_adjacent_with_meta_keys_join($join) {
  global $wpdb;
  if(is_singular('seminar_cpt'))
    return $join . "INNER JOIN $wpdb->postmeta AS m ON p.ID = m.post_id";
  else
    return $join;
}

add_filter('get_previous_post_join',  'seminar_get_adjacent_with_meta_keys_join');
add_filter('get_next_post_join',      'seminar_get_adjacent_with_meta_keys_join');

add_filter('get_previous_post_where', function($where) {
  global $post;
  if(is_singular('seminar_cpt'))
    return "WHERE p.post_type = 'seminar_cpt' " .
           "AND ( p.post_status = 'publish' OR p.post_status = 'private' ) " .
           "AND m.meta_key = 'when' AND CAST(m.meta_value AS UNSIGNED) < " . get_post_meta($post->ID, 'when', true);
  else
    return $where;
});

add_filter('get_next_post_where',     function($where) {
  global $post;
  if(is_singular('seminar_cpt'))
    return "WHERE p.post_type = 'seminar_cpt' " .
           "AND ( p.post_status = 'publish' OR p.post_status = 'private' ) " .
           "AND m.meta_key = 'when' AND CAST(m.meta_value AS UNSIGNED) > " . get_post_meta($post->ID, 'when', true);
  else
    return $where;
});

add_filter('get_previous_post_sort', function($sort) {
  if(is_singular('seminar_cpt'))
    return "ORDER BY CAST(m.meta_value AS UNSIGNED) DESC LIMIT 1";
  else
    return $sort;
});

add_filter('get_next_post_sort', function($sort) {
  if(is_singular('seminar_cpt'))
    return "ORDER BY CAST(m.meta_value AS UNSIGNED) ASC LIMIT 1";
  else
    return $sort;
});

/* calender */

function icsSanitize($str) {
  return preg_replace('/([\,;])/','\\\$1', $str);
}

function natural_list($list, $sep = 'and') {
  $last = array_pop($list);
  return $list ? implode(', ', $list) . ' ' . $sep . ' ' . $last : $last;
}
  
function seminar_ics_gen($posts, $merge) {
  $merge = $merge && count($posts) > 1;
  $seminar_name = seminar_name();

  $res = [ 'BEGIN:VCALENDAR',
           'VERSION:2.0',
           'PRODID:' . get_bloginfo('url'),
           'CALSCALE:GREGORIAN',
           'METHOD:PUBLISH',
           'NAME:' . $seminar_name,
           'DESCRIPTION:' . $seminar_name,
           'REFRESH-INTERVAL;VALUE=DURATION:PT12H' ];

  $only_mandatory = array_key_exists('only-mandatory', $_REQUEST);
  $tell_if_mandatory = array_key_exists('is-mandatory', $_REQUEST);

  $ts_first_start = null;
  $ts_last_start = null;
  $ts_last_end = null;
  $events = array();
  $speakers = array();
  
  foreach($posts as $post) {
    $ts_start = get_when_utc($post);

    if($ts_start && (!$only_mandatory || is_seminar_mandatory($post))) {
      $ts_end = $ts_start + get_duration($post);
      
      if(!$ts_first_start || $ts_start < $ts_first_start)
        $ts_first_start = $ts_start;
      if(!$ts_last_end || $ts_last_end < $ts_end) {
        $ts_last_start = $ts_start;
        $ts_last_end = $ts_end;
      }
        
      $start = new DateTime('@' . $ts_start);
      $end = new DateTime('@' . $ts_end);

      $speaker = get_post_meta($post->ID, 'speaker', true);
      $summary = $seminar_name . ($speaker ? ' by ' . $speaker : '');

      if($speaker) array_push($speakers, $speaker);
      
	    global $seminar_types;
      $where = get_post_meta($post->ID, 'where', true);
      $visio = get_visio($post);
      $description = $seminar_types[get_post_meta($post->ID, 'type', true)];
      $description .= $merge ? ' on ' . date('j/n/Y \a\t G:i', utc_to_local($ts_start)) : "";
      $description .= ": " . format_seminar_short($post, '', '', false, false);
      $description = $description . ($visio ? "\\n\\nVisio: " . $visio : "") . ".";
      if($tell_if_mandatory) {
        if(is_seminar_mandatory($post))
          $description .= '\\n\\nMandatory for the master students.';
        else
          $description .= '\\n\\nNot mandatory for the master students.';
      }

      $description .= "\\n\\nFull post: " . get_permalink($post);

      $events[$ts_start] = array();
      $events[$ts_start]['BEGIN'] = 'VEVENT';
      $events[$ts_start]['DTSTART'] = $start->format('Ymd\THis\Z');
      $events[$ts_start]['DTEND'] = $end->format('Ymd\THis\Z');
      $events[$ts_start]['DTSTAMP'] = (new DateTime('now'))->format('Ymd\THis\Z');
      $events[$ts_start]['LOCATION'] = $where ? $where : ($visio ? $visio : '');
      $events[$ts_start]['UID'] = $post->ID . '@' . md5(get_bloginfo('url') . '@' . $seminar_name);// . uniqid();
      $events[$ts_start]['ORGANIZER;CN=' . $seminar_name] = 'mailto:' . seminar_mailing();
      $events[$ts_start]['SUMMARY'] = icsSanitize($summary);
      $events[$ts_start]['DESCRIPTION'] = icsSanitize($description);
      $events[$ts_start]['END'] = 'VEVENT';
    }
  }

  if($merge) {
    $event = $events[$ts_first_start];
    $event['DTEND'] = $events[$ts_last_start]['DTEND'];
    $event['SUMMARY'] = $seminar_name . ' - ' . natural_list($speakers);

    $desc = '';
    foreach($events as $cur)
      $desc = ($desc ? $desc . "\\n" : "") . "\\n==================\\n" . $cur['DESCRIPTION'];
    $event['DESCRIPTION'] = $desc;

    $events = array($event);
  }
  
  foreach($events as $cur) {
    $entry = array();
    foreach($cur as $k => $v) {
      $str = $k . ':' . $v;

      $lines = array();
      while(strlen($str) > 74) {
        $line = mb_substr($str, 0, 74);
        $llength = mb_strlen($line);  //  must use mb_strlen with mb_substr otherwise will not work things like &nbsp;
        $lines[] = $line.chr(13).chr(10).chr(32); /* CRLF and space*/
        $str = mb_substr($str, $llength); /* set value to what's left of the string */
      }
      if(!empty($str))
        $lines[] = $str; /* the last line does not need a white space */
      $str = implode($lines);
    
      array_push($entry, $str);
    }
    
    $res = array_merge($res, $entry);
  }
  

  $res[] = 'END:VCALENDAR';

  return $res;
}

  
function seminar_ics_feed() {
  ob_get_clean();
  ob_start();
  //header('Content-type: text/html; charset=utf-8');
  header('Content-type: text/calendar; charset=utf-8');
  header('Content-Disposition: inline; filename=calendar.ics');
  ob_clean();

  $posts = array();
  $query = get_seminars(-1, $_REQUEST['id']);
	while($query->have_posts()) {
    $query->the_post();
		array_push($posts, get_post());
  }

  $res = seminar_ics_gen($posts, false);
  
  //echo implode("<br>", $res);
  echo implode("\r\n", $res);
}

add_action('init', function() {
	add_feed('calendar', 'seminar_ics_feed');
  // Only uncomment these 2 lines the first time you load this script, to update WP rewrite rules, or in case you see a 404
  //global $wp_rewrite;
  //$wp_rewrite->flush_rules( false );
});

  
/* send seminars to mailing lists */
function add_send_form($checked, $str) {
  $res = '';
  $options = get_option('tsp_inf_settings');
  $to = seminar_mailing();
  $user = wp_get_current_user();
  $self_mail = $user ? $user->user_email : null;
  
  $force = $_POST['seminar-send'] === 'force';
  
  $query = get_seminars(-1, false, time(), null, 'ASC');

  if($query->have_posts()) {
    $res .= "<div style='width: 100%; margin-top: 1em; margin-bottom: 1em;" .
            "            background-color: lightcyan; border: 1pt solid black;" .
            "            padding-left: 3em; padding-right: 3em; padding-top: 1em; padding-bottom: 1.8em;'>";
    //$res .= "<p style='color: red; text-align: center;'>Don't use that for the moment, I'm doing some test :)</p>";
    $res .= "<p style='color: blue;'>Send an invitation for the seminar(s) of" . ($force ? " (forced mode)" : "") . ":</p>";
    $res .= '<form action="#" method="POST" style="">';
    $res .= wp_nonce_field('seminar-mailing', 'seminar-mailing-wpnonce', true, false);

		while($query->have_posts()) {
      $query->the_post();
			$post = get_post();
      $mail_sent = get_post_meta($post->ID, 'mail_sent', true);
      $can_send = !$mail_sent || $force;

      $res .= '  <label style="" for="post-' . $post->ID . '">';
      $res .= '    <input style="margin-left: 1em; margin-right: 1em; transform: scale(1.2);" . type="checkbox" id="post-' . $post->ID . '" name="post-' . 
              $post->ID . '"' . (!$mail_sent && in_array($post, $checked) ? ' checked' : '') .
              ($can_send ? '' : 'disabled="disabled"') .
              '/>';
      $res .= '<span ' . ($can_send ? '' :  'style="color: grey"') . '>' .
              $post->speaker . ' on ' . format_when($post) . ($mail_sent ? ' (mail already sent)' : '') .
              '</span>';
      $res .=   '</label></br>';
    }

    $res .= '  <div style="display: flex;  flex-wrap: wrap; text-align: center; margin-top: 1em;">';
    $res .= '    <input list="seminar-mails" style="margin-right: 0.2em; flex: auto;" class="input-text" type="mail" name="seminar-mail" value="" />';
    $res .= '    <datalist id="seminar-mails">';
    $res .= '      <option value="' . $to . '">';
    $res .= $self_mail ? '      <option value="' . $self_mail . '">' : '';
    $res .= '    </datalist>';
    $res .= '    <input style="margin-left: 0.2em;"' .
            '           class="submit"" id="submit" type="submit" name="seminar-send" value="send" />';
    $res .= '    <input style="margin-left: 0.2em;"' .
            '           class="submit"" id="submit" type="submit" name="seminar-send" value="force" />';
    $res .= '  </div>';
    if($force) {
      $res .= '  <div style="width: 100%; margin-top: 1em; text-align: center; color: red;">If you fill the erratum text, add an "Erratum" in the subject and a line that contains your erratum text prefixed by "Erratum: " at the beginning of the body</div>';
      $res .= '  <div style="margin-top: 1em; display: flex; flex-wrap: wrap">';
      $res .= '    <input style="flex:auto;" style="width:100%" class="input-text" type="text" name="seminar-erratum" value=""/>';
      $res .= '    <label style="color: blue; margin-right: 1.2em; margin-left: 1.2em; text-align: center;">Erratum text</label>';
      $res .= '  </div>';
    }
    $res .= '</form>';
    //  $res .= "<p style='margin-left: 3em; margin-right: 3em; color: blue; margin-top: 1em; width:'>You can verify that everything is ok by first posting the post to your mail address</p>";

    if($str)
      $res .= '<div style="width: 100%; text-align: center; margin-top: 2em;">' . $str . '</div>';

    $res .= '</div>';
  }

	wp_reset_postdata();

  return $res;
}

function seminar_retrieve_from_form() {
  $res = array();
  $query = get_seminars(-1, false, time(), null, 'ASC');

	while($query->have_posts()) {
    $query->the_post();
		$post = get_post();
    
    if(isset($_POST['post-' . $post->ID]))
      array_push($res, $post);
  }

	wp_reset_postdata();

  return count($res)  ? $res : null;
}

add_action( 'phpmailer_init', function($phpmailer) {
  if(isset($_POST['seminar-send']) && 
    isset($_POST['seminar-mailing-wpnonce']) &&
		wp_verify_nonce($_POST['seminar-mailing-wpnonce'], 'seminar-mailing') &&
    is_user_logged_in()) {

    $user = wp_get_current_user();
    $mail = $user->user_email;

    // change the sender when located at Telecom SudParis and when the user is from Telecom SudParis (we are using the smtp of telecom sudparis)
    if($mail && strpos($mail, "@telecom-sudparis.eu")) {
      $phpmailer->From = $mail;     // Adresse email d'envoi des mails
      $phpmailer->FromName = $user->first_name || $user->last_anme ? $user->first_name . ' ' . $user->last_name : $phpmailer->FromName;
    }
    
    $phpmailer->ClearAttachments();
    $phpmailer->AddStringAttachment(implode("\r\n", seminar_ics_gen(seminar_retrieve_from_form(), true)), 
                                    'invite.ics', 'base64', 'text/calendar');
  }
}, 1000);
  
function seminar_send($to, $posts) {
  global $seminar_types_det;
  global $seminar_types;
  
  $multiple = count($posts) > 1;
  $min_local = false;
  $max_local = false;
  $first_where = false;
  $first_visio = false;
  $erratum = $_POST['seminar-erratum'];
  
  foreach($posts as $post) {
    $start_local = get_when_local($post);
    $end_local = $start_local + get_duration($post);
    if(!$min_local || $start_local < $min_local) {
      $min_local = $start_local;
      $first_where = get_post_meta($post->ID, 'where', true);
      $first_visio = get_post_meta($post->ID, 'visio', true);
    }
    if(!$max_local || $max_local < $end_local)
      $max_local = $end_local;
  }

  $seminar_name = seminar_name();
  $when_str = ceil($min_local / (24*60*60)) == ceil($max_local / (24*60*60)) ? 
              date('j/n/Y \f\r\o\m G:i', $min_local) . ' to ' . date('G:i', $max_local) :
              date('j/n/Y \a\t G:i', $min_local) . ' - ' . date('j/n/Y G:i', $max_local);
  $when_where_str = $when_str . ($first_where ? ' at ' . $first_where : ($first_visio ? ' in visio' : ''));

  $list = '';
  $speakers_subject = array();
  $speakers_body = array();

  foreach($posts as $post) {
    $when_local = get_when_local($post);
    $venue = get_post_meta($post->ID, 'venue', true);
    $title = get_the_title($post);
    $title = $title ? $title : "To be announced";
    $title = $venue ? $title . " (" . $venue . ")" : $title;
    $speaker = get_post_meta($post->ID, 'speaker', true);
    $type = get_post_meta($post->ID, 'type', true);
    $seminar_type = $seminar_types[$type];
    $paper = get_post_meta($post->ID, 'url', true);
    $abstract = get_post_meta($post->ID, 'abstract', true);
    $bio = get_post_meta($post->ID, 'bio', true);

    if($speaker) {
      array_push($speakers_subject, $speaker . ' (' . strtolower($seminar_type) . ')');
      array_push($speakers_body, $speaker . ' will present ' . $seminar_types_det[$type] . ' ' . strtolower($seminar_type) . ' talk' );
    }

    $list .= "\n\n";
    $list .= "# " . ucfirst($seminar_type) . ": " . $title;
    if($multiple || is_seminar_mandatory($post)) {
      if($multiple)
        $list .= "\n\nPresented by " . $speaker . " on " . date('j/n/Y \a\t G:i', $when_local) . ".";
      if(is_seminar_mandatory($post))
        $list .= ($multiple ? " " : "\n\n") . "Attending this presentation is mandatory for the master students.";
    }
    if($paper)
      $list .= "\n\nPaper: " . $paper;
    $list .= "\n\nFull post: " . get_permalink($post);
    if($abstract)
      $list .= "\n\n## Abstract\n" . preg_replace("/[\n]+/", "", strip_tags($abstract));
    if($bio)
      $list .= "\n\n## Bio\n" . preg_replace("/[\n]+/", "", strip_tags($bio));
  }

  $speakers_subject = natural_list($speakers_subject);
  $speakers_body = natural_list($speakers_body);

  $subject = '[' . $seminar_name . '] ' . ($erratum ? 'Erratum: ' : '') . $when_where_str . " - " . $speakers_subject;

  $body = "";
  $body .= $erratum ? "Erratum: " . $erratum . "\n===================\n" : "";
  $body .= "Dear all,";
  $body .= "\n\nDuring the " . $seminar_name . " of " . $when_str . ", " . $speakers_body . ".";
  $body .= $first_visio ? "\n\nVisio: " . $first_visio : "";
  $body .= $first_where ? "\n\nLocation: " . $first_where : "";
  $body .= $list;
  $body .= "\n\nSee you soon,\nThe " . $seminar_name . " organizing committee";
  $body .= "\n\n===========================\nIf you don't want to receive the " . $seminar_name . 
           " posts anymore, please contact " . seminar_sender();

  //echo 'subject: ' . $subject . '</br>';
  //echo 'body: ' . preg_replace("/\n/", "<br>", $body) . '</br>';
  if(wp_mail($to, $subject, $body)) {
    foreach($posts as $post)
	    update_post_meta($post->ID, 'mail_sent', true);
    return true;
  } else {
    return false;
  }
}

function add_seminar_form($post=null) {
  $res = '';
  if(current_user_can('publish_posts')) {
    $is_post_form = $_POST['seminar-send'] === 'send' && 
                    isset($_POST['seminar-mailing-wpnonce']) && 
                    wp_verify_nonce($_POST['seminar-mailing-wpnonce'], 'seminar-mailing');

    $str = '';

    if($is_post_form) {
      $checked = seminar_retrieve_from_form();
      $to = $_POST['seminar-mail'];
      if($checked && $to && seminar_send($to, $checked))
        $str = '<span style="color: blue">Mail successfully sent to ' . $to . '</span>';
      else
        $str = '<span style="color: red">Unable to send the mail to ' . $to . '</span>';
    }

    $res .= add_send_form($checked ? $checked : array($post), $str);
	}

  return $res;
}
  
add_filter('the_content', function($content) {
  global $post;
  if ($post->post_type == 'seminar_cpt') {
    $content .= format_seminar_full($post);
  }
  return $content;
});

  
function onMailError( $wp_error ) {
  echo "<pre>";
  print_r($wp_error);
  echo "</pre>";
}
//add_action( 'wp_mail_failed', 'onMailError', 10, 1 ); // debug
  
?>
