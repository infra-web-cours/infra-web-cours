<?php

/* logotech */
// Creating the widget
// See wp.css for the style
class logos_widget extends WP_Widget {
  // The construct part  
  function __construct() {
    parent::__construct('logos_widget', 
                        'Logos widget',
												array('description' => __('Adds the corporate logos of IP Paris')));
  }
  
  // Creating widget front-end
  public function widget( $args, $instance ) {
    echo '<div class="logos-container"><div class="logos">';
    foreach(array(array('ipparis', 100),
                  array('x', 75),
                  array('ensae', 100),
                  array('ensta', 92),
                  array('tp', 92),
                  array('tsp', 92),
                  array('inria', 100),
                  array('cnrs', 100)) as $cur) {
      $url = plugin_dir_url(__FILE__) . '/logos/' . $cur[0] . '.png';
      echo '<div class="logo-' . $cur[0] . '"><a href="' . $url . '"><img style="width: ' . $cur[1] . '%" src="' . $url . '" alt="?"></img></a></div>';
//      echo '<div class="logo-' . $cur[0] . '"><a href="' . $url . '"><img style="height: fit-content" src="' . $url . '" alt="?"></img></a></div>';
    }
    echo "</div></div>";
  }

  // Creating widget Backend 
  public function form( $instance ) {
    return '';
  }

  // Updating widget replacing old instances with new
  public function update( $new_instance, $old_instance ) {
    return array();
  }
}

add_action('widgets_init', function() { register_widget('logos_widget'); });

?>
