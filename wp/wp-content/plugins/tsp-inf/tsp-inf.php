<?php
  /**
  * Plugin Name: CS Departement of TSP
   * Plugin URI: 
   * Description: This plugin is used by the Computer Science Department of Telecom SudParis/IP Paris
   * Version: 1.0
   * Author: Gaël Thomas
   * Author URI: 
   */

if(!defined('PLUGIN_DIR'))
  define( 'PLUGIN_DIR', dirname(__FILE__).'/' );  

include 'fix-user-edit.php';
include 'members.php';
include 'logos.php';

add_action( 'phpmailer_init', function($phpmailer) {
  $phpmailer->isSMTP();
  $phpmailer->Host = 'smtp1.int-evry.fr';
  $phpmailer->SMTPAuth = false; // Indispensable pour forcer l'authentification
  $phpmailer->Port = 25;
  // Configurations complémentaires
  // $phpmailer->SMTPSecure = "ssl"; // Sécurisation du serveur SMTP : ssl ou tls

  $phpmailer->From = "gael.thomas@telecom-sudparis.eu";
  $phpmailer->FromName = "WordPress - INF";
}, 10);

// Add scripts and stylesheets
	//wp_enqueue_style('wp-style', get_template_directory_uri() . '/wp.css', array('style') );

add_action( 'wp_enqueue_scripts', function() {
	wp_enqueue_style('wp-style', plugin_dir_url(__FILE__) . '/wp.css', array() );
});

//add_filter('option_blogdescription', function($value) {
//  $url = get_theme_mod('tagline_link') ? get_theme_mod('tagline_link') : network_home_url();
//  return '<a class="tagline" href="' . $url . '">' . wp_load_alloptions()['blogdescription'] . '</a>';
//});
  
?>
