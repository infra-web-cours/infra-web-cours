<?php

function cleanup_string($str) {
  return trim(preg_replace('/\s+/', ' ', $str));
}

function capitalize($str) {
  return str_replace('¿ ', '-', ucwords(str_replace('-', '¿ ', strtolower($str))));
}

function all_roles() {
  global $wp_roles;
  return $wp_roles->get_names();
}

class Group {
  public $group_name;
  public $user;
  public $blog;
  public $is_member;
  public $is_modifiable;
  public $role;
  public $role_changed;
  
  public function __construct($user, $group_name, $blog, $is_member, $is_modifiable) {
    $this->user = $user;
    $this->group_name = $group_name;
    $this->blog = $blog;
    $this->is_member = $this->group_name === 'main' || $is_member;
    $this->is_modifiable = $is_modifiable;

    $this->role_changed = false;
    switch_to_blog($this->blog->blog_id);
    $roles = get_userdata($this->user->wpuser->ID)->roles;
    restore_current_blog();
    
    switch(count($roles)) {
      case 0: $this->role = ''; break;
      case 1: $this->role = $roles[0]; break;
      default:
        $all = all_roles();
        foreach($all as $k => $v)
          if(!$this->role && in_array($k, $roles))
            $this->role = $k;
    }
  }

  public function is_admin() {
    return $this->role === 'administrator';
  }

  public function is_author() {
    switch($this->role) {
      case 'administrator':
      case 'editor':
      case 'author':
        return true;
      default:
        return false;
    }
  }

  public function is_member() {
    return $this->role;
  }
  
  public function save() {
    if($this->role_changed) {
      remove_user_from_blog($this->user->wpuser->ID, $this->blog->blog_id);
      if(!empty($this->role))
        add_user_to_blog($this->blog->blog_id, $this->user->wpuser->ID, $this->role);
    }
  }
  
  public function set_role($role) {
    $this->role = $role;
    $this->role_changed = true;
  }
  
  public function update_membership($group_set) {
    if($this->is_modifiable)
      $this->is_member = in_array($this->group_name, $group_set);
  }

  public function html_form_role_id() {
    return $this->user->uid . '-role-' . $this->group_name;
  }
  
  public function role_as_select_list($enable=true) {
    $enable = $enable ? '' : ' disabled';
    $res = '<select name="' . $this->html_form_role_id() . '">';
    $res .= '<option' . $enable . ' value=""' . ($this->role ? '' : ' selected') . '></option>';
    foreach(all_roles() as $id => $str)
      $res .= '<option' . $enable . ' value="' . $id . '"' . ($this->role === $id ? ' selected' : '') .  '>' . $str . '</option>';
    $res .= '</select>';
    return $res;
  }
  
  public function as_checkbox($enable=true) {
    return '<input type="checkbox" id="' . $this->user->uid . '-' . $this->group_name . 
           '" name="' . $this->user->html_form_group_id() . '[]" value="' . $this->group_name . '"' .
            ($this->is_member ? ' checked' : '') . ($enable ? '' : ' disabled') . '>';
  }
  
  public function as_label() {
    return '<label for="' . $this->user->uid . '-' . $this->group_name . '">' . $this->group_name . '</label>';
  }
  
  public function saved_value() {
    return $this->is_member ? $this->group_name : '';
  }
}

class TSPUser {
  public $state = array();
  public $uid;
  public $wpuser;
  public $first_name;
  public $last_name;
  public $mail;
  public $web;
  public $location;
  public $office;
  public $ldap_account;
  public $jpeg;
  public $url_photo;
  public $positions;
  public $groups;

  public static function load($wpuser, $groups_filter=null) {
    $res = new TSPUser($wpuser->user_login);
    $res->wpuser = $wpuser;
    $res->post_load($groups_filter);
    return $res;
  }
  
  public function __construct($uid) {
    $this->uid = $uid;
  }

  public function load_and_merge($groups_filter=null) {
    $this->wpuser = get_user_by('login', $this->uid);
    if($this->wpuser)
      $this->post_load($groups_filter);
    return $this;
  }
  
  private function merge_value($new_val, $old_val) {
    return $new_val ? $new_val : $old_val;
  }
  
  private function post_load($groups_filter) {
    foreach(explode(' ', cleanup_string(get_the_author_meta('state', $this->wpuser->ID))) as $key)
      $this->add_state($key);

    $this->first_name = $this->merge_value($this->wpuser->first_name, $this->first_name);
    $this->last_name = $this->merge_value($this->wpuser->last_name, $this->last_name);
    $this->mail = $this->merge_value($this->wpuser->user_mail, $this->mail);
    $this->web = $this->merge_value($this->wpuser->user_url, $this->web);
    $this->phone = $this->merge_value(get_the_author_meta('phone', $this->wpuser->ID), $this->phone);
    $this->location = $this->merge_value(get_the_author_meta('location', $this->wpuser->ID), $this->location);
    $this->office = $this->merge_value(get_the_author_meta('office', $this->wpuser->ID), $this->office);
    $this->url_photo = get_the_author_meta('photo', $this->wpuser->ID);
    $position = get_the_author_meta('position', $this->wpuser->ID);
    $this->positions = $position ? array($position) : $this->positions;
    
    $group_set = explode(' ', cleanup_string(get_the_author_meta('groups', $this->wpuser->ID)));
    $this->groups = array();
    foreach(get_sites(array('fields' => null)) as $blog) {
      if(!$blog->archived && !$blog->deleted) {
        $group_name = str_replace('/', '', $blog->path);
        $group_name = $group_name ? $group_name : 'main';
        $this->groups[$group_name] = new Group($this,
                                               $group_name,
                                               $blog,
                                               in_array($group_name, $group_set),
                                               !$groups_filter || in_array($group_name, $groups_filter));
      }
    }
    
    return $this;
  }

  public function html_form_group_id() {
    return $this->uid . '-groups';
  }
  
  public function set_groups($group_set) {
    foreach($this->groups as $cur)
      $cur->update_membership($group_set);
  }
  
  public function sort_key() {
    return $this->last_name . ' ' . $this->first_name . ' (' . $this->uid . ')';
  }

  public function full_name() {
    return $this->first_name . ' ' . $this->last_name;
  }

  public function full_location() {
    if($this->office)
      return $this->office . ($this->location ? ' ' . $this->location : '');
    else if($this->location)
      return $this->location;
    else
      return null;
  }

  public function import() {
    if(!$this->wpuser) {
      echo "importing " . $this->uid . ' - ' . $this->first_name . ' - ' . $this->last_name . ' - ' . $this->mail . '<br>';
      if(wp_create_user($this->uid, wp_generate_password(20), $this->mail)) {
        $this->load_and_merge();
        if($this->wpuser) {
          echo "update update: " . $this->wpuser->ID . "<br>";
          wp_update_user(array('ID' => $this->wpuser->ID,
                               'first_name' => $this->first_name,
                               'last_name' => $this->last_name));
        }
      }
    }
  }
  
  public function save() {
    if(!$this->wpuser)
      echo "WARNING: try to save a non-wp members";
    else {
      update_user_meta($this->wpuser->ID, 'state', $this->state_to_string());
      update_user_meta($this->wpuser->ID, 'position', implode(' ', $this->positions));
      update_user_meta($this->wpuser->ID, 'phone', $this->phone);
      update_user_meta($this->wpuser->ID, 'photo', $this->url_photo);
      update_user_meta($this->wpuser->ID, 'location', $this->location);
      update_user_meta($this->wpuser->ID, 'office', $this->office);
      update_user_meta($this->wpuser->ID, 'groups', implode(' ', array_map(function($g) { return $g->saved_value(); }, $this->groups)));

      foreach($this->groups as $group)
        $group->save();
    }
  }

  public function has_state($key) { return array_key_exists($key, $this->state); }
  public function add_state($key) { $this->state[$key] = $key; }
  public function remove_state($key) { unset($this->state[$key]); }
  public function set_state($key, $val) { if($val) $this->add_state($key); else $this->remove_state($key); return $this; }

  public function set_known($val=true) { return $this->set_state('known', $val); }
  public function set_alumni($val=true) { return $this->set_state('alumni', $val); }
  public function set_cnr($val=true) { return $this->set_state('cnr', $val); }
  public function set_invited($val=true) { return $this->set_state('invited', $val); }
  public function set_adjunct($val=true) { return $this->set_state('adjunct', $val); }
  public function set_director($val=true) { return $this->set_state('director', $val); }
  public function set_secretary($val=true) { return $this->set_state('secretary', $val); }
  public function set_hide_photo($val=true) { return $this->set_state('hide-photo', $val); }
  public function set_hide_user($val=true) { return $this->set_state('hide-user', $val); }
  public function set_hide_phone($val=true) { return $this->set_state('hide-phone', $val); }
  public function set_hide_office($val=true) { return $this->set_state('hide-office', $val); }
  public function set_hide_mail($val=true) { return $this->set_state('hide-mail', $val); }
  
  public function is_known() { return array_key_exists('known', $this->state); }
  public function is_alumni() { return array_key_exists('alumni', $this->state); }
  public function is_cnr() { return array_key_exists('cnr', $this->state); }
  public function is_invited() { return array_key_exists('invited', $this->state); }
  public function is_adjunct() { return array_key_exists('adjunct', $this->state); }
  public function is_director() { return array_key_exists('director', $this->state); }
  public function is_secretary() { return array_key_exists('secretary', $this->state); }
  public function is_hide_photo() { return array_key_exists('hide-photo', $this->state); }
  public function is_hide_user() { return array_key_exists('hide-user', $this->state); }
  public function is_hide_phone() { return array_key_exists('hide-phone', $this->state); }
  public function is_hide_office() { return array_key_exists('hide-office', $this->state); }
  public function is_hide_mail() { return array_key_exists('hide-mail', $this->state); }

  function is_blogadmin() {
    $caller = rua_get_user($this->wpuser->ID);
    $level = rua_get_level_by_name('blogadmins');
    return $caller->has_level($level->ID);
  }

  function set_blogadmin($is_grant) {
    $caller = rua_get_user($this->wpuser->ID);
    $level = rua_get_level_by_name('blogadmins');
    if($is_grant)
      $caller->add_level($level->ID);
    else
      $caller->remove_level($level->ID);
  }

  function is_megaadmin() {
    return $this->groups['main']->is_admin();
  }
  
  function is_admin($group) {
    return $this->groups[$group->group_name]->is_admin();
  }

  function cmessage($constrain, $msg) {
    return '<li>[<span style="color: red">' . $constrain . '</span>] ' . $this->sort_key() . ': ' . $msg . '</li>';
  }
  
  function check_constrains($constrains) {
    $res = '';
    foreach($this->groups as $group) {
      if($group->is_member && empty($group->role)) {
        $res .= $this->cmessage("in group => in blog", "added to the " . $group->group_name . " blog");
        $group->set_role('subscriber');
      }
      
      if($group->is_author())
        foreach($constrains[$group->group_name] as $group_name) {
          $target = $this->groups[$group_name];
          if($target->is_member && !$target->is_author()) {
            $res .= $this->cmessage("author in " . $group->group_name . " => in " . $group_name,
                                    "change role to author in the " . $group_name . " blog");
            $target->set_role('author');
          }
        }
    }
    return $res;
  }
  
  public function state_to_string() {
    return implode(' ', $this->state);
  }
  
  public function regen() {
    $res = '$u = new TSPUser("' . $this->uid . '");<br>';

    foreach($this as $key => $value) {
      if(is_null($value))
        $regen_value = 'null';
      else if(is_bool($value))
        $regen_value = $value ? 'true' : 'false';
      else if(is_array($value)) {
        $regen_value = 'array(';
        foreach($value as $k2 => $v2)
          $regen_value .= '"' . $k2 . '" => "' . $v2 . '", ';
        $regen_value .= ')';
      } else {
        $regen_value = '"' . $value . '"';
      }

        
      $res .= '$u->' . $key . ' = ' . $regen_value . ';<br>';
    }

    return $res;
  }
    
  public function __toString() {
    return sort_key();
  }
}

?>
