<?php

include 'user.php';
include 'ldap-members.php';
include 'wp-members.php';

add_action('wp_print_styles', function() {
	wp_register_style('fontawesome', 'https://use.fontawesome.com/releases/v5.10.0/css/all.css');
	wp_enqueue_style( 'fontawesome');
});

function do_fa_icon($name) {
  return '<span style="display: inline-block; text-align: center; font-size: 0.9em; width: 1.4em;"><i class="fa ' . $name . '" aria-hidden="true"></i></span>';
}

function categories() {
  return array('dir'      => 'Direction',
               'faculty'  => 'Faculty members',
               'invited'  => 'Invited researchers',
               'admin'    => 'Administrative staff',
               'engineer' => 'Engineers',
               'cdd'      => 'Postdocs and temporary positions',
               'visiting' => 'Visiting',
               'phd'      => 'PhD students',
               'intern'   => 'Master students',
               'cnr'      => 'Former faculty members',
               'alumni'   => 'Alumnis',
               'other'    => 'Others',
               'unknow'   => 'Unknow');
}

function get_members($department, $simulate, $filter, $group, $import, $additional, $config=null) {
  $filter = merge_filter($filter, $group);

  $dump = $simulate ? ldap_get_members_simulate($filter) : ldap_get_members($department, $filter, $additional, $config);

 if($dump || $group || $additional) {
    $members = $filter ? $filter : array_keys($dump);
    $members = $additional ? array_merge($additional, $members) : $members;
    $dump = wp_get_members($dump ? $dump : array(), $members, $import);
  }

  return $dump;
}

function prepare_members($dump, $with_structure, $show_alumni, $show_cnr) {
  $sorted = array();
  $manager = array();
  $secretary = array();
  
  foreach($dump as $u) {
    $k = $u->sort_key();
    if($with_structure && $u->is_director())
	    $manager[$k] = $u;
	  else if($with_structure && $u->is_secretary())
	    $secretary[$k] = $u;
	  else
	    $sorted[$k] = $u;
  }
  
  ksort($sorted);
  ksort($manager);
  ksort($secretary);
  
  $res = array();

  foreach(categories() as $cat => $str)
    $res[$cat] = array();
  
  foreach($manager as $cur)
	  array_push($res['dir'], $cur);
  foreach($secretary as $cur)
	  array_push($res['dir'], $cur);
  
  foreach($sorted as $u) {
    if($u->is_cnr()) {
      if($show_cnr)
        array_push($res['cnr'], $u);
    } else if($u->is_adjunct() || in_array('Visiting professor', $u->positions)) {
      array_push($res['visiting'], $u);
    } else if($u->is_invited()) {
      array_push($res["invited"], $u);
    } else if($show_alumni && $u->is_alumni())
      array_push($res['alumni'], $u);
    else if(in_array('Associate professor', $u->positions) || in_array('Professor', $u->positions))
      array_push($res['faculty'], $u);
    else if(in_array('Research engineer', $u->positions))
      array_push($res['engineer'], $u);
    else if(in_array('Post-doc researcher', $u->positions) || in_array('Assistant professor', $u->positions))
      array_push($res['cdd'], $u);
    else if(in_array('PhD student', $u->positions))
      array_push($res['phd'], $u);
    else if(in_array('Master student', $u->positions))
      array_push($res['intern'], $u);
    else if(in_array('Department assistant', $u->positions))
      array_push($res['admin'], $u);
    else
      array_push($res['unknow'], $u);
  }
  
  return $res;
}

function ldap_gencss($dump) {
  $res .= '<br><br><br>';
  $res .= ".members { display: grid; grid-template-columns: 1fr; grid-template-areas: " .
          implode(" ", array_map(function($cat) { return "'cat-" . $cat . "-ldap' 'cat-" . $cat . "-other'"; },
                                 array_keys(categories()))) . "; }<br>";
  foreach(categories() as $cat => $str) {
    $res .= ".cat-" . $cat . "-ldap { grid-area: cat-" . $cat . "-ldap; }<br>";
    $res .= ".cat-" . $cat . "-other { grid-area: cat-" . $cat . "-other; }<br>";
  }

  return $res;
}

function do_fa_url($url, $id) {
  return '&nbsp;<a href="' . $url . '">' . do_fa_icon($id) . '</a>';
}

function picture_url($u, $width='80em') {
  $subdir = '/wp-content/uploads/pictures';
  $base_url = network_site_url() . $subdir;
  
  $url = plugin_dir_url(__FILE__) . 'unknow.jpg';

  //if($u->uid === 'thomas_g' && $u->jpeg)
  //echo "jpeg!<br>";
  
  if($u->is_hide_photo())
    $url = plugin_dir_url(__FILE__) . 'anonymous.jpg';
  else if($u->url_photo)
    $url = $u->url_photo;
  else if($u->ldap_account) {
    $file_name = '/' . $u->uid . '.jpg';
  
    $upload_dir = ABSPATH . $subdir;
  
    if(!file_exists($upload_dir))
      wp_mkdir_p($upload_dir);

    $file = $upload_dir . $file_name;
  
    $orig_url = "https://trombi.imtbs-tsp.eu/photo.php?uid=" . $u->uid;
    // recreate the file between 1 and 2 days
    $day = 60*60*24;
    $limit = rand(1*$day, 2*$day);

    $download_url = $base_url . $file_name;
    
    if(file_exists($file) && time() - filemtime($file) < $limit)
      $url = $download_url;
    else {
      if($u->jpeg) {
        file_put_contents($file, $u->jpeg);
        $url = $download_url;
      } else {
        $c = curl_init($orig_url);

        if($c) {
          //echo "reload " . $file . "<br>";//(time() - filemtime($file)) . "<br>";
          $fp = fopen($file, "w");
          if($fp) {
            curl_setopt($c, CURLOPT_FILE, $fp);
            curl_setopt($c, CURLOPT_COOKIEFILE, '/dev/null');
            curl_setopt($c, CURLOPT_FOLLOWLOCATION	, true);
            curl_setopt($c, CURLOPT_SSL_CIPHER_LIST, 'DEFAULT:!DH');

            $s = curl_exec($c);

            if($s)
              $url = $download_url;
          }
        }
      }
    }
  }

  return $url;
}

function make_picture($u, $width='80em') {
  $url = picture_url($u, $width);
  $img = '<img style="width: 5em; min-width:5em;" src="' . $url . '">';
  return '<a href="' . $url . '">' . $img . '</a>';
}

function display_members($dump, $show_positions, $show_icons, $show_uid, $show_pictures) {
  $c = 0;
  
  foreach($dump as $cat => $list) {
    if(!empty($list)) {
      $open = false;
      
      foreach($list as $u) {
        if(!$u->is_hide_user()) {
          if(!$open) {
            $open = true;
            $res .= '<div ' . ($show_pictures ? 'style="margin-bottom: 1em" ' : '') . 'class="cat-' . $cat . '-ldap">';
            $res .= '<h3>' . categories()[$cat] . '</h3>';
            if(!$show_pictures)
              $res .= '<ul>';
            else
              $res .= '<div style="display: grid; grid-gap: 0.5em; grid-template-columns: repeat(auto-fill, 22em);">';
          }

          if($show_pictures)
            $res .= '<div style="width: 22em; display: flex; align-items: center;">' .
                    '<div style="margin-right: 1em;">' . make_picture($u) . '</div>' . '<div>';
          else
            $res .= '<li>';
          
	        $name = $u->full_name();
	        if($u->web)
	          $name = '<a href="' . $u->web . '">' . $name . '</a>';
	        $res .= $name;

          if($show_uid)
            $res .= ', ' . $u->uid;

	        if($show_pictures || ($show_positions && in_array($cat, array("dir", "faculty", "other", "unknow")))) {
            if($show_pictures)
              $res .= '<br>';
            else
              $res .= ', ';
            $p = $u->is_adjunct() ? array_map(function($p) { return 'Visiting ' . strtolower($p); }, $u->positions) : $u->positions;
	          $res .= $cat === 'engineer' ? "Research engineer" : implode(', ', $p);
          }

          if(!$u->is_hide_office()) {
            $loc = $u->full_location();
            if($loc) {
              if($show_pictures)
                $res .= '<br>' . $loc;
              else if($show_icons)
	              $res .= '<span id="loc-' . $u->uid . '" class="hide-toggle">, ' . $loc . '</span>';
            }
          }
          
	        if($show_icons) {
            if($u->location && !$show_pictures && !$u->is_hide_office())
	            $res .= '&nbsp;<span class="as-link" onclick="document.getElementById(\'loc-' .
                      $u->uid . '\').classList.toggle(\'active\')">' .
                      do_fa_icon('fa-map-marker-alt') . '</span>';

            if(!$show_pictures && !$u->is_hide_photo()) {
              $url = picture_url($u);
              if($url)
                $res .= do_fa_url($url, 'fa-id-badge');
            }
            
	          if($u->mail && !$u->is_hide_mail())
	            $res .= do_fa_url('mailto:' . str_replace('@', '_at_', $u->mail), 'fa-envelope');

	          if($u->phone && ! $u->is_hide_phone())
	            $res .= do_fa_url('tel:' . $u->phone, 'fa-phone');
	        }

	        if($show_pictures)
            $res .= "</div></div>";
          else
	          $res .= '</li>';
        }
	    }
      
      if($open) {
        if(!$show_pictures)
          $res .= '</ul>';
        else
          $res .= '</div>';
        $res .= '</div>';
      }
    }
  }
  
  return $res;
}

function as_trombi($dump) {
  $n = 3;
  $res = '';
  foreach(prepare_members($dump, false, true) as $cat => $members) {
    if(!empty($members)) {
      $res .= '<h3>' . categories()[$cat] . '</h3>';
      $res .= '<table class="center borderless" style="line-height: 1.3em;">';
      $picture = true;
  
      for($i=0; $i<count($members); $picture=!$picture) {
        $res .= '<tr>';
        for($j=0; $j<$n; $j++) {
          $k = $i + $j;
          $res .= '<td style="' . ($picture ? '' : 'padding-bottom: 1.3em; width: 6em;') . '">' .
                         ($k < count($members) ?
                          ($picture ? make_picture($members[$k]) : $members[$k]->first_name . '<br>' . $members[$k]->last_name) :
                          '') . '</td>';
        }
        $res .= '</tr>';
        if(!$picture)
          $i += $n;
      }

      $res .= '</table>';
    }
  }

  return $res;
}

add_shortcode('tspmembers', function($a) {
	$attrs = shortcode_atts( array(
    'department' => null,
    'filter' => null,
    'show-direction' => 'false',
    'simulate' => 'false',
    'show-position' => 'true',
    'show-icons' => 'true',
    'show-uid' => 'false',
    'show-picture' => 'false',
    'show-cnr' => true,
    'show-alumnis' => true,
    'group' => false,
    'import' => false,
    'additional' => null,
    'as-trombi' => false
  ), $a );

  $department = $attrs['department'];
  $filter = is_null($attrs['filter']) ? null : explode(' ', $attrs['filter']);
  $additional = is_null($attrs['additional']) ? null : explode(' ', $attrs['additional']);
  $show_picture = filter_var($attrs['show-picture'], FILTER_VALIDATE_BOOLEAN);
  $import = filter_var($attrs['import'], FILTER_VALIDATE_BOOLEAN);
  $group = $attrs['group'];
  $self = TSPUser::load(wp_get_current_user());

  if(empty($department) && empty($filter) && empty($group)) {
    if($self->is_blogadmin()) {
      if($show_picture || $import)
        return "Sorry, cannot show the pictures of all TSP and cannot import all TSP in the database";
    } else {
      return "Please specify either a department, a filter or a group";
    }
  }
  
  $dump = get_members($department,
                      filter_var($attrs['simulate'], FILTER_VALIDATE_BOOLEAN),
                      $filter,
                      $group,
                      $import,
                      $additional);
  
  if(!$dump)
    return "Unable to connect to database";
  else if(filter_var($attrs['as-trombi'], FILTER_VALIDATE_BOOLEAN))
    return as_trombi($dump);
  else
    return display_members(prepare_members($dump,
                                           filter_var($attrs['show-direction'], FILTER_VALIDATE_BOOLEAN),
                                           filter_var($attrs['show-alumnis'], FILTER_VALIDATE_BOOLEAN),
                                           filter_var($attrs['show-cnr'], FILTER_VALIDATE_BOOLEAN)),
                           filter_var($attrs['show-position'], FILTER_VALIDATE_BOOLEAN),
                           filter_var($attrs['show-icons'], FILTER_VALIDATE_BOOLEAN),
                           filter_var($attrs['show-uid'], FILTER_VALIDATE_BOOLEAN),
                           $show_picture);
});
    
function parse_constrains($constrains_str) {
  $constrains = array();
  foreach(explode(',', $constrains_str) as $c) {
    $constrain = array();
    foreach(explode("=>", $c) as $list_str) {
      $list = array();
      foreach(explode(' ', cleanup_string($list_str)) as $blog_name)
        $list[$blog_name] = $blog_name;
      array_push($constrain, $list);
    }

    foreach($constrain[0] as $blog_from)
      $constrains[$blog_from] = $constrain[1];
  }

  /* transitive closure */
  for($cont=true; $cont; ) {
    $cont = false;
    foreach($constrains as $from => $tos) {
      foreach($tos as $to) {
        if(count(array_intersect($constrains[$from], $constrains[$to])) != count($constrains[$to])) {
          $constrains[$from] += $constrains[$to];
          $cont = true;
        }
      }
    }
  }

  return $constrains; 
}

function admin_get_members($groups_filter, $simulate) {
  $users = array();
  foreach(get_users(array('blog_id' => 0)) as $user)
    if($user->user_login !== 'admin') {
      $u = TSPUser::load($user, $groups_filter);
      $users[$u->uid] = $u;
    }
  
  if(!$simulate) {
    $dump = ldap_get_members(null, array_keys($users), null);
    foreach($dump as $u)
      if(array_key_exists($u->uid, $users))
        $users[$u->uid]->ldap_account = true;
  }

  usort($users, function($a, $b) { return $b->sort_key() < $a->sort_key(); });
  
  return $users;
}

add_shortcode('tspadmin', function($a) {
	$attrs = shortcode_atts(array(
    'simulate' => false,
    'constrains' => 'pds dissem diego => acmes, acmes => main',
    'blogs' => 'main pds dissem diego acmes',
  ), $a);

  $self = TSPUser::load(wp_get_current_user());

  if(!$self->is_blogadmin())
    return "Sorry, you don't have the blogadmins level";

  
  $constrains = parse_constrains($attrs['constrains']);
  $users = admin_get_members(explode(' ', cleanup_string($attrs['blogs'])), filter_var($attrs['simulate'], FILTER_VALIDATE_BOOLEAN));
  
  $is_post = $_POST['tsp-admin'] === 'submit' && 
             isset($_POST['tsp-admin-wpnonce']) && 
             wp_verify_nonce($_POST['tsp-admin-wpnonce'], 'tsp-admin');

  $res = '';
  $res .= '<form action="#" method="POST">';
  $res .= wp_nonce_field('tsp-admin', 'tsp-admin-wpnonce', true, false);
  $res .= '<div style="width: 100%; text-align: center; margin-bottom: 2em;">' .
          '<input id="submit" type="submit" name="tsp-admin" value="submit" />' .
          '</div>';
  
  $before = '<ul>';

  foreach($users as $u) {
    if($is_post) {
      $known = $_POST['known-user-' . $u->uid] === 'true';
      if($known != $u->is_known()) {
        $u->set_known($known);
        $before .= '<li>Change state of ' . $u->sort_key() . ' to ' . ($known ? "known" : "unknown") . '</li>';
      }

      $blogadmin = $_POST['blogadmin-' . $u->uid] === 'true';
      if($self->is_megaadmin() && $blogadmin != $u->is_blogadmin()) {
        $u->set_blogadmin($blogadmin);
        $before .= '<li>' . $u->sort_key() . ': ' . ($blogadmin ? 'marked' : 'unmarked') . ' as blog administrator</li>';
      }

      foreach($u->groups as $group)
        if($self->is_admin($group) && $group->is_modifiable) {
          $is_member = $group->group_name === 'main' || in_array($group->group_name, $_POST[$u->html_form_group_id()]);
          if($group->is_member != $is_member) {
            $before .= '<li>' . $u->sort_key() . ': ' . ($is_member ? 'added to' : 'removed from') .
                       ' the ' . $group->group_name . ' group</li>';
            $group->is_member = $is_member;
          }
          $role = $_POST[$group->html_form_role_id()];
          if($group->role !== $role) {
            $before .= '<li>' . $u->sort_key() . ': ' . ($role ? 'change role to ' . $role . ' in' : 'removed from') .
                       ' the ' . $group->group_name . ' blog</li>';
            $group->set_role($role);
          }
        }

      $before .= $u->check_constrains($constrains);
      $u->save();
    }
    
    $res .= '<table class="max-width center" style="border: none;">';
    $res .= '<tr><th style="border: none; height: 2em;" colspan="' . (count($u->groups) + 1) . '">';
    $res .= '<span style="color: ' . ($u->is_known() ? ($u->ldap_account ? 'blue' : 'magenta') : 'red') . '">' .
            $u->full_name() . ' (' . $u->uid . ($u->ldap_account ? ', ldap user' : ', local user') . ')</span>';
    $res .= '</th></tr>';

    $res .= '<tr>';
    $res .= '<td style="border: none;"></td>';
    foreach($u->groups as $group)
      if($group->is_modifiable)
        $res .= '<td style="color: ' . ($self->is_admin($group) ? '' : 'grey') . '">' . $group->group_name . '</td>';
    $res .= '</tr>';

    $res .= '<tr>';
    $res .= '<td>Groups</td>';
    foreach($u->groups as $group)
      if($group->is_modifiable)
        $res .= '<td>' . ($group->group_name === 'main' ? '' : $group->as_checkbox($self->is_admin($group))) . '</td>';
    $res .= '</tr>';

    $res .= '<tr>';
    $res .= '<td>' . 'Roles' . '</td>';
    foreach($u->groups as $group)
      if($group->is_modifiable)
        $res .= '<td>' . $group->role_as_select_list($self->is_admin($group)) . '</td>';
    $res .= '</tr>';

    $res .= '<tr>';
    $res .= '<td colspan=3 style="color: ' . ($self->is_megaadmin() ? '' : 'grey') . '">Blog admin ' .
            '<input style="margin-left: 0.5em;" type="checkbox" name="blogadmin-' . $u->uid . '" value="true"' .
             ($u->is_blogadmin() ? ' checked' : '') . ($self->is_megaadmin() ? '' : 'disabled') . '/>';
    $res .= '<td colspan=3>Known user ' .
            '<input style="margin-left: 0.5em;" type="checkbox" name="known-user-' . $u->uid . '" value="true"' .
             ($u->is_known() ? ' checked' : '') . '/>';
    $res .= '</tr>';
    
    $res .= '</table>';
  }

  $res .= '</form>';
  $before .= '</ul>';
  $res = $before . $res;
  
  if($is_post)
    $res = '<p style="text-align: center; color: blue">Database updated successfully</p>' . $res;
  
  return $res;
});

?>
