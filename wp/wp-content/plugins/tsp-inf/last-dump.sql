<?php
$dump = array();

$u = new TSPUser("simatic");
$u->state = array();
$u->uid = "simatic";
$u->wpuser = null;
$u->first_name = "Michel";
$u->last_name = "Simatic";
$u->mail = "michel.simatic@telecom-sudparis.eu";
$u->web = "http://www-public.it-sudparis.eu/~simatic/";
$u->location = "Evry";
$u->office = "B310-01";
$u->ldap_account = true;
$u->url_photo = null;
$u->positions = array("0" => "Professor", );
$u->phone = "+33160764561";
$dump["simatic"] = $u;

$u = new TSPUser("taconet");
$u->state = array();
$u->uid = "taconet";
$u->wpuser = null;
$u->first_name = "Chantal";
$u->last_name = "Taconet";
$u->mail = "chantal.taconet@telecom-sudparis.eu";
$u->web = "http://www-public.int-evry.fr/~taconet";
$u->location = "Evry";
$u->office = "B302";
$u->ldap_account = true;
$u->url_photo = null;
$u->positions = array("0" => "Associate professor", );
$u->phone = "+33160764592";
$dump["taconet"] = $u;

$u = new TSPUser("lallet");
$u->state = array();
$u->uid = "lallet";
$u->wpuser = null;
$u->first_name = "Eric";
$u->last_name = "Lallet";
$u->mail = "eric.lallet@telecom-sudparis.eu";
$u->web = null;
$u->location = "Evry";
$u->office = "D311";
$u->ldap_account = true;
$u->url_photo = null;
$u->positions = array("0" => "Associate professor", );
$u->phone = "0160764471";
$dump["lallet"] = $u;

$u = new TSPUser("hennequi");
$u->state = array();
$u->uid = "hennequi";
$u->wpuser = null;
$u->first_name = "Pascal";
$u->last_name = "Hennequin";
$u->mail = "pascal.hennequin@telecom-sudparis.eu";
$u->web = "http://www-inf.int-evry.fr/~hennequi";
$u->location = "Evry";
$u->office = "B310-1";
$u->ldap_account = true;
$u->url_photo = null;
$u->positions = array("0" => "Associate professor", );
$u->phone = "0160764790";
$dump["hennequi"] = $u;

$u = new TSPUser("berger_o");
$u->state = array();
$u->uid = "berger_o";
$u->wpuser = null;
$u->first_name = "Olivier";
$u->last_name = "Berger";
$u->mail = "olivier.berger@telecom-sudparis.eu";
$u->web = "http://www-public.telecom-sudparis.eu/~berger_o/";
$u->location = "Evry";
$u->office = "B303 et C09";
$u->ldap_account = true;
$u->url_photo = null;
$u->positions = array("0" => "Engineer for pedagogical transformation through digital technology", "1" => "Research engineer", );
$u->phone = "0160764532";
$dump["berger_o"] = $u;

$u = new TSPUser("bouzegho");
$u->state = array();
$u->uid = "bouzegho";
$u->wpuser = null;
$u->first_name = "Amel";
$u->last_name = "Bouzeghoub";
$u->mail = "amel.bouzeghoub@telecom-sudparis.eu";
$u->web = null;
$u->location = "Palaiseau";
$u->office = "4A308";
$u->ldap_account = true;
$u->url_photo = null;
$u->positions = array("0" => "Professor", );
$u->phone = "0175314429";
$dump["bouzegho"] = $u;

$u = new TSPUser("chabrido");
$u->state = array();
$u->uid = "chabrido";
$u->wpuser = null;
$u->first_name = "Sophie";
$u->last_name = "Chabridon";
$u->mail = "sophie.chabridon@telecom-sudparis.eu";
$u->web = "http://www-public.it-sudparis.eu/~chabrido";
$u->location = "Palaiseau";
$u->office = "4A324";
$u->ldap_account = true;
$u->url_photo = null;
$u->positions = array("0" => "Professor", );
$u->phone = "0175314434";
$dump["chabrido"] = $u;

$u = new TSPUser("belaid");
$u->state = array("director" => "director", );
$u->uid = "belaid";
$u->wpuser = null;
$u->first_name = "Djamel";
$u->last_name = "Belaid";
$u->mail = "djamel.belaid@telecom-sudparis.eu";
$u->web = "http://www-inf.int-evry.fr";
$u->location = "Evry";
$u->office = "D301";
$u->ldap_account = true;
$u->url_photo = null;
$u->positions = array("0" => "Director", "1" => "Professor", );
$u->phone = "0160764762";
$dump["belaid"] = $u;

$u = new TSPUser("conan");
$u->state = array();
$u->uid = "conan";
$u->wpuser = null;
$u->first_name = "Denis";
$u->last_name = "Conan";
$u->mail = "denis.conan@telecom-sudparis.eu";
$u->web = "http://www-public.it-sudparis.eu/~conan/";
$u->location = "Evry";
$u->office = "D308";
$u->ldap_account = true;
$u->url_photo = null;
$u->positions = array("0" => "Associate professor", );
$u->phone = "+33160764534";
$dump["conan"] = $u;

$u = new TSPUser("gancarsk");
$u->state = array();
$u->uid = "gancarsk";
$u->wpuser = null;
$u->first_name = "Alda";
$u->last_name = "Gancarski";
$u->mail = "alda.gancarski@telecom-sudparis.eu";
$u->web = null;
$u->location = "Evry";
$u->office = "B304";
$u->ldap_account = true;
$u->url_photo = null;
$u->positions = array("0" => "Associate professor", );
$u->phone = "+33160764531";
$dump["gancarsk"] = $u;

$u = new TSPUser("gibson");
$u->state = array();
$u->uid = "gibson";
$u->wpuser = null;
$u->first_name = "Paul";
$u->last_name = "Gibson";
$u->mail = "paul.gibson@telecom-sudparis.eu";
$u->web = "http://www-public.int-evry.fr/~gibson/";
$u->location = "Evry";
$u->office = "D311";
$u->ldap_account = true;
$u->url_photo = null;
$u->positions = array("0" => "Associate professor", );
$u->phone = "+33160764477";
$dump["gibson"] = $u;

$u = new TSPUser("mammar_a");
$u->state = array();
$u->uid = "mammar_a";
$u->wpuser = null;
$u->first_name = "Amel";
$u->last_name = "Mammar";
$u->mail = "amel.mammar@telecom-sudparis.eu";
$u->web = null;
$u->location = "Evry";
$u->office = "B309";
$u->ldap_account = true;
$u->url_photo = null;
$u->positions = array("0" => "Professor", );
$u->phone = "+33160764421";
$dump["mammar_a"] = $u;

$u = new TSPUser("gaaloulw");
$u->state = array();
$u->uid = "gaaloulw";
$u->wpuser = null;
$u->first_name = "Walid";
$u->last_name = "Gaaloul";
$u->mail = "walid.gaaloul@telecom-sudparis.eu";
$u->web = "http://www-inf.it-sudparis.eu/~gaaloulw/";
$u->location = "Palaiseau";
$u->office = "4A310";
$u->ldap_account = true;
$u->url_photo = null;
$u->positions = array("0" => "Professor", );
$u->phone = "0175314430";
$dump["gaaloulw"] = $u;

$u = new TSPUser("brunet_e");
$u->state = array();
$u->uid = "brunet_e";
$u->wpuser = null;
$u->first_name = "Elisabeth";
$u->last_name = "Brunet";
$u->mail = "elisabeth.brunet@telecom-sudparis.eu";
$u->web = "http://www-inf.it-sudparis.eu/~brunet_e";
$u->location = "Palaiseau";
$u->office = "4A320";
$u->ldap_account = true;
$u->url_photo = null;
$u->positions = array("0" => "Associate professor", );
$u->phone = "0175314432";
$dump["brunet_e"] = $u;

$u = new TSPUser("trahay_f");
$u->state = array();
$u->uid = "trahay_f";
$u->wpuser = null;
$u->first_name = "Francois";
$u->last_name = "Trahay";
$u->mail = "francois.trahay@telecom-sudparis.eu";
$u->web = "http://trahay.wp.tem-tsp.eu/";
$u->location = "Evry";
$u->office = "B301-1";
$u->ldap_account = true;
$u->url_photo = null;
$u->positions = array("0" => "Associate professor", );
$u->phone = "+33160764740";
$dump["trahay_f"] = $u;

$u = new TSPUser("lescouet");
$u->state = array("hide-data" => "hide-data", );
$u->uid = "lescouet";
$u->wpuser = null;
$u->first_name = "Alexis";
$u->last_name = "Lescouet";
$u->mail = "alexis.lescouet@telecom-sudparis.eu";
$u->web = null;
$u->location = "Evry";
$u->office = "B312";
$u->ldap_account = true;
$u->url_photo = null;
$u->positions = array("0" => "PhD student", );
$u->phone = null;
$dump["lescouet"] = $u;

$u = new TSPUser("thomas_g");
$u->state = array();
$u->uid = "thomas_g";
$u->wpuser = null;
$u->first_name = "Gael";
$u->last_name = "Thomas";
$u->mail = "gael.thomas@telecom-sudparis.eu";
$u->web = "http://www-public.it-sudparis.eu/~thomas_g/";
$u->location = "Palaiseau";
$u->office = "4A318";
$u->ldap_account = true;
$u->url_photo = null;
$u->positions = array("0" => "Professor", );
$u->phone = "0175314431";
$dump["thomas_g"] = $u;

$u = new TSPUser("wang_yuw");
$u->state = array();
$u->uid = "wang_yuw";
$u->wpuser = null;
$u->first_name = "Yuwei";
$u->last_name = "Wang";
$u->mail = "yuwei.wang@telecom-sudparis.eu";
$u->web = null;
$u->location = "Evry";
$u->office = null;
$u->ldap_account = true;
$u->url_photo = null;
$u->positions = array("0" => "PhD student", );
$u->phone = null;
$dump["wang_yuw"] = $u;

$u = new TSPUser("dulong_r");
$u->state = array();
$u->uid = "dulong_r";
$u->wpuser = null;
$u->first_name = "Remi";
$u->last_name = "Dulong";
$u->mail = "remi.dulong@telecom-sudparis.eu";
$u->web = null;
$u->location = "Evry";
$u->office = null;
$u->ldap_account = true;
$u->url_photo = null;
$u->positions = array("0" => "PhD student", );
$u->phone = null;
$dump["dulong_r"] = $u;

$u = new TSPUser("tanigass");
$u->state = array();
$u->uid = "tanigass";
$u->wpuser = null;
$u->first_name = "Subashiny";
$u->last_name = "Tanigassalame";
$u->mail = "subashiny.tanigassalame@telecom-sudparis.eu";
$u->web = null;
$u->location = "Evry";
$u->office = null;
$u->ldap_account = true;
$u->url_photo = null;
$u->positions = array("0" => "PhD student", );
$u->phone = null;
$dump["tanigass"] = $u;

$u = new TSPUser("sutra_p");
$u->state = array();
$u->uid = "sutra_p";
$u->wpuser = null;
$u->first_name = "Pierre";
$u->last_name = "Sutra";
$u->mail = "pierre.sutra@telecom-sudparis.eu";
$u->web = null;
$u->location = "Palaiseau";
$u->office = "4A322";
$u->ldap_account = true;
$u->url_photo = null;
$u->positions = array("0" => "Associate professor", );
$u->phone = "0175314433";
$dump["sutra_p"] = $u;

$u = new TSPUser("guermouc");
$u->state = array();
$u->uid = "guermouc";
$u->wpuser = null;
$u->first_name = "Amina";
$u->last_name = "Guermouche";
$u->mail = "amina.guermouche@telecom-sudparis.eu";
$u->web = null;
$u->location = "Evry";
$u->office = "D307";
$u->ldap_account = true;
$u->url_photo = null;
$u->positions = array("0" => "Associate professor", );
$u->phone = "0160764738";
$dump["guermouc"] = $u;

$u = new TSPUser("pipereau");
$u->state = array();
$u->uid = "pipereau";
$u->wpuser = null;
$u->first_name = "Yohan";
$u->last_name = "Pipereau";
$u->mail = "yohan.pipereau@telecom-sudparis.eu";
$u->web = null;
$u->location = "Evry";
$u->office = null;
$u->ldap_account = true;
$u->url_photo = null;
$u->positions = array("0" => "PhD student", );
$u->phone = null;
$dump["pipereau"] = $u;

$u = new TSPUser("raddaoui");
$u->state = array();
$u->uid = "raddaoui";
$u->wpuser = null;
$u->first_name = "Badran";
$u->last_name = "Raddaoui";
$u->mail = "badran.raddaoui@telecom-sudparis.eu";
$u->web = null;
$u->location = "Palaiseau";
$u->office = "4A306";
$u->ldap_account = true;
$u->url_photo = null;
$u->positions = array("0" => "Associate professor", );
$u->phone = "0175314428";
$dump["raddaoui"] = $u;

$u = new TSPUser("franca_t");
$u->state = array();
$u->uid = "franca_t";
$u->wpuser = null;
$u->first_name = "Tuanir";
$u->last_name = "Franca-Rezende";
$u->mail = "tuanir.franca-rezende@telecom-sudparis.eu";
$u->web = null;
$u->location = "Evry";
$u->office = null;
$u->ldap_account = true;
$u->url_photo = null;
$u->positions = array("0" => "PhD student", );
$u->phone = null;
$dump["franca_t"] = $u;

$u = new TSPUser("mebrek_w");
$u->state = array();
$u->uid = "mebrek_w";
$u->wpuser = null;
$u->first_name = "Wafaa";
$u->last_name = "Mebrek";
$u->mail = "wafaa.mebrek@telecom-sudparis.eu";
$u->web = null;
$u->location = "Evry";
$u->office = null;
$u->ldap_account = true;
$u->url_photo = null;
$u->positions = array("0" => "PhD student", );
$u->phone = null;
$dump["mebrek_w"] = $u;

$u = new TSPUser("albilani");
$u->state = array();
$u->uid = "albilani";
$u->wpuser = null;
$u->first_name = "Mohamad";
$u->last_name = "Albilani";
$u->mail = "mohamad.albilani@telecom-sudparis.eu";
$u->web = null;
$u->location = "Evry";
$u->office = null;
$u->ldap_account = true;
$u->url_photo = null;
$u->positions = array("0" => "PhD student", );
$u->phone = null;
$dump["albilani"] = $u;

$u = new TSPUser("afendi");
$u->state = array();
$u->uid = "afendi";
$u->wpuser = null;
$u->first_name = "Meryem";
$u->last_name = "Afendi";
$u->mail = "meryem.afendi@telecom-sudparis.eu";
$u->web = null;
$u->location = "Evry";
$u->office = null;
$u->ldap_account = true;
$u->url_photo = null;
$u->positions = array("0" => "PhD student", );
$u->phone = null;
$dump["afendi"] = $u;

$u = new TSPUser("chaaben");
$u->state = array();
$u->uid = "chaaben";
$u->wpuser = null;
$u->first_name = "Nour-El-Houda";
$u->last_name = "Ben-Chaabene";
$u->mail = "nourelhouda.benchaabene@telecom-sudparis.eu";
$u->web = null;
$u->location = "Evry";
$u->office = null;
$u->ldap_account = true;
$u->url_photo = null;
$u->positions = array("0" => "PhD student", );
$u->phone = null;
$dump["chaaben"] = $u;

$u = new TSPUser("borges_c");
$u->state = array();
$u->uid = "borges_c";
$u->wpuser = null;
$u->first_name = "Pedro Victor";
$u->last_name = "Borges Caldas Da Silva";
$u->mail = "pedro.borges@telecom-sudparis.eu";
$u->web = null;
$u->location = "Evry";
$u->office = null;
$u->ldap_account = true;
$u->url_photo = null;
$u->positions = array("0" => "PhD student", );
$u->phone = null;
$dump["borges_c"] = $u;

$u = new TSPUser("lefor_an");
$u->state = array("hide-data" => "hide-data", );
$u->uid = "lefor_an";
$u->wpuser = null;
$u->first_name = "Anatole";
$u->last_name = "Lefort";
$u->mail = "anatole_lefort@telecom-sudparis.eu";
$u->web = null;
$u->location = "Evry";
$u->office = null;
$u->ldap_account = true;
$u->url_photo = null;
$u->positions = array("0" => "PhD student", );
$u->phone = null;
$dump["lefor_an"] = $u;

$u = new TSPUser("sellam_m");
$u->state = array();
$u->uid = "sellam_m";
$u->wpuser = null;
$u->first_name = "Mohamed";
$u->last_name = "Sellami";
$u->mail = "mohamed.sellami@telecom-sudparis.eu";
$u->web = "http://www-public.imtbs-tsp.eu/~sellam_m/";
$u->location = "Evry";
$u->office = "B309";
$u->ldap_account = true;
$u->url_photo = null;
$u->positions = array("0" => "Associate professor", );
$u->phone = "0160764737";
$dump["sellam_m"] = $u;

$u = new TSPUser("colin_a");
$u->state = array();
$u->uid = "colin_a";
$u->wpuser = null;
$u->first_name = "Alexis";
$u->last_name = "Colin";
$u->mail = "alexis_colin@telecom-sudparis.eu";
$u->web = null;
$u->location = "Evry";
$u->office = "B312";
$u->ldap_account = true;
$u->url_photo = null;
$u->positions = array("0" => "PhD student", );
$u->phone = "0160764573";
$dump["colin_a"] = $u;

$u = new TSPUser("daumen");
$u->state = array();
$u->uid = "daumen";
$u->wpuser = null;
$u->first_name = "Anton";
$u->last_name = "Daumen";
$u->mail = "anton.daumen@telecom-sudparis.eu";
$u->web = null;
$u->location = "Evry";
$u->office = null;
$u->ldap_account = true;
$u->url_photo = null;
$u->positions = array("0" => "PhD student", );
$u->phone = null;
$dump["daumen"] = $u;

$u = new TSPUser("ahmed_m");
$u->state = array();
$u->uid = "ahmed_m";
$u->wpuser = null;
$u->first_name = "Mohamed";
$u->last_name = "Ahmed";
$u->mail = "mohamed_ahmed@telecom-sudparis.eu";
$u->web = null;
$u->location = "Evry";
$u->office = null;
$u->ldap_account = true;
$u->url_photo = null;
$u->positions = array("0" => "PhD student", );
$u->phone = null;
$dump["ahmed_m"] = $u;

$u = new TSPUser("elleuch");
$u->state = array();
$u->uid = "elleuch";
$u->wpuser = null;
$u->first_name = "Marwa";
$u->last_name = "Elleuch";
$u->mail = "marwa.elleuch@telecom-sudparis.eu";
$u->web = null;
$u->location = "Evry";
$u->office = null;
$u->ldap_account = true;
$u->url_photo = null;
$u->positions = array("0" => "PhD student", );
$u->phone = null;
$dump["elleuch"] = $u;

$u = new TSPUser("kane_b");
$u->state = array();
$u->uid = "kane_b";
$u->wpuser = null;
$u->first_name = "Boubacar";
$u->last_name = "Kane";
$u->mail = "boubacar.kane@telecom-sudparis.eu";
$u->web = null;
$u->location = "Evry";
$u->office = null;
$u->ldap_account = true;
$u->url_photo = null;
$u->positions = array("0" => "PhD student", );
$u->phone = null;
$dump["kane_b"] = $u;

$u = new TSPUser("degli");
$u->state = array("secretary" => "secretary", );
$u->uid = "degli";
$u->wpuser = null;
$u->first_name = "Marie";
$u->last_name = "Degli-Esposti";
$u->mail = "marie.degli_esposti@telecom-sudparis.eu";
$u->web = null;
$u->location = "Evry";
$u->office = "D302";
$u->ldap_account = true;
$u->url_photo = null;
$u->positions = array("0" => "Department assistant", );
$u->phone = "0160764783";
$dump["degli"] = $u;

$u = new TSPUser("lmoctar");
$u->state = array();
$u->uid = "lmoctar";
$u->wpuser = null;
$u->first_name = "Leyla";
$u->last_name = "Moctar M Baba";
$u->mail = "leyla.moctar_mbaba@telecom-sudparis.eu";
$u->web = null;
$u->location = "Evry";
$u->office = null;
$u->ldap_account = true;
$u->url_photo = null;
$u->positions = array("0" => "PhD student", );
$u->phone = null;
$dump["lmoctar"] = $u;

$u = new TSPUser("thenry");
$u->state = array();
$u->uid = "thenry";
$u->wpuser = null;
$u->first_name = "Tiphaine";
$u->last_name = "Henry";
$u->mail = "tiphaine.henry@telecom-sudparis.eu";
$u->web = null;
$u->location = "Evry";
$u->office = null;
$u->ldap_account = true;
$u->url_photo = null;
$u->positions = array("0" => "PhD student", );
$u->phone = null;
$dump["thenry"] = $u;

$u = new TSPUser("thenot");
$u->state = array();
$u->uid = "thenot";
$u->wpuser = null;
$u->first_name = "Damien";
$u->last_name = "Thenot";
$u->mail = "damien.thenot@telecom-sudparis.eu";
$u->web = null;
$u->location = "Evry";
$u->office = null;
$u->ldap_account = true;
$u->url_photo = null;
$u->positions = array("0" => "PhD student", );
$u->phone = null;
$dump["thenot"] = $u;

$u = new TSPUser("mhinge");
$u->state = array();
$u->uid = "mhinge";
$u->wpuser = null;
$u->first_name = "Marius";
$u->last_name = "Hinge";
$u->mail = "marius.hinge@telecom-sudparis.eu";
$u->web = null;
$u->location = "Evry";
$u->office = null;
$u->ldap_account = true;
$u->url_photo = null;
$u->positions = array("0" => "PhD student", );
$u->phone = null;
$dump["mhinge"] = $u;

$u = new TSPUser("bounader");
$u->state = array();
$u->uid = "bounader";
$u->wpuser = null;
$u->first_name = "Ralph";
$u->last_name = "Bou Nader";
$u->mail = "ralph.bounader@telecom-sudparis.eu";
$u->web = null;
$u->location = null;
$u->office = null;
$u->ldap_account = true;
$u->url_photo = null;
$u->positions = array("0" => "PhD student", );
$u->phone = null;
$dump["bounader"] = $u;

$u = new TSPUser("mika");
$u->state = array();
$u->uid = "mika";
$u->wpuser = null;
$u->first_name = "Grzegorz";
$u->last_name = "Mika";
$u->mail = "grzegorz.mika@telecom-sudparis.eu";
$u->web = null;
$u->location = null;
$u->office = null;
$u->ldap_account = true;
$u->url_photo = null;
$u->positions = array("0" => "PhD student", );
$u->phone = null;
$dump["mika"] = $u;

$u = new TSPUser("amaheo");
$u->state = array();
$u->uid = "amaheo";
$u->wpuser = null;
$u->first_name = "Aurele";
$u->last_name = "Maheo";
$u->mail = "aurele.maheo@telecom-sudparis.eu";
$u->web = null;
$u->location = null;
$u->office = null;
$u->ldap_account = true;
$u->url_photo = null;
$u->positions = array("0" => "Post-doc researcher", );
$u->phone = null;
$dump["amaheo"] = $u;

$u = new TSPUser("msa");
$u->state = array();
$u->uid = "msa";
$u->wpuser = null;
$u->first_name = "Muktikanta";
$u->last_name = "Sa";
$u->mail = "muktikanta.sa@telecom-sudparis.eu";
$u->web = null;
$u->location = null;
$u->office = null;
$u->ldap_account = true;
$u->url_photo = null;
$u->positions = array("0" => "Post-doc researcher", );
$u->phone = null;
$dump["msa"] = $u;

$u = new TSPUser("mbacou");
$u->state = array();
$u->uid = "mbacou";
$u->wpuser = null;
$u->first_name = "Mathieu";
$u->last_name = "Bacou";
$u->mail = "mathieu.bacou@telecom-sudparis.eu";
$u->web = "https://bacou.wp.imtbs-tsp.eu/";
$u->location = "Evry";
$u->office = "B305";
$u->ldap_account = true;
$u->url_photo = null;
$u->positions = array("0" => "Associate professor", );
$u->phone = "0160764701";
$dump["mbacou"] = $u;

$u = new TSPUser("belheoua");
$u->state = array();
$u->uid = "belheoua";
$u->wpuser = null;
$u->first_name = "Chourouk";
$u->last_name = "Belheouane";
$u->mail = "chourouk.belheouane@telecom-sudparis.eu";
$u->web = null;
$u->location = null;
$u->office = null;
$u->ldap_account = true;
$u->url_photo = null;
$u->positions = array("0" => "Assistant professor", );
$u->phone = null;
$dump["belheoua"] = $u;

$u = new TSPUser("boulouka");
$u->state = array();
$u->uid = "boulouka";
$u->wpuser = null;
$u->first_name = "Georgios";
$u->last_name = "Bouloukakis";
$u->mail = "georgios.bouloukakis@telecom-sudparis.eu";
$u->web = null;
$u->location = null;
$u->office = null;
$u->ldap_account = true;
$u->url_photo = null;
$u->positions = array("0" => "permanent", );
$u->phone = null;
$dump["boulouka"] = $u;

$u = new TSPUser("munante");
$u->state = array();
$u->uid = "munante";
$u->wpuser = null;
$u->first_name = "Denisse-Yessica";
$u->last_name = "Munante-Arzapalo";
$u->mail = "denisse_yessica.munante_arzapalo@telecom-sudparis.eu";
$u->web = null;
$u->location = null;
$u->office = null;
$u->ldap_account = true;
$u->url_photo = null;
$u->positions = array("0" => "permanent", );
$u->phone = null;
$dump["munante"] = $u;

$u = new TSPUser("mechouch");
$u->state = array();
$u->uid = "mechouch";
$u->wpuser = null;
$u->first_name = "Jeremy";
$u->last_name = "Mechouche";
$u->mail = "jeremy.mechouche@telecom-sudparis.eu";
$u->web = null;
$u->location = null;
$u->office = null;
$u->ldap_account = true;
$u->url_photo = null;
$u->positions = array("0" => "PhD student", );
$u->phone = null;
$dump["mechouch"] = $u;

$u = new TSPUser("assy");
$u->state = array();
$u->uid = "assy";
$u->wpuser = null;
$u->first_name = "Nour";
$u->last_name = "Assy";
$u->mail = "nour_assy@telecom-sudparis.eu";
$u->web = null;
$u->location = null;
$u->office = null;
$u->ldap_account = true;
$u->url_photo = null;
$u->positions = array("0" => "permanent", );
$u->phone = null;
$dump["assy"] = $u;

$u = new TSPUser("canek");
$u->state = array();
$u->uid = "canek";
$u->wpuser = null;
$u->first_name = "Rodrigo-Andres";
$u->last_name = "Canek-Aragon";
$u->mail = "rodrigo_andres.canek_aragon@telecom-sudparis.eu";
$u->web = null;
$u->location = null;
$u->office = null;
$u->ldap_account = true;
$u->url_photo = null;
$u->positions = array("0" => "Internship", );
$u->phone = null;
$dump["canek"] = $u;
?>
