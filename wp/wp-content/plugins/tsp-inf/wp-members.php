<?php

add_action( 'show_user_profile', 'extra_user_profile_fields' );
add_action( 'edit_user_profile', 'extra_user_profile_fields' );

function extra_user_profile_fields($user) {
  $self = TSPUser::load(wp_get_current_user());
  $u = TSPUser::load($user);
?>
    <h3><?php _e("TSP profile information", "blank"); ?></h3>

    <p>If you have a CAS account, the information recorded through this page
      overhides the ones provided by TSP. You can also overhide your first name, last name, email and Web site
      by changing these settings (see above).
      If you have a local account, don't forget to fill your first name, last name, email and Web site.</p>

    <table class="form-table">
    <tr>
        <th><label for="position"><?php _e("Position"); ?></label></th>
        <td>
          <select  name="position" id="position" <?= $self->is_blogadmin() ? '' : 'disabled' ?>>
            <?php
            $pos = get_the_author_meta('position', $user->ID );

            echo '<option value=""' . ($pos ? '' : ' selected') . '>--Please choose an option--</option>';
            foreach(job_names() as $cur)
              echo '<option value="' . $cur . '"' . ($pos === $cur ? 'selected' : '') . '>' . $cur . '</option>';
            ?>
          </select><br>
          <span class="description"><?php _e("Please enter your position."); ?></span>
        </td>
    </tr>
    
    <tr>
        <th><?php _e("Groups"); ?></th>
        <td>
          <?php
          foreach($u->groups as $cur)
            if($cur->group_name !== 'main') {
              echo '<span style="display: inline-block; margin-right: 1em;">';
              echo $cur->as_checkbox('groups');
              echo $cur->as_label();
              echo '</span>';
            }
          ?>
          <br>
          <span class="description"><?php _e("Please enter your groups."); ?></span>
        </td>
    </tr>
    
    <tr>
        <th><?php _e("Status"); ?></th>
        <td>
          <?php
          foreach(array('alumni' => 'Alumni',
                        'cnr' => 'Sabbatical',
                        'invited' => 'Invited Researcher',
                        'adjunct' => 'Adjunct') as $k => $v) {
            echo '<span style="display: inline-block; margin-right: 1em;">';
            echo '<input type="checkbox" id="' . $k . '" name="status[]" value="' . $k . '"' .
                   ($u->has_state($k) ? ' checked' : '') .
                   ($self->is_blogadmin() ? '' : ' disabled') .
                   '><label for="' . $k . '">' . $v . '</label></span>';
              echo '</span>';
          }
          ?>
          <br>
          <span class="description"><?php _e("Status."); ?></span>
        </td>
    </tr>

    <tr>
        <th><label for="phone"><?php _e("Phone"); ?></label></th>
        <td>
            <input type="text" name="phone" id="phone" value="<?= $u->phone; ?>" class="regular-text" /><br />
            <span class="description"><?php _e("Please enter your phone number."); ?></span>
        </td>
    </tr>

    <tr>
        <th><label for="photo"><?php _e("Photo"); ?></label></th>
        <td>
            <input type="url" name="photo" id="photo" value="<?= $u->url_photo; ?>" class="regular-text" /><br />
            <span class="description"><?php _e("Please enter the URL of your photo."); ?></span>
        </td>
    </tr>
    
    <tr>
        <th><label for="location"><?php _e("Location"); ?></label></th>
        <td>
          <select  name="location" id="location">
            <?php
            $loc = get_the_author_meta('location', $user->ID );
            foreach(array("--Please choose an option--" => "",
                          "Evry" => "Evry",
                          "Palaiseau" => "Palaiseau") as $v => $k) {
              echo '<option value="' . $k . '"' . ($loc === $k ? 'selected' : '') . '>' . $v . '</option>';
            }
            ?>
          </select><br>
          <span class="description"><?php _e("Please enter the location of your office."); ?></span>
        </td>
    </tr>
    
    <tr>
        <th><label for="office"><?php _e("Office"); ?></label></th>
        <td>
          <input type="text" name="office" id="office" value="<?php echo esc_attr( get_the_author_meta( 'office', $user->ID ) ); ?>" class="regular-text" /><br />
          <span class="description"><?php _e("Please enter your office number."); ?></span>
        </td>
    </tr>
    
    <tr>
        <th><?php _e("Hide"); ?></th>
        <td>
          <?php
          foreach(array('hide-photo' => 'Hide photo',
                        'hide-phone' => 'Hide phone',
                        'hide-office' => 'Hide office',
                        'hide-mail' => 'Hide mail',
                        'hide-user' => 'Disappear') as $k => $v) {
            echo '<span style="display: inline-block; margin-right: 1em;">';
            echo '<input type="checkbox" id="' . $k . '" name="hides[]" value="' . $k . '"' .
                     ($u->has_state($k) ? ' checked' : '') .
                   '><label for="' . $k . '">' . $v . '</label></span>';
              echo '</span>';
          }
          ?>
          <br>
          <span class="description"><?php _e("Hide specific parts of your profile or totally disappear from the trombis."); ?></span>
        </td>
    </tr>
    </table>
<?php }

add_action( 'personal_options_update', 'save_extra_user_profile_fields' );
add_action( 'edit_user_profile_update', 'save_extra_user_profile_fields' );

function save_extra_user_profile_fields( $user_id ) {
  if (empty($_POST['_wpnonce']) || !wp_verify_nonce($_POST['_wpnonce'], 'update-user_' . $user_id)) { return; }
    
  if (!current_user_can( 'edit_user', $user_id)) { return false; }

  $u = TSPUser::load(get_userdata($user_id));

  if(TSPUser::load(wp_get_current_user())->is_blogadmin()) {
    $u->positions = $_POST['position'] ? array($_POST['position']) : array();
    $u->set_groups($_POST[$u->html_form_group_id()]);

    foreach(array('alumni', 'cnr', 'adjunct', 'invited') as $k)
      $u->set_state($k, in_array($k, $_POST['status']));
  }

  $u->phone = $_POST['phone'];
  $u->url_photo = $_POST['photo'];
  $u->location = $_POST['location'];
  $u->office = $_POST['office'];

  foreach(array('hide-photo', 'hide-phone', 'hide-office', 'hide-mail', 'hide-user') as $k)
    $u->set_state($k, in_array($k, $_POST['hides']));

  if($u->is_cnr()) {
    $u->set_hide_office();
    $u->set_hide_phone();
    $u->set_hide_mail();
  }
  
  $u->save();
}

function wp_get_members($dump, $filter, $import) {
  foreach($filter as $uid) {
    if(array_key_exists($uid, $dump))
      $dump[$uid]->load_and_merge();
    else {
      $user = get_user_by('login', $uid);
      if($user)
        $dump[$uid] = TSPUser::load($user);
    }

    $u = $dump[$uid];
    
    if($u && !$u->wpuser && $import && TSPUser::load(wp_get_current_user())->is_blogadmin())
      $u->import();
  }

  return $dump;
}

function merge_filter($filter, $group) {
  if(!$group)
    return $filter;
  
  $filter = $filter ? $filter : array();

  foreach(get_users(array('meta_key' => 'groups',
                          'meta_value' => $group,
                          'meta_compare' => 'LIKE')) as $user) {
    $filter[] = $user->user_login;
  }

  return $filter;
}

?>
