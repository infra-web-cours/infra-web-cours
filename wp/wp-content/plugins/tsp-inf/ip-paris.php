<?php

// * Computer Networking & Future Networking Architectures [Cedric Ware, Thomas Clausen, Djamal].
// * Quantum Networking [Romain Alléaume, Cédric Ware]
// * Mobile, Cellular, networks, 5G, 6G [Marceau Coupechoux, Djamal Zeghlache, Badii Jouaber].
// * IoT - Connected Objects. [Keun-Woo Lim, Juan-Antonio Cordero-Fuertes, Sophie Chabridon, Aline Carneiro Viana, Georgios Bouloukakis, Djamel Belaïd, Badii Jouaber]
// * Parallel and Distributed Systems [Gaël Thomas, Petr Kuznetsov, Pierre Sutra, François Trahay]
// * Cloud Computing & Elastic Cloud Infrastructures [Pierre Sutra, Thomas Clausen, Jean-Louis Rougier]

function do_fa_icon($name) {
  return '<span style="display: inline-block; text-align: center; font-size: 0.9em; width: 1.4em;"><i class="fa ' . $name . '" aria-hidden="true"></i></span>';
}

function do_fa_url($url, $id) {
  return '&nbsp;<a href="' . $url . '">' . do_fa_icon($id) . '</a>';
}

class Topic {
  public $name;
  public $teams;

  public function __construct($name) {
    $this->name = $name;
    $this->teams = array();
  }
}

$topics = array();

$topics['Cloud'] = new Topic('Cloud Computing & Elastic Cloud Infrastructures');
$topics['Network'] = new Topic('Computer Networking & Future Networking Architectures');
$topics['IoT'] = new Topic('IoT – Connected Objects');
$topics['Mobile'] = new Topic('Mobile, Cellular, networks, 5G, 6G');
$topics['Quantum'] = new Topic('Quantum Networking');
$topics['PDS'] = new Topic('Parallel and Distributed Systems');

class Team {
  public $id;
  public $name;
  public $institute;
  public $web;
  public $contact;
  public $allContacts;
  public $topics;

  public function __construct($id, $name, $institute, $web, $contact, $allContacts, $_topics = null) {
    global $topics;
    $this->id = $id;
    $this->name = $name;
    $this->institute = $institute;
    $this->web = $web;
    $this->contact = $contact;
    $this->allContacts = $allContacts;

    if($_topics) {
      $this->topics = $_topics;

      foreach($this->topics as $topic)
        $topics[$topic]->teams[$this->id] = $this;
    }
  }

  public function addTopics($theme, $_topics) {
    global $topics;
    $this->topics = $_topics;

    foreach($this->topics as $topic)
      $topics[$topic]->teams[$this->id] = $this;
  }
  
  public function contactLink() {
//    return do_fa_url('mailto:%22' . str_replace(' ', '%20', $this->contact[0]) . '%22%3c' . str_replace(' ', '', str_replace('@', '@', $this->contact[1])) . '%3e', 'fa-envelope');
    return do_fa_url('mailto:' . str_replace(' ', '', str_replace('@', '_at_', $this->contact[1])), 'fa-envelope');
  }
  
  public function asTableEntry() {
    $res  = '<tr>';
    $res .= '  <td><a href="' . $this->web .'">' . $this->id . '</a></td>';
    $res .= '  <td>' . $this->name . '</td>';
    $res .= '  <td>' . $this->institute . '</td>';
    $res .= '  <td>' . implode(', ', $this->topics) . '</td>';
    $res .= '  <td>' . $this->contactLink() . '</td>';
    $res .= "</tr>";
    return $res;
  }
};

class Teams {
  public $teams;

  public function __construct() {
    $this->teams = array();
  }
  
  public function asTable() {
    $res = "<table class='teams'>";
    $res .= '<tr><th>Acronym</th><th>Name</th><th>Institute</th><th>Research topics</th><th>Contact</th></tr>';
    foreach($this->teams as $cur) {
      $res .= $cur->asTableEntry();
    } 
    $res .= "</table>";
    return $res;
  }
};

function to_html($str) {
  return str_replace('>', '&gt;', str_replace('<', '&lt;', $str));
}

function topics_to_teams($to_html = false) {
  global $topics;
  $res = '';
  foreach($topics as $id => $topic) {
    $res .= '<p>' . $topic->name . ':</p>';
    $str = $to_html ? '<p>Involved teams:</p>' : '';
    $str .= '<ul>';
    foreach($topic->teams as $team) {
      $str .= '<li><a href="' . $team->web . '">' . $team->id . ' - ' . $team->name . '</a> ' . $team->contactLink() . '</li>';
    }
    $str .= '</ul>';
    $res .= $to_html ? to_html($str) . '<br><br>' : $str;
  }
  return $res;
}

$teams = new Teams();

$teams->teams['Cedar'] = new Team("Cedar",
                                  "Rich Data Analytics at Cloud Scale",
                                  "X/INRIA",
                                  "https://team.inria.fr/cedar/",
                                  array("Ioana Manolescu", "ioana.manolescu@inria.fr"),
                                  array("Ioana Manolescu", "Yanlei Diao"));
$teams->teams['Cedar']->addTopics('NeGeDi', array('Cloud', 'PDS'));

//$teams->teams['Cedar']->addTopics(array('Cloud', 'PDS'));

$teams->teams['Cosynus'] = new Team("Cosynus",
                                    "Cosynus",
                                    "X",
                                    "http://www.lix.polytechnique.fr/cosynus/",
                                    array("Samuel Mimram", "samuel.mimram@lix.polytechnique.fr"),
                                    array("Éric Goubault", "Samuel Mimram"));
$teams->teams['Cosynus']->addTopics('NeGeDi', array('PDS'));

$teams->teams['PDS'] = new Team("PDS",
                                "Parallel and Distributed Systems",
                                "Telecom SudParis",
                                "https://www.inf.telecom-sudparis.eu/pds",
                                array("Gaël Thomas", "gael.thomas@telecom-sudparis.eu"),
                                array("Gaël Thomas", "François Trahay", "Pierre Sutra"),
                                array('PDS', 'Cloud'));
$teams->teams['PDS']->addTopics('NeGeDi', array('PDS', 'Cloud'));

$teams->teams['DiSSEM'] = new Team("DiSSEM",
                                   "Distributed Systems, Software Engineering and Middleware",
                                   "Telecom SudParis",
                                   "https://www.inf.telecom-sudparis.eu/dissem",
                                   array("Sophie Chabridon", "sophie.chabridon@telecom-sudparis.eu"),
                                   array("Sophie Chabridon", "Djamel Belaid", "Georgios Bouloukakis"));
$teams->teams['DiSSEM']->addTopics('NeGeDi', array('IoT', 'PDS', 'Cloud'));

$teams->teams['RMS'] = new Team("RMS",
                                "Réseaux, Mobilité et Services",
                                "Telecom Paris",
                                "https://nms.telecom-paristech.fr/",
                                array("Marceau Coupechoux", "marceau.coupechoux@telecom-paris.fr "),
                                array("Marceau Coupechoux", "Keun Woo Lim", "Jean-Louis Rougier"));
$teams->teams['RMS']->addTopics('NeGeDi', array('Network', 'Mobile', 'IoT', 'Cloud'));

//array_push($teams->teams, new Team("Comète",
//                                   "Comète",
//                                   "X",
//                                   "https://www.lix.polytechnique.fr/team/4/view",
//                                   array("Bernadette Charron-Bost"),
//                                   array()
//));

$teams->teams['ACES'] = new Team("ACES",
                                 "Systèmes embarqués critiques autonomes",
                                 "Telecom Paris",
                                 "https://aces.wp.imt.fr/",
                                 array("Petr Kuznetsov", "petr.kuznetsov@telecom-paris.fr "),
                                 array("Petr Kuznetsov"));
$teams->teams['ACES']->addTopics('NeGeDi', array("PDS"));

$teams->teams['IQA'] = new Team("IQA",
                                "Information Quantique et Applications",
                                "Telecom Paris",
                                "https://iqa.telecom-paris.fr/",
                                array("Romain Alléaume", "romain.alleaume@telecom-paristech.fr "),
                                array("Romain Alléaume"));
$teams->teams['IQA']->addTopics('NeGeDi', array('Quantum'));

$teams->teams['R3S'] = new Team("R3S",
                                "Réseaux, Services, Systèmes et Sécurité",
                                "Telecom SudParis",
                                "https://samovar.telecom-sudparis.eu/spip.php?rubrique129",
                                array("Badii Joauber", "badii.jouaber@telecom-sudparis.eu"),
                                array("Badii Joauber", "Djamal Zeghlache"));
$teams->teams['R3S']->addTopics('NeGeDi', array('Network', 'Mobile', 'IoT'));

$teams->teams['Methodes'] = new Team("Methodes",
                                     "Méthodes et modèles pour les réseaux",
                                     "Telecom SudParis",
                                     "https://samovar.telecom-sudparis.eu/spip.php?rubrique128",
                                     array("Tijani Chahed", "tijani.chahed@telecom-sudparis.eu"),
                                     array("Tijani Chahed", "Natalia Kushik"));
$teams->teams['Methodes']->addTopics('NeGeDi', array('Network', 'IoT', 'Mobile', 'Cloud'));

$teams->teams['Epizeuxis'] = new Team("Epizeuxis",
                                      "Epizeuxis",
                                      "X",
                                      "https://www.lix.polytechnique.fr/team/7/view",
                                      array("Thomas Clausen", "thomas.clausen@polytechnique.edu"),
                                      array("Thomas Clausen", "Juan-Antonio Cordero-Fuertes"));
$teams->teams['Epizeuxis']->addTopics('NeGeDi', array('Network', 'IoT', 'Cloud'));

$teams->teams['Tribe'] = new Team("Tribe",
                                  "Internet Beyond the Usual",
                                  "X/INRIA",
                                  "https://team.inria.fr/tribe/",
                                  array("Aline Carneiro Viana", "aline.viana@inria.fr "),
                                  array("Philippe Jacquet", "Aline Carneiro Viana"));
$teams->teams['Tribe']->addTopics('NeGeDi', array('Network', 'Mobile', 'IoT', 'PDS'));

$teams->teams['ASR'] = new Team("ASR",
                                "Autonomous Systems and Robotics",
                                "ENSTA",
                                "http://cogrob.ensta-paris.fr/",
                                array("Omar Hammami", "omar.hammami@ensta-paris.fr"),
                                array("Omar Hammami", "Benoit Geller"));
$teams->teams['ASR']->addTopics('NeGeDi', array('PDS', 'Cloud', 'IoT', 'Mobile'));

$teams->teams['GTO'] = new Team("GTO",
                                "Télécommunications optiques",
                                "Telecom Paris",
                                "https://www.telecom-paris.fr/en/research/laboratories/information-processing-and-communication-laboratory-ltci/research-teams/optical-telecommunications",
                                array("Cédric Ware", "cedric.ware@telecom-paris.fr"),
                                array("Cédric Ware", "Elie Awwad"));
$teams->teams['GTO']->addTopics('NeGeDi', array('Network', 'Quantum'));

$teams->teams['CCN'] = new Team("CCN",
                                "Sécurité des réseaux",
                                "Telecom Paris",
                                "https://www.telecom-paris.fr/fr/recherche/laboratoires/laboratoire-traitement-et-communication-de-linformation-ltci/les-equipes-de-recherche/securite-des-reseaux-ccn",
                                array("Houda Labiod", "houda.labiod@telecom-paris.fr"),
                                array("Houda Labiod", "Jun Zhang"));
$teams->teams['CCN']->addTopics('NeGeDi', array('IoT', 'Mobile'));

$tmp = $teams->teams;
$teams->teams = array();
foreach($tmp as $team)
  $teams->teams[$team->id] = $team;
ksort($teams->teams);

foreach($topics as $topic)
  ksort($topic->teams);

?>

<?php

echo '<h2>Research topics:</h2><ul>';
echo topics_to_teams();

echo '<h2>Teams:</h2>';
echo $teams->asTable();

echo '<h2>Generated topics</h2>';
echo topics_to_teams(true);

echo '<h2>Teams table</h2>';
echo to_html($teams->asTable());

?>
