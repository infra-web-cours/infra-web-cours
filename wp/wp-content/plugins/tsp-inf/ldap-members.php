<?php

$ldap_config = array(
  'uri'           => 'ldap://idwldapfr1.imtbs-tsp.eu',
  'version'       => '3',
  'user'          => 'cn=dptu,ou=dsa,dc=int-evry,dc=fr',
  'password'      => '21pDPTUdsa:',
  'baseDN'        => 'ou=active,dc=int-evry,dc=fr',
  'meta_data'     => array('uid',
                           'givenname',
                           'sn',
                           'labeleduri',
                           'mail',
                           'telephonenumber',
                           'l',
                           'roomnumber',
                           'description',
                           'employeeType',
                           'supannEtuEtape',
                           'jpegPhoto',
                           'eduPersonAffiliation')
);

function job_names() {
  return array('prof'      => 'Professor',
               'mdc'       => 'Associate professor',
               'teacher'   => 'Assistant professor',
               'engineer'  => 'Research engineer',
               'postdoc'   => 'Post-doc researcher',
               'phd'       => 'PhD student',
               'intern'    => 'Master student',
               'assistant' => 'Department assistant',
               'visiting'  => 'Visiting professor',
               'drinria'   => 'Inria senior researcher',
               'crinria'   => 'Inria young researcher',
  );
}

function ldap_get_single($info, $key) {
  return array_key_exists($key, $info) && array_key_exists('count', $info[$key]) && $info[$key]['count'] > 0 ? $info[$key][0] : null;
}

function ldap_get_array($info, $key) {
  $res = array();
  if(array_key_exists($key, $info) && array_key_exists('count', $info[$key])) {
    for($i=0; $i<$info[$key]['count']; $i++)
      $res[] = $info[$key][$i];
  }
  return $res;
}

function fix_ldap($u) {
  // fix
  switch($u->uid) {
    //case 'thomas_g': $u->positions = array("Inria senior researcher"); break;
    //case 'hmontels': $u->set_secretary(); $u->positions = array('Temporary department assistant'); break;
    //case 'boulouka': $u->positions = array('Maître de conférences'); break;
  }

  
  // translate
  $positions = array_map(function($in) {
    $val = null;
    $jobs = job_names();

    if(preg_match("/ma[iî]tre de conférence/ui", $in) || in_array($in, array("Ingénieur d'études", "MC en informatique")))
      $val = $jobs['mdc'];
    else if(in_array($in, array("Enseignant-chercheur")))
      $val = $jobs['teacher'];
    else if(in_array($in, array("Professeur", "Professeure", "Directeur d'études", "Directrice d'études")))
      $val = $jobs['prof'];
    else if(in_array($in, array("Ingénieur de recherche")))
      $val = $jobs['engineer'];
    else if(in_array($in, array("Ingénieur pour la transformation pédagogique par le numérique")))
      $val = "Engineer for pedagogical transformation through digital technology";
    else if(preg_match("/Adjoint directeur/ui", $in))
      $val = "Associate director";
    else if(preg_match("/Directeur/ui", $in))
      $val = "Director";
    else if(in_array($in, array("Ingénieur de recherche et développement", "postdoc")))
      $val = $jobs['postdoc'];
    else if(in_array($in, array("Doctorant", "Doctorante")))
      $val = $jobs['phd'];
    else if(in_array($in, array("stagiaire")))
      $val = $jobs['intern'];
    else if(in_array($in, array("Chargée de gestion")))
      $val = $jobs['assistant'];
    else if(in_array($in, array("Visiteur"))) {
      $val = $jobs['visiting'];
    } else
      $val = $in;

    return $val;
  }, $u->positions);

  sort($positions);

  $u->positions = $positions;
}

function ldap_to_positions($u, $a, $d, $t, $etu) {
  if(count($a)) {
    if(in_array('member', $a)) {
      if(preg_match("/CDD/ui", $t) || preg_match("/Post-doc/ui", $t))
        $u->positions = array("postdoc");
      else if(preg_match("/Doctorant/ui", $t) || preg_match('/DOCTORANT/ui', $etu))
        $u->positions = array("Doctorant");
      else if(preg_match("/Stagiaire/ui", $t))
        $u->positions = array("stagiaire");
      else {
        $u->positions = array_filter($d, function($e) { return stripos($e, 'département') === false; });
        
        if(array_filter($d, function($e) { return (stripos($e, 'département') !== false) && (stripos($e, 'Adjoint') === false); }))
          $u->set_director();
        if(array_filter($d, function($e) { return stripos($e, 'de gestion') !== false; }))
          $u->set_secretary();
      }

	    fix_ldap($u);
    }
  }
}

function ldap_get_members($department, $filter=null, $additionnal, $config=null) {
  global $ldap_config;
  $config = $config ? $config : $ldap_config;

  $conn = ldap_connect($config['uri']);

  if(!$conn)
    exit("LDAP: " . ldap_error($conn));

  ldap_set_option($conn, LDAP_OPT_PROTOCOL_VERSION, $config['version']);

  if(!ldap_bind($conn, $config['user'], $config['password']))
    exit("LDAP bind error: " . ldap_error($conn));

  //$department = 'INF';
  //$filter = array('aabdel-rahman');
  
  if($filter)
	  $f = '(|' . implode(array_map(function($id) { return '(uid=' . $id . ')'; }, $filter)) . ')';
  else
	  $f = '';

  if($department)
    $d = '(supannentiteaffectationprincipale=TSP/' . $department . ')';
  else
    $d = '';

  if($additionnal)
	  $a = '(|' . implode(array_map(function($id) { return '(uid=' . $id . ')'; }, $additionnal)) . ')';
  else
	  $a = '';
  
  $t = '(!(|' . implode(array_map(function($type) { return "(employeeType=*" . $type . "*)"; },
                                  array('associé', 'eleve', 'vacataire'))) . '))';
  
  $magic = '(|' . '(&' . $f . $d . ')' . $a . ')';

  //$magic = "(uid=thomas_g)";
  // remove meta_data to see all the fields
  $info = ldap_get_entries($conn, ldap_search($conn,
                                              $config['baseDN'],
                                              $magic,
                                              $config['meta_data']));

  //  explore database
  //for($i=0; $i<$info['count']; $i++) {
  //foreach($info[$i] as $k => $cur) {
  //print_r("------<br>");
  //print_r($k);
  //print_r(" => ");
  //print_r($cur);
  //print_r("<br>");
  //}
  //print_r(ldap_get_single($info[$i], 'mail'));
  //print_r("<br>");
  //print_r($info);
  //}
  //exit(1);

  $res = array();

  for($i=0; $i<$info['count']; $i++) {
    $u = new TSPUser(ldap_get_single($info[$i], 'uid'));

    //print_r($u->uid);
    //print_r("<br>");
    $u->first_name = capitalize(ldap_get_single($info[$i], 'givenname'));
    $u->last_name = capitalize(ldap_get_single($info[$i], 'sn'));
    $web = ldap_get_single($info[$i], 'labeleduri');
	  $u->web = $web === 'http://www.it-sudparis.eu' ? null : $web;
	  $u->mail = ldap_get_single($info[$i], 'mail');
	  $phone = str_replace(' ', '', ldap_get_single($info[$i], 'telephonenumber'));
    $u->phone = $phone && $phone !== '016076' ? $phone : null;
	  $u->location = ldap_get_single($info[$i], 'l');
	  $room = ldap_get_single($info[$i], 'roomnumber');
    $u->office = $room && !preg_match("/ABCDE/i", $room) ? preg_replace("/([BCD]) /ui", "$1", $room) : null;
    $u->jpeg = ldap_get_single($info[$i], 'jpegphoto');
    $u->ldap_account = true;

    $in = ldap_get_single($info[$i], 'employeetype');
    
    ldap_to_positions($u,
                      ldap_get_array($info[$i], 'edupersonaffiliation'),
                      explode(PHP_EOL, ldap_get_single($info[$i], 'description')),
                      ldap_get_single($info[$i], 'employeetype'),
                      ldap_get_single($info[$i], 'supannetuetape'));

    //echo $u->uid . ": " . $u->first_name . " " . $u->last_name . " - " . implode($u->positions, ", ") . "<br>";

    if($u->positions)
      $res[$u->uid] = $u;
  }

  //foreach($res as $u)
  //echo $u->uid . " - " . $u->first_name . ' ' . $u->last_name . ": " . implode(', ', $u->positions) . '<br>';

  return $res;
}

function ldap_get_members_simulate($filter, $file='last-dump.sql') {
  include $file;

  if(!$filter)
    return $dump;
  
  $redump = array();
  foreach($dump as $k => $v)
    if(in_array($v->uid, $filter))
      $redump[$k] = $v;
  return $redump;
}

function ldap_regen($dump) {
  $res = '$dump = array();<br>';

  foreach($dump as $cur) {
    $res .= '<br>';
    $res .= $cur->regen();
    $res .= '$dump["' . $cur->uid . '"] = $u;<br>';
  }
  
  // comment to generate the css for the categories
  return $res;
}

?>
