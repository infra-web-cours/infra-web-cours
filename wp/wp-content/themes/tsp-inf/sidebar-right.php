<?php if ( is_active_sidebar( 'right-sidebar' ) ) : ?>
	<div class="sidebar right-sidebar">
		<?php dynamic_sidebar( 'right-sidebar' ); ?>
	</div>
<?php endif; ?>
