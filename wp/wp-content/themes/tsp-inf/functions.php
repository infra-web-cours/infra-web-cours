<?php

$language = isset($language) ? $language : 'en';

$corporate = get_theme_mod('corporate') ? get_theme_mod('corporate') : 'tsp';

/* remove RUA protected pages and posts from search/archive/widgets */
/* directly the code of the RUA developer */
if(class_exists('RUA_App')) {
  add_action('pre_get_posts', function($query) {
    if (!is_admin() && !$query->is_singular() && (!isset($query->query["post_type"]) || $query->query["post_type"] != RUA_App::TYPE_RESTRICT)) {
      global $wpdb;
      $other_levels = array_diff(
        array_keys(RUA_App::instance()->get_levels()),
        rua_get_user()->get_level_ids()
      );
      $result = $wpdb->get_col("SELECT m.meta_value FROM $wpdb->postmeta m INNER JOIN $wpdb->posts p ON m.post_id = p.ID WHERE m.meta_key = '_ca_post_type' AND p.post_parent IN ('".implode("','", $other_levels)."')");
      if($result) {
        $query->set('post__not_in', $result);
      }
    }
    return $query;
  });
}

/*Navigation Menus*/
function register_my_menu() {
  register_nav_menu('header-menu',__( 'Header Menu' ));
}

add_action( 'init', 'register_my_menu' );

// WordPress Titles
add_theme_support( 'title-tag' );

// Add scripts and stylesheets
function load_scripts() {
  global $corporate;
  
	wp_enqueue_style('bootstrap-reboot', get_template_directory_uri() . '/bootstrap/css/bootstrap-reboot.min.css', array(), '4.4.1' );
	wp_enqueue_style('style-base', get_template_directory_uri() . '/style.css', array('bootstrap-reboot') );
	wp_enqueue_style('style', get_template_directory_uri() . '/' . $corporate . '/style.css', array('style-base') );
	wp_enqueue_script('menu', get_template_directory_uri() . '/menu.js', array(), null, true);
}

add_action( 'wp_enqueue_scripts', 'load_scripts' );

function load_fonts() {
	wp_register_style('OpenSans', '//fonts.googleapis.com/css?family=Open+Sans');
	wp_enqueue_style( 'OpenSans');

	wp_register_style('fontawesome', 'https://use.fontawesome.com/releases/v5.10.0/css/all.css');
	wp_enqueue_style( 'fontawesome');
}

add_action('wp_print_styles', 'load_fonts');


function sidebar_register() {
	register_sidebar(array('name'          => 'Left sidebar',
												 'description'   => '',
												 'id'            => 'left-sidebar',
												 'class'         => 'sidebar',
												 'before_widget' => '<div class="widget">',
												 'after_widget'  => '</div>',
												 'before_title'  => '<h2>',
												 'after_title'   => '</h2>'));

	register_sidebar(array('name'          => 'Right sidebar',
												 'description'   => '',
												 'id'            => 'right-sidebar',
												 'class'         => 'sidebar',
												 'before_widget' => '<div class="widget">',
												 'after_widget'  => '</div>',
												 'before_title'  => '<h2>',
												 'after_title'   => '</h2>'));
}

add_action( 'widgets_init', 'sidebar_register' );

// customize the link associated to the tagline
function tsp_customizer($wp_customize) {
  //adding section in wordpress customizer   
  $wp_customize->add_section('tsp_section', array(
    'title'          => 'TSP theme options'
  ));

  //adding setting for copyright text
  $wp_customize->add_setting('tagline_link', array(
    'default'        => '',
  ));

  $wp_customize->add_control('tagline_link', array(
    'label'   => 'Tagline link',
    'section' => 'tsp_section',
    'type'    => 'url',
  ));

  $wp_customize->add_setting('corporate', array(
    'default'        => 'tsp',
  ));

  $wp_customize->add_control('corporate', array(
    'label'   => 'Theme',
    'section' => 'tsp_section',
    'type'    => 'select',
    'choices' => array(
      'tsp' => __( 'Telecom SudParis' ),
      'ipparis' => __( 'IP Paris' ),
    )
  ));
}

add_action('customize_register', 'tsp_customizer');

?>
