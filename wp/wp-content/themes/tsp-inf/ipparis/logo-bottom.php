<table class='borderless' style='text-align: left'>
  <tr>
    <td>
      <a href="https://www.ip-paris.fr/en/">
        <table class='borderless'>
          <tr>
            <td>
              <span class='inline-block' style='margin-right: 20px'>
	              <img src="<?php echo get_bloginfo('template_directory'); ?>/ipparis/ipparis.png" 
			               width="64"
			               height="64"
			               alt="IP Paris"/>
              </span>
            </td>
            <td>
              <b style='font-variant: small-caps;'>Institut<br>
                Polytechnique<br>
                de Paris</b>
            </td>
          </tr>
        </table>
      </a>
    </td>
  </tr>
  <tr>
    <td>
      <span class='inline-block' style='margin-right: 8px'>
        <a href="https://www.polytechnique.edu/en/">
	        <img src="<?php echo get_bloginfo('template_directory'); ?>/ipparis/x.png" 
			         width="38"
			         height="64"
			         alt="Polytechnique"/>
        </a>
      </span>
  
      <span class='inline-block' style='margin-right: 8px'>
        <a href="https://www.ensta-paristech.fr/en">
	        <img src="<?php echo get_bloginfo('template_directory'); ?>/ipparis/ensta.png" 
			         width="46"
			         height="64"
			         alt="ENSTA"/>
        </a>
      </span>

      <span class='inline-block' style='margin-right: 8px'>
        <a href="https://www.telecom-paris.fr/en/">
	        <img src="<?php echo get_bloginfo('template_directory'); ?>/ipparis/tp.png" 
			         width="45"
			         height="64"
			         alt="Telecom Paris"/>
        </a>
      </span>

      <span class='inline-block'>
        <a href="https://www.telecom-sudparis.eu/en/">
	        <img src="<?php echo get_bloginfo('template_directory'); ?>/ipparis/tsp.png" 
			         width="45"
			         height="64"
			         alt="Telecom SudParis"/>
        </a>
      </span>
    </td>
  </tr>
</table>
