<!doctype html>
<html lang="<?= $language ?>">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="Gaël Thomas">

	  <?php wp_head(); ?>
  </head>

	<body>
		<div class='header'>
      <?php
      global $corporate;
      include(dirname(__FILE__) . '/' . $corporate . '/logo-top.php');
      ?>
      
			<a class='title' href="<?php echo get_bloginfo( 'wpurl' ) ;?>">
				<h1><?php echo get_bloginfo('name'); ?></h1>
			</a>
      
			<a class='tagline' href="<?= get_theme_mod('tagline_link') ? get_theme_mod('tagline_link') : network_home_url();?>">
				<?php echo get_bloginfo('description'); ?>
			</a>

			<div class='navbar dropdown-area'>
				<div class='navbar-toggler dropdown-button' id='main-menu'><!-- style='background-color: cyan;'> -->
					<div class="navbar-toggler-icon"></div>
				</div>

				<div class='navbar-menu tsp-menu' data-toggler='main-menu'><!-- style='background-color: lightgreen;'> -->
					<?php wp_nav_menu(array('container' => '')); ?>
				</div>
				
				<div class='navbar-search'>
					<div class='dropdown-target fade-height' data-from='main-menu'>
						<div class='dropdown-button d-none d-md-block' id='search'><i class='fas fa-search'></i></div>
						<form role='search' 
									class='search-menu dropdown-target fade-height' 
									data-from='search'
									action="<?= get_home_url(); ?>">
							<div>
								<input type="text" name="s" placeholder="Search" aria-label="Search"/>
								<button type='submit'><i class='fas fa-search'></i></button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>

