
  	<div class='footer'>
			<div class='logo'>
        <?php
        global $corporate;
        include(dirname(__FILE__) . '/' . $corporate . '/logo-bottom.php');
        ?>
			</div>

			<div class='address'>
        <?php
        global $corporate;
        include(dirname(__FILE__) . '/' . $corporate . '/address.php');
        ?>
			</div>
		
			<div class='copyright'>
        <?php
        global $corporate;
        include(dirname(__FILE__) . '/' . $corporate . '/copyright.php');
        ?>
			</div>
		</div>

    <!-- JS files: jQuery first, then Popper.js, then Bootstrap JS -->
		<div>
			<?php wp_footer(); ?>
		</div>
	</body>
</html>
