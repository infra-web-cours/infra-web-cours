<?php get_header(); ?>

<div class='content'>
<div class='main'>

<?php
if(have_posts()):
	while (have_posts()):
		the_post();

?>
      <h2><?php the_title(); ?><br></h2>
      <?php the_category(); ?>
		  <?php
		  the_content();

    edit_post_link(__('Edit'), '<p>', '</p>');
	endwhile;

  if(!is_page()): ?>
  <div class='row'>
<?php if(is_single()): ?>
		<div style='width: 100%; text-align: left'><?php next_post_link('&#8592; %link', '%title', is_category()); ?></div>
		<div style='width: 100%; text-align: right'><?php previous_post_link('%link &#8594;', '%title', is_category()); ?></div>
<?php   else: ?>
		<div style='width: 100%; text-align: left'><?php previous_posts_link( '&#8592; Newer posts' ); ?></div>
		<div style='width: 100%; text-align: right'><?php next_posts_link( 'Older posts &#8594;' ); ?></div>
<?php endif; ?>
	</div>
  <p></p>

<?php 
  endif;
endif;
?>

</div>

<?php get_sidebar("left"); ?>
<?php get_sidebar("right"); ?>
</div> <!-- content -->


<?php get_footer(); ?>
