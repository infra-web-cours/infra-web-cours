<?php

// lot of work to:
// - eliminate the search results that only have matching pattern in html tags (href for example)
// - highlight where are the results

get_header(); 

function search_highlight($content, $found, $justContains = false) {
  if($found)
    return '';

  $res = '';
  $str = strip_tags($content);
  foreach(explode(' ', sanitize_text_field(get_query_var('s'))) as $term) {
    $pos = stripos($str, $term);
    if($pos !== false) {
      $tlen = strlen($term);
      $slen = strlen($str);

      $before = 20;
      $after = 40;

      if($justContains || $pos < $before) {
        $start = 0;
      } else {
        //echo "str: " . substr($str, $pos - $before, $before) . "<br>";
        preg_match('/^[[:alpha:]]*[^[:alpha:]]+([[:alpha:]])/', substr($str, $pos - $before, $before), $matches, PREG_OFFSET_CAPTURE);
        //echo $matches[1][1] . "<br>";
        $start = $pos - $before + (empty($matches) ? 0 : $matches[1][1]);
      }

      if($justContains || $pos + $tlen + $after >= $slen) {
        $end = $slen;
      } else {
        //echo "str2: " . substr($str, $pos + $tlen, $after) . "<br>";
        preg_match('/([[:alpha:]])[^[:alpha:]]+[[:alpha:]]*$/', substr($str, $pos + $tlen, $after), $matches, PREG_OFFSET_CAPTURE);
        $end = $pos + $tlen + (empty($matches) ? 0 : $matches[1][1] + 1);
      }

      //echo "pos: $pos, start: $start, end: $end<br>";

      $s1 = substr($str, $start, $pos - $start);
      $s2 = substr($str, $pos, $tlen);
      $s3 = substr($str, $pos + $tlen, $end - $pos - $tlen);
      $res = '<p>' . ($start > 0 ? '[...] ' : '') . $s1 . '<span style="color: var(--title); text-decoration: underline;">' . $s2 . '</span>' . $s3 . ($end < $slen ? ' [...]' :  '') . '</p>';

      return $res;
    }
  }
  return '';
}

function make_search_result($post) {
  $res = '';

  if($post->post_type === 'job_cpt') {
    $title = format_job_title($post);
    $found_in_title = search_highlight($title, false, true);
    $content = search_highlight(get_the_content($post), $found_in_title);
  } else if($post->post_type === 'seminar_cpt') {
    $title = 'Seminar - ' . get_the_title($post);
    $found_in_title = search_highlight($title, false, true);
    $content = search_highlight(format_seminar_short(get_post()), $found_in_title);
    foreach (get_post_meta($post->ID) as $key => $value)
      $content = search_highlight($value[0], $content);
  } else {
    $title = get_the_title();
    $found_in_title = search_highlight($title, false, true);
    $content = search_highlight(get_the_content($post), $found_in_title);
  }

  if($found_in_title || $content) {
    $title = $found_in_title ? $found_in_title : $title;
    $res  = '<div class="search-result"><a href="' . get_the_permalink($post) . '"><h3>' . $title . '</h3>';
    $res .= ($content ? '<p>' . $content . '</p>' : '') . '</a></div>';
  }

  return $res;
}

$res_search = array();

if(have_posts()) {
  while(have_posts()) {
    the_post(); 
    $tmp = make_search_result(get_post());
    if($tmp)
      $res_search[] = $tmp;
  }
}

?>

<div class='content'>
  <div class='main'>
	  <h2>
		  <?php echo count($res_search) ?> <?php _e( 'search results found for', 'locale' ); ?>: 
		  "<?php the_search_query(); ?>"
	  </h2>
  
	  <?php
    foreach($res_search as $elmt)
      echo $elmt;
    ?>
  </div>

  <?php get_sidebar("left"); ?>
  <?php get_sidebar("right"); ?>
</div> <!-- content -->


<?php get_footer(); ?>
