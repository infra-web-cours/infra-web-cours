#! /bin/bash

USER=gael
HOST=docker1
SRC=$(dirname $0)/
DEST=infra-web-cours/wp
#www-docker/volumes/wp/

for cur in "wp-content/themes/cs-ipparis" "wp-content/themes/tsp-inf" "wp-content/plugins/tsp-inf" "wp-content/plugins/seminars-and-jobs"; do
    rsync --rsh=ssh -arv --chmod=g+rw,Dg+x --exclude='*~' "$SRC/$cur/" "$USER@$HOST:$DEST/$cur"
done
