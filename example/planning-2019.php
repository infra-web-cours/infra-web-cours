<?php

//
//   La plupart des enseignants sont déjà dans infra-web-cours/infra-web-cours/new/teachers.php
//   et ce fichier est inclus sans que vous n'ayez rien à faire.
//   N'hésitez pas à me demander si vous voulez ajouter des enseignants.
//
//   J'ai ajouté des enseignants ici pour avoir des noms rigolos (et indépendants du fichier teachers.php)
//
$camille = new Teacher('Camille la Chenille', 'camille.la-chenille at telecom-sudparis.eu');
$loulou = new Teacher('Loulou le Pou', 'loulou.le-pou at telecom-sudparis.eu');
$mireille = new Teacher('Mireille l\'Abeille', 'mireille.l-abeille at telecom-sudparis.eu');
$simeon = new Teacher('Siméon le Papillon', 'simeon.le-papillon at telecom-sudparis.eu');
$carole = new Teacher('Carole la Luciole', 'carole.la-luciole at telecom-sudparis.eu');
$lulu = new Teacher('Lulu la Tortue', 'lulu.la-tortue  at telecom-sudparis.eu');
$marie = new Teacher('Marie la fourmi', 'larie.la-fourmi at telecom-sudparis.eu');
$patouch = new Teacher('Patouch la mouche', 'Patouch.la-mouche at telecom-sudparis.eu');
$benjamin = new Teacher('Benjamin le lutin', 'benjamin.le-lutin at telecom-sudparis.eu');
$leonard = new Teacher('Léonard le têtard', 'léonard.le-têtard at telecom-sudparis.eu');
$victor = new Teacher('Victor le castor', 'victor.le-castor at telecom-sudparis.eu');
$oscar = new Teacher('Oscar le cafard', 'oscar.le-cafard at telecom-sudparis.eu');
$adrien = new Teacher('Adrien le lapin', 'adrien.le-lapin at telecom-sudparis.eu');

//
//  En argument: l'année (n'importe quelle chaîne de caractères) suivie des responsables du module
//
$schedule = new Schedule("2019/2020", $camille, $loulou);

//
//   Première étape : on cré les groupes
//   La couleur est obligatoire, elle permet de rendre le planning à peu près lisible
//
$gA = $schedule->createGroup('A', 'Cyan',       'B02', $mireille, $oscar);
$gB = $schedule->createGroup('B', 'Cornsilk',   'B02', $simeon, $oscar);
$gC = $schedule->createGroup('C', 'LightPink',  'B06', $carole, $tba);     // $tba veut dire "to be anounced"
$gD = $schedule->createGroup('D', 'Khaki',      'B07', $lulu);             // pas d'assistant dans ce groupe
$gE = $schedule->createGroup('E', 'PaleGreen',  'B02', $marie, $oscar);
$gF = $schedule->createGroup('F', 'PeachPuff',  'B03', $patouch, $tba);
$gG = $schedule->createGroup('G', 'PowderBlue', 'B06', $benjamin, $oscar);
$gH = $schedule->createGroup('H', 'Thistle',    'B07', $leonard, $tba);
$gI = $schedule->createGroup('I', 'Cornsilk',   'B111', $victor, $oscar);

//
//   Seconde étape : on cré les regroupement de groupes qui ont des cours au même moment
//
$g1 = [ $gA, $gB, $gC, $gD ];
$g2 = [ $gE, $gF, $gG, $gH, $gI ];

//
//   Troisième étape : on cré les créneaux
//
//   Arguments: jour, debut, fin, nombre-enseignants, debut, fin, nombre-enseignants, ...
//
$cm1   = new Slot('17/9/2019',  '8:15',  '11:15', 1);                        // un seul long créneau de 3h
$ci1g1 = new Slot('19/9/2019',  '8:15',  '9:45',  1, '10:00', '11:30', 2);   // deux petits créneaux de 1h30 chacun
$ci1g2 = new Slot('17/9/2019',  '12:45', '14:15', 1, '14:30', '16:00', 2);
$ci2g1 = new Slot('26/9/2019',  '8:15',  '9:45',  1, '10:00', '11:30', 2);
$ci2g2 = new Slot('25/9/2019',  '12:45', '14:15', 1, '14:30', '16:00', 2);
$ci3g1 = new Slot('2/10/2019',  '8:15',  '9:45',  1, '10:00', '11:30', 2);
$ci3g2 = new Slot('3/10/2019',  '8:15',  '9:45',  1, '10:00', '11:30', 2);
$ci4g1 = new Slot('9/10/2019',  '8:15',  '9:45',  1, '10:00', '11:30', 2);
$ci4g2 = new Slot('10/10/2019', '8:15',  '9:45',  1, '10:00', '11:30', 2);
$ci5g1 = new Slot('16/10/2019', '14:30', '16:00', 1, '16:15', '17:45', 2);
$ci5g2 = new Slot('17/10/2019', '8:15',  '9:45',  1, '10:00', '11:30', 2);
$ci6g1 = new Slot('23/10/2019', '8:15',  '9:45',  1, '10:00', '13:00', 2);
$ci6g2 = new Slot('5/11/2019',  '8:15',  '9:45',  1, '10:00', '13:00', 2);
$ci7g1 = new Slot('6/11/2019',  '8:15',  '9:45',  1, '10:00', '11:30', 2);
$ci7g2 = new Slot('7/11/2019',  '8:15',  '9:45',  1, '10:00', '11:30', 2);
$ci8g1 = new Slot('20/11/2019', '8:15',  '9:45',  1, '10:00', '11:30', 2);
$ci8g2 = new Slot('21/11/2019', '8:15',  '9:45',  1, '10:00', '11:30', 2);
$ci9g1 = new Slot('27/11/2019', '8:15',  '9:45',  1, '10:00', '11:30', 2);
$ci9g2 = new Slot('28/11/2019', '8:15',  '9:45',  1, '10:00', '11:30', 2);
$cf1   = new Slot('13/12/2019', '8:30',  '11:30', 12);
$cf2   = new Slot(null,         null,     null,   2); // on ne connait pas encore l'horaire

//
//   Troisième étape : on crée les cours
//
$schedule->createBloc('CM1')                               // le cours CM1
         ->add($cm1, 'Promo', 'LightGray', 'Amphi 10', $adrien);  // un seul group avec toute la promo

$schedule->createBloc('CI1')                               // le cours CI1
         ->addGroups($ci1g1, $g1)                          // avec un créneau pour les groupes A, B, C, D
         ->addGroups($ci1g2, $g2);                         // et un créneau pour les groupes E, F, G, H, I

$schedule->createBloc('CI2')                               // le cours CI2
         ->addGroups($ci2g1, $g1)                          // avec un créneau pour les groupes A, B, C, D
         ->addGroups($ci2g2, $g2)                          // et un créneau pour les groupes E, F, G, H, I
         ->setRoom($gA, 'Sur la Lune')                     // pour ce cours, on change la salle du groupe A
         ->setRoom($gB, 'Sur Mars', 1)                     // et ici, uniquement la salle pour le seconde créneau
         ->setTeachers($gC, $camille, $loulou)             // remplace les enseignants pour les deux créneaux du groupe F 
         ->replaceTeacher($gD, 1, 1, $mireille)            // remplace le second enseignant du second créneau
         ->addTeacher($gE, 1, $camille)                    // ajoute un troisième enseignant au second créneau
         ->setTeachersAt($gF, 1, $simeon)                  // remplace tous les enseignants du second créneau
          ;

$schedule->createBloc('CI3')        // etc...
         ->addGroups($ci3g1, $g1)
         ->addGroups($ci3g2, $g2);

$schedule->createBloc('CI4')
         ->addGroups($ci4g1, $g1)
         ->addGroups($ci4g2, $g2);

$schedule->createBloc('CI5')
         ->addGroups($ci5g1, $g1)
         ->addGroups($ci5g2, $g2);

$schedule->createBloc('CI6')
         ->addGroups($ci6g1, $g1)
         ->addGroups($ci6g2, $g2);

$schedule->createBloc('CI7')
         ->addGroups($ci7g1, $g1)
         ->addGroups($ci7g2, $g2);

$schedule->createBloc('CI8')
         ->addGroups($ci8g1, $g1)
         ->addGroups($ci8g2, $g2);

$schedule->createBloc('CI9')
         ->addGroups($ci9g1, $g1)
         ->addGroups($ci9g2, $g2);

$schedule->createBloc('CF1')            // le CF1 avec plein de surveillants
         ->add($cf1, 'Promo', 'LightGray', 'E010/E011',  
               $camille, $loulou, $mireille, $simeon, $carole, $lulu, 
               $marie, $patouch, $benjamin, $leonard, $victor, $oscar);

$schedule->createBloc('CF2')            // et le CF2, on ne connait pas grand chose, mais on sait qu'il existe
         ->add($cf2, 'Promo', 'LightGray', '???', $adrien, $tba);

?>

