<?php

$root = dirname(__FILE__);       /* root directory of the module */
$web = '../infra-web-cours';     /* relative path of the web infrastructure (the infrastructure is in $root . '/' $web) */

$language = 'fr';                /* pour avoir les termes en anglais, il faut utiliser en */
$moduleId = 'CSC0000';           /* id of the module, used to generate the calendar */
$module = "Utilisation de l'infrastructure de développement web";  /* name of the module */

//$myCss = 'cours.css';            /* css of the module (optional) */

$planning = 'planning-2019.php'; /* calendar of the module, see the associated help section */

$nav = 'nav';                    /* nav file (technically, nav is in $root . '/' . $nav . '.php') */
$main = 'example';               /* main file (technically, main is in $root . '/' . $main . '.php') */

include($root . '/' . $web . '/tsp.php');
?>

<!-- Local Variables: -->
<!-- mode: web -->
<!-- indent-tabs-mode: nil -->
<!-- mode: flyspell -->
<!-- ispell-local-dictionary: "french" -->
<!-- coding: utf-8 -->
<!-- End: -->
