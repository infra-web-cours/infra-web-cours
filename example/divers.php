
<chapitre title='Divers'>
	<exercice title='Gestion des chemins'>
		<p>Il y a deux cas dans lesquels on a besoin de gérer des chemins relatifs.</p>
		<ul>
			<li>
				Pour inclure un fichiers php dans un autre en utilisant un chemin relatif par rapport au fichier
				en cours, il suffit d'utiliser le <code>include</code> de php,
				typiquement, avec&nbsp;:
				<code class="code-block">&lt;?php include(dirname(__FILE__) . '/a/b/truc.php'); ?&gt;</code>
			</li>
			<li>
				Pour générer des liens relatifs par rapports au fichier en cours, une fonction php vous est fournie&nbsp;:
				la fonction <code>makeRelative</code>. Par exemple, si vous voulez créer
				un lien se trouvant en <code>../a/f.c</code> à partir du fichier actuel, il vous suffit
				de mettre&nbsp;:
				<code class='code-block' prettify lang='html'>&lt;a url='&lt;?= makeRelative(__FILE__, "/../a/f.c"); ?&gt;'&gt;Lien&lt;/a&gt;</code>

				<remarque>
					Il ne faut pas oublier le "/" au début du chemin relatif. 
				</remarque>
			</li>
		</ul>
	</exercice>

	<exercice title="Configuration avancée de l'affichage">
		<p>On peut aussi modifier entièrement l'affichage via un css local au cours. 
		  Pour cela, il suffit d'ajouter à l'aiguilleur&nbsp;:
		  <code class='code-block'>$myCss = 'web/cours.css'</code>
		  La chemin <code>web/cours.css</code> est relatif à la racine du module (i.e., au répertoire dans lequel
		  se trouve l'aiguilleur). Ensuite, dans le css, on peut modifier n'importe quelle partie de l'affichage, par exemple
		  avec&nbsp;:</p>
    
		<code url='cours.css'></code>

		<p>On peut aussi bien sûr modifier localement l'affichage en utilisant une balise englobante comme dans cet exemple
		  (rendu <a href='?page=sample2'>ici</a>)&nbsp;:</p>
		<code url='sample2.php'></code>
	</exercice>
</chapitre>


<!-- Local Variables: -->
<!-- mode: web -->
<!-- mode: flyspell -->
<!-- ispell-local-dictionary: "french" -->
<!-- coding: utf-8 -->
<!-- End: -->
