<!-- n'affiche que les min-week <= 2 -->
  <ul class='current-week' data-current-week='2'>             <!-- ul est un marqueur de menu ou sous menu -->
    <li>           <!-- et un li indique une entrée d'un menu -->
      Organisation
      <ul>
        <li><?php echo "<a href='?page=" . $main . "'>Page principale</a>"; ?></li>
        <li><a href='?page=planning'>Planning</a></li>
        <li><a href='?page=all'>Tous les sujets de TP</a></li>
        <li class='min-week' data-min-week='2'>On est en semaine 2&nbsp;!</li>
        <li class='min-week' data-min-week='3'>Ceci ne s'affichera que quand data-current-week vaudra 3</li>
      </ul>
    </li>

    <li>
      Supports
      <ul class='chapitres'> <!-- le marqueur chapitres est utilisé pour générer le séquencement -->

        <li>                 <!-- chaque li définit un chapitre -->
          <chapter-id>C1</chapter-id>
          <chapter-title>Installation</chapter-title> <!-- utilisé à la fois dans le menu est dans le séquencement -->
          <chapter-content>
            <ul> <!-- le ul ici est important, c'est ce qui indique que c'est un sous-menu du chapitre -->
              <li><a class='all-in-one' href='?page=installation'>Cours</a></li>
            </ul>
          </chapter-content>
          <chapter-notion>
            <ul> <!-- le ul ici n'est pas important, cette zone n'est affichée que dans le séquencement -->
              <li>Test avec PhP</li>
              <li>Installation complète avec Apache</li>
            </ul>
          </chapter-notion>
        </li>

        <li>
          <chapter-id>C2</chapter-id>
          <chapter-short>Création d'un cours</chapter-short><!-- si chapter-short présent, utilisé dans les menus -->
          <chapter-title>Création d'un cours complet</chapter-title><!-- et le long ailleurs -->
          <chapter-content>
            <ul>
              <li><a class='all-in-one' href='?page=creation'>Cours</a></li>
            </ul>
          </chapter-content>
          <chapter-notion>
            <ul>
              <li>Création d'un cours complet</li>
              <li>Fonctionnement de l'aiguilleur</li>
            </ul>
          </chapter-notion>
        </li>

        <li>
          <chapter-id>C3</chapter-id>
          <chapter-title>Les balises</chapter-title>
          <chapter-content>
            <ul>
              <li><a class='all-in-one' href='?page=balises'>Cours</a></li>
              <li><a class='all-in-one' href='?page=new'>Un petit exemple</a></li>
            </ul>
          </chapter-content>
          <chapter-notion>
            <ul>
              <li>Liste de toutes les balises</li>
            </ul>
          </chapter-notion>
        </li>


        <li>
          <chapter-id>C4</chapter-id>
          <chapter-title>Gérer des planning</chapter-title>
          <chapter-content>
            <ul>
              <li><a class='all-in-one' href='?page=edt'>Cours</a></li>
            </ul>
          </chapter-content>
          <chapter-notion>
            <ul>
              <li>Générer et afficher des plannings</li>
            </ul>
          </chapter-notion>
        </li>

        <li>
          <chapter-id>C5</chapter-id>
          <chapter-title>Informations diverses</chapter-title>
          <chapter-content>
            <ul>
              <li><a class='all-in-one' href='?page=divers'>Cours</a></li>
            </ul>
          </chapter-content>
          <chapter-notion>
            <ul>
              <li>Gestion des chemins dans les cours</li>
              <li>Configuration avancée de l'affichage</li>
            </ul>
          </chapter-notion>
        </li>


        <!-- quelques exemples un peu plus avancés -->
        <li> 
          <chapter-id>
            <expanded-version><img src='alarm.png' width="14px"></img></expanded-version> <!-- ignoré dans le menu -->
            A1
          </chapter-id>

          <!-- si chapter-short présent, utilisé dans les menus -->
          <chapter-short>Exemple avancé</chapter-short>
          <!-- le titre long est utilisé ailleurs -->
          <chapter-title>Exemple avancé avec un titre mega long car j'aime bien les titres longs</chapter-title> 

          <chapter-content>
            <ul>
              <li>
                <a href='#'>Le cours (pas de lien, c'est normal)</a>
                <expanded-version> &ndash; 20mn</expanded-version> <!-- ignoré dans le menu -->
              </li>
              <li>
                <a href='#'>TP (pas de lien, c'est normal)</a>
                <expanded-version> &ndash; 15mn + 1h25</expanded-version>
              </li>

              <li ignore>Ma partie du cours que je ne veux pas rendre visible</li>
            </ul>
          </chapter-content>
          <chapter-notion>
            Le format de la case notion est totalement libre.
          </chapter-notion>
        </li>

        <li only-schedule> <!-- ce chapitre n'est affiché que dans le tableau de séquencement -->
          <!-- on peut utilise only-nav pour n'afficher un chapitre que dans le menu (comportement inverse) -->
          <chapter-id>
            CI42
          </chapter-id>
          <chapter-title>Chapitre qui n'apparaît que dans schedule et pas dans le menu</chapter-title>
          <chapter-content>
            <ul>
              <li>
                <a href='#'>Une ressource</a>
                <expanded-version> &ndash; 20mn</expanded-version> <!-- ignoré dans le menu -->
              </li>
            </ul>
          </chapter-content>
          <chapter-notion>
            L'attribut schedule-only
          </chapter-notion>
        </li>
      </ul>
    </li>
  </ul>
</week>

<!-- Local Variables: -->
<!-- mode: web -->
<!-- indent-tabs-mode: nil -->
<!-- mode: flyspell -->
<!-- ispell-local-dictionary: "french" -->
<!-- coding: utf-8 -->
<!-- End: -->
