
<!-- on commence par charger l'interface de planning -->
<?php include($root . '/' . $web . '/planning-interface.php'); ?>

<!-- On peut afficher l'année en accédant à $schedule->year -->
<chapitre title="<?= 'Equipe pedagogique ' . $schedule->year ?>">
  <p>L'aide permettant de comprendre comment cette page
    est générée se trouve <a href="?page=edt">ici</a></p>

  <!-- on peut afficher les coordinateurs de la façon suivante -->
  <p>Responsables : <?= $schedule->displayCoordinators(); ?></p>
  <!-- et le enseignants qui s'occupent des différents groupes comme ça -->
  <!-- l'argument citpMetaSlot signifie que sur chaque séance et pour chaque groupe, -->
  <!-- il y a une partie cours avec un unique enseignant -->
  <!-- et une partie TP avec deux enseignants -->
  <!-- vous pouvez utiliser ciMetaSlot si il n'y a qu'un unique enseignant par séance -->
  <?= $schedule->displayGroups($citpMetaSlot); ?>

  <!-- un lien vers la sythèse des heures enseignées, c'est utile -->
  <p><a href='?page=synthese-enseignements'>Synthèse des heures enseignées</a></p>
</chapitre>

<chapitre title="<?= 'Planning ' . $schedule->year ?>">
  <!-- un selecteur pour pouvoir charger un calendrier associé à l'un des enseignants -->
  <?= $schedule->displayICSSelector() ?>

  <!-- affichage du planning complet -->
  <?= $schedule->displaySchedule($citpMetaSlot); ?>
</chapitre>
