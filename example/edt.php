<chapitre title='Gestion du planning'>
  <p>Pour gérer le planning, il faut les éléments suivants&nbsp;:</p>
  <ul>
    <li>une variable <code>$planning</code> indique où se trouve le planning de l'année en cours
      (partie qui change chaque année), cette variable doit être définit
      dans l'aiguilleur (le fichier <code>index.php</code>,</li>
    <li>un fichier <code>planning.php</code> qui s'occupe d'afficher le planning
      (partie fixe et indépendante de l'année),</li>
    <li>un fichier <code>synthese-enseignements.php</code> qui s'occupe de générer
      une synthèse des heures effectuées par les enseignants (bien utile pour déclarer les heures),</li>
  </ul>

  <p>Vous pouvez voir le rendu de l'exemple de planning décrit sur cette page <a href='?page=planning'>ici</a>.</p>

  <exercice title="Le planning de l'année en cours">
    <p>La construction d'un planning pour une année est relativement naturelle. Vous
      pouvez vous inspirer librement de l'exemple <a download href='planning-2019.php'>suivant</a> qui utilise
      toutes les fonctionnalités, y compris celle permettant
      de modifier les enseignants ou les salles pour certaines séances.</p>

		<code class='code-block'><?php highlight_file(dirname(__FILE__) . '/planning-2019.php'); ?></code>
  </exercice>

  <exercice title='Affiche du planning'>
    <p>Vous pouvez vous inspirer de l'exemple <a download href='planning.php'>suivant</a>&nbsp;:</p>

		<code class='code-block'><?php highlight_file(dirname(__FILE__) . '/planning.php'); ?></code>
  </exercice>

  <exercice title='Synthèse des heures enseignées'>
    <p>Vous pouvez vous inspirer de l'exemple <a download href='synthese-enseignements.php'>suivant</a>&nbsp;:</p>
    
		<code class='code-block'><?php highlight_file(dirname(__FILE__) . '/synthese-enseignements.php'); ?></code>
  </exercice>

</chapitre>

<!-- Local Variables: -->
<!-- mode: web -->
<!-- mode: flyspell -->
<!-- ispell-dictionary: "french" -->
<!-- coding: utf-8 -->
<!-- End: -->
