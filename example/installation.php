
<chapitre title='Installation et tests en local'>
  <p>Pour faire des tests en local, vous avez deux solutions. Vous
    pouvez soit avoir une installation minimale avec le package <code>php</code>
    (première exercice), soit avoir une installation complète incluant
    le serveur <code>apache</code> (second exercice).</p>

  <exercice title='Test en local'>
    <p>Pour faire des tests en local, il vous suffit d'installer le package
      <code>php</code>. Ensuite, vous avez deux possibilités&nbsp;:</p>
    <ul>
      <li>utiliser le script <code>start-server.sh</code>,</li>
      <li>lancer le mini-serveur web inclus dans
        le package <code>php</code> manuellement.</li>
    </ul>

    <question title='Le script start-server.sh'>
      <p>Le script <code>start-server.sh</code> se trouve à la racine du projet.
        Il lance le mini-serveur web inclus dans le package <code>php</code>.
        Ce script lance le serveur sur le port 8080 et utilise par défaut la racine de votre compte comme
        point de départ du serveur. </p>

      <p>Une fois le script lancé, il vous suffit de vous connecter en <code>localhost:8080/cours/csc3101/Supports/fise/</code>
        pour charger le fichier <code>index.php</code> ou <code>index.html</code>
        se trouvant dans votre répertoire <code>~/cours/csc3101/Supports/fise/</code>. <attention>Le slash 
        final est important</attention></p>

      <p>Si vous indiquez un répertoire dans lequel il n'y a pas de fichier <code>index.php</code>
        ou <code>index.html</code>, le script va gentiment vous lister le contenu
        de votre répertoire et vous permettre de vous promener dans votre arborescence.
        N'hésitez donc pas à simplement vous connecter à <code>localhost:8080</code>.</p>
      
      <p>Si vous avez besoin de lancer le script avec une racine spécifique, il vous suffit
        de passer cette racine en argument. Par exemple, <code>start-server.sh ~/cours</code>
        utilise <code>~/cours</code> comme racine.</p>
      
      <attention>Il faut que l'ensemble des répertoires de <code>infra-web-cours</code>
        soit accessibles à partir de la racine.</attention>
    </question>

    <question title='Lancement manuel'>
	    <p>Il suffit de lancer <code>php -S 0.0.0.0:8000</code> dans un répertoire pour lancer
	      le serveur Web sur le port 8000. 
        Il faut que les répertoires <code>infra-web-cours</code> et le répertoire
	      du cours qu'on souhaite tester soient accessibles à partir du répertoire.
	      Ensuite, dans un navigateur, il faut s'y connecter avec
	      <code>localhost:8000/chemin-vers-le-repertoire-du-cours</code>.</p>

	    <p>Chez moi, par exemple&nbsp;:</p>
	    <code class='code-block'>$ cd ~/cours
$ php -S 0.0.0.0:8000</code>

	    <p>Et dans le navigateur, j'utilise l'URL <code>http://localhost:8000/csc3101/Supports/fise/</code>.</p>
    </question>
  </exercice>

  <exercice title='Installation Apache/PHP sous Linux'>
	  <p>Il est nécessaire d'installer plusieurs paquets et de les configurer
	    avant de pouvoir utiliser l'infrastructure Web.
	    La présente méthode s'applique aux machines Debian.</p>

	  <question title='Installation des paquets'>
		  <p>Installer les paquets apache2, libapache2-mod-php, php-mbstring, php-intl et php-xml.</p>
		  <code class='code-block'>$ apt-get install apache2 libapache2-mod-php php-mbstring php-intl php-xml</code>
	  </question>

	  <question title="Configuration d'apache">
			<p>Créer le fichier <code>/etc/apache2/sites-available/moncours.conf</code>.</p>
			<code url="moncours.conf"></code>
		</question>

		<question>
			<p>Créer les répertoires de <code>log</code>&nbsp;:</p>
			<code class='code-block'>sudo mkdir -p /var/log/apache2/moncours</code>
		</question>

		<question>
			<p>Par défaut, apache refuse les sites web en dehors des répertoires classique 
			  (<code>/var/www</code>, <code>/usr/share</code>, etc.) 
			  Pour autoriser un autre répertoire, éditez le fichier <code>/etc/apache2/apache2.conf</code> 
			  et ajoutez&nbsp;:</p>

			<code class='code-block' prettify>&lt;Directory /home/login/MonCours/&gt;
  Options Indexes FollowSymLinks
  AllowOverride None
  Require all granted
&lt;/Directory&gt;</code>
		</question>

		<question>
			<p>Activer le nouveau site web, et redémarrer apache&nbsp;:</p>
			<code class='code-block'>$ sudo a2ensite moncours
$ sudo service apache2 restart</code>
		</question>

		<question>
			<p>Editer le fichier <code>/etc/hosts</code> afin d'ajouter un alias&nbsp;:</p>
			<code class='code-block'>127.0.0.1	introsysteme.local.org</code>
		</question>
	</exercice>
</chapitre>

<!-- Local Variables: -->
<!-- mode: web -->
<!-- mode: flyspell -->
<!-- ispell-local-dictionary: "french" -->
<!-- coding: utf-8 -->
<!-- End: -->
