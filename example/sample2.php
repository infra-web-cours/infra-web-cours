
<style>
 body {
	 counter-reset: stage; /* nouveau compteur nommé stage */
 }

.stage {
	 display: block;
	 background-color: yellow;
 }

.stage .exercice-toggle:lang(fr)::before { /* un exercice dans un .stage utilise le compteur .stage et l'affiche en upper-roman */
	 content: 'Stage ' counter(stage, upper-roman) ' : ' !important;;
	 counter-increment: stage !important;;
 }

 .stage .question-toggle:lang(fr)::before { /* une question dans un .stage utilise le compteur .stage au lieu du compteur exercice */
	 content: 'Stage-step ' counter(stage, upper-roman) '.' counter(question, lower-alpha) '. ' !important;
 }
</style>

<chapitre>
	<exercice title='exercice normal'>
		<question>question</question>
	</exercice>

	<div class='stage'><exercice title='exercice bizarre'>
		<question>question</question>
	</exercice></div>

	<exercice title='exercice normal'>
		<question>question</question>
	</exercice>

	<div class='stage'><exercice title='autre exercice bizarre'>
		<question>question</question>
	</exercice></div>
</chapitre>

<!-- Local Variables: -->
<!-- ispell-dictionary: "fr_FR" -->
<!-- mode: web -->
<!-- mode: flyspell -->
<!-- coding: utf-8 -->
<!-- End: -->
