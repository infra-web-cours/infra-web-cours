<chapitre title="Balises de l'infrastructures">
	<p>L'infrastructure Web fournit quelques balises permettant de structurer un énoncé.
	  Vous pouvez voir un exemple utilisant la plupart des balises
    <a target="_blank" href='?page=new'>ici</a>.</p>

	<exercice title="Structuration d'un énoncé">
		<p>Les balises <code>chapitre</code>, <code>exercice</code> et <code>question</code> et <code>reponse</code>
		  sont des boîtes cliquables. Si vous cliquez en haut (sur le titre, le numéro de question etc...), 
		  le contenu de la boite s'affiche si elle ne l'était pas et réciproquement.
		  Par défaut, les boites associées à <code>chapitre</code>, <code>exercice</code> et <code>question</code> sont
		  affichées et celles associées à <code>solution</code> ne le sont pas, sauf
		  si, dans l'URL, vous spécifiez <code>wrap=true</code>.
		  Vous pouvez changer ce comportement en utilisant l'attribut <code>unwrap</code> (positionné à <code>false</code>
		  on force l'affichage, positionné à autre chose, la boîte n'est pas affichée, désolé pour le contre-sens&nbsp;!).</p>

		<p>Les balises <code>exercice</code> et <code>question</code>
		  sont numérotées. Si vous voulez annuler ce comportement, il suffit
		  d'ajouter l'attribut <code>raw</code> à la balise.</p>
	</exercice>

  <exercice title='Comment cacher les solutions'>
    <question title='Cacher les solutions avec la balise <code>&lt;no-soluce&gt;</code>'>
      <no-soluce></no-soluce>
      <p>Dans cette question, on utilise la balise <code>&lt;no-soluce&gt;</code> pour cacher la solution qui suit.
        Les solutions ne sont pas envoyées au client, ce qui fait que les étudiants ne peuvent pas les voir, même en
        inspectant le code source <code>HTML</code>. </p>

      <p>Lorsque le serveur tombe sur cette balise, il considère que toutes les solutions atteignable
        à partir du <b>parent</b> qui contient <code>no-soluce</code> ne doivent plus être affichées.
        Il suffit donc de placer cette balise à la racine d'un document pour cacher toutes
        solutions.</p>

      <solution>Cette solution n'est pas envoyée au client</solution>
    </question>

    <question title="Cacher les solutions avec l'attribut <code>no-soluce</code>" no-soluce>
      <p>Lorsque le serveur tombe sur une balise qui possède l'attribut <code>no-soluce</code>,
        le serveur n'envoie plus les solutions sur l'ensemble des balises <b>enfants</b></code>.</p>

      <solution>Cette solution n'est pas envoyée non plus</solution>
    </question>

    <question title='Dans cette question on voit toujours les réponses'>
      <p>À votre avis, pourquoi&nbsp;? (pour afficher la solution, ajoutez <code>&amp;soluce=true</code> à l'URL.</p>

      <solution>Techniquement, aucun parent de la balise question englobante no possède l'attribut
        <code>no-soluce</code>, et il n'existe pas de balise <code>no-soluce</code> dans aucun des parents.
      </solution>
    </question>
  </exercice>

	<exercice title='La balise <code>code</code>'>
		<p>L'infrastructure propose une balise <code>&lt;code&gt;</code> permettant d'insérer du code.
		  Cette balise utilise les options suivantes&nbsp;:</p>
		<ul>
			<li>L'attribut <code>class='code-block'</code>&nbsp;: lorsque cette option est ajoutée,
				le code apparaît dans un bloc séparé avec un fond coloré, sinon, le code est 
				inséré en ligne dans le paragraphe courant.
				Voici du <code>code en ligne</code> et voici du <code class='code-block'>code en bloc.</code>
			</li>
			<li><code>&lt;code url='...'&gt;&lt;/code&gt;</code>&nbsp;:
				Insère le code qui se trouve à l'url directement dans le format <code>code-block</code>.
				Le code est précédé d'un lien pour télécharger le code.
				Par exemple, le rendu de <code>&lt;code url='sample.c'&gt;&lt;/code&gt;</code> est&nbsp;:
				<code url='sample.c'></code>
			</li>
			<li>L'attribut <code>only-ref</code> ajouté à la balise <code>&lt;code&gt;</code> permet de n'insérer
        que le lien vers le code, mais sans le code lui même.
				Par exemple, le rendu du <code>&lt;code url='cours.css' only-ref&gt;&lt;/code&gt;</code>
				est <code url='cours.css' only-ref></code>.</li>
			<li>L'attribut <code>prettify</code> indique qu'il faut colorer le code, et l'attribut
				<code>noprettify</code> indique qu'il ne faut pas colorer le code.
				Par défaut, l'attribut <code>prettify</code> est utilisé par défaut si l'attribut <code>url</code>
				est utilisé (comme dans l'exemple d'affichage coloré du fichier <code>sample.c</code> précédent),
				et l'attribut <code>noprettify</code> est utilisé sinon.
				La coloration est assurée par le projet externe <a href='https://github.com/google/code-prettify'>google prettify</a>.</li>
			<li>L'attribut <code>lang='...'</code> permet, pour du code coloré qui serait mal
				reconnu, d'indiquer le langage a utilisé. En général, il suffit de mettre l'extension utilisée,
				comme avec <code>'java'</code>, 
				sauf pour <code>bash</code> pour lequel il faut utiliser <code>'bsh'</code>.</li>
		</ul>
	</exercice>

	<exercice title='Liste des balises'> 
		<p>Voici une liste (probablement non-exhaustive) des balises définies par l'infrastructure&nbsp;:</p>
		<table>
			<tr>
				<td  width='350px'><code>&lt;chapitre title='titre'&gt; ... &lt;/chapitre&gt;</code></td>
				<td>Définit un nouveau chapitre de nom <code>title</code>. L'argument <code>title</code> est obligatoire.</td>
			</tr>
			
			<tr>
				<td><code>&lt;exercice title='titre'&gt; ... &lt;/exercice&gt;</code></td>
				<td>Définit un nouvel exercice de nom <code>title</code>. L'argument <code>title</code> est optionnel.</td>
			</tr>
			
			<tr>
				<td><code>&lt;question title='titre'&gt; ... &lt;/question&gt;</code></td>
				<td>Définit une nouvelle question de nom <code>title</code>. L'argument <code>title</code> est optionnel.</td>
			</tr>

			<tr>
				<td><code>&lt;partie&gt; Titre partie &lt;/partie&gt;</code></td>
				<td>Définit une partie d'un exercice. Permet de séparer logiquement des ensembles de questions d'un exercice.</td>
			</tr>

			<tr>
				<td><code>&lt;solution&gt; ... &lt;/solution&gt;</code></td>
				<td>Donne une réponse qui n'est affichée que si la page reçoit l'option <code>soluce=true</code></td>
			</tr>

			<tr>
				<td><code>&lt;tip&gt; ... &lt;/tip&gt;</code></td>
				<td>Similaire à solution, mais toujours affiché pour que les étudiants puissent accéder à de l'aide
          (regardez l'aide pour en savoir plus :)).
				<tip>En ajoutant la classe <code>as-paragraph</code>, le conseil s'affiche comme un bloc
          avec des marges en bas.</tip></td>
			</tr>

			<tr>
				<td><code>&lt;details&gt; ... &lt;/details&gt;</code></td>
				<td>Similaire à solution, mais toujours affiché pour que les étudiants puissent accéder aux détails
          (regardez les détails pour en savoir plus :)).
				<details>En ajoutant la classe <code>as-paragraph</code>, le conseil s'affiche comme un bloc
          avec des marges en bas</details></td>
			</tr>
			
			<tr>
				<td><code>&lt;code &gt; ... &lt;/code&gt;</code></td>
				<td>Insère du code.</td>
			</tr>

			<tr>
				<td><code>&lt;attention&gt; ... &lt;/attention&gt;</code></td>
				<td>Affiche un attention.</td>
			</tr>

			<tr>
				<td><code>&lt;remarque&gt; ... &lt;/remarque&gt;</code></td>
				<td>Affiche une remarque.</td>
			</tr>

			<tr>
				<td><code>&lt;enconstruction&gt;&lt;/enconstruction&gt;</code></td>
				<td>Affiche un message qui explique que la page est en cours de construction.</td>
			</tr>

			<tr>
				<td><code>&lt;objectif&gt; ... &lt;/objectif&gt;</code></td>
				<td>Affiche un objectif.</td>
			</tr>

			<tr>
				<td><code>&lt;bravo&gt; ... &lt;/bravo&gt;</code></td>
				<td>Affiche des félicitations pour encourager les étudiants.</td>
			</tr>

			<tr>
				<td><code>&lt;all-in-one all-class='cl'&gt; ... &lt;/all-in-one&gt;</code></td>
				<td>Agrège toutes les pages référencées par les balises <code>a</code>
					qui possèdent la classe <code>cl</code> et qui se trouvent
					sous la balise <code>nav</code>. Par défaut, <code>all-class</code> vaut <code>all-in-one</code>.
					C'est cette balise qui permet de construire le fascicule contenant l'ensemble des sujets.
				</td>
			</tr>

			<tr>
				<td><code>&lt;generate-schedule&gt;&lt;/generate-schedule&gt;</code></td>
				<td>Génère le séquencement du cours</td>
			</tr>
      
		</table>
	</exercice>
</chapitre>

<!-- Local Variables: -->
<!-- mode: web -->
<!-- mode: flyspell -->
<!-- ispell-local-dictionary: "french" -->
<!-- coding: utf-8 -->
<!-- End: -->
