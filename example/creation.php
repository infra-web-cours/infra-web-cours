<chapitre title="Création d'un cours complet">
	<p>L'infrastructure repose sur un système d'aiguilleur. L'aiguilleur est le point d'entrée
    d'un cours et toute les requêtes webs passent par cet aiguilleur. En tant que concepteur
    de cours, il faut définir quelques variables dans cet aiguilleur puis l'aiguilleur
    délègue l'affichage à l'infrastructure.
    L'infrastructure, elle, s'occupe de la mise en page et d'afficher une page,
    appelée page courante passée en argument de l'aiguilleur.</p>

	<p>Typiquement, si on suppose que l'aiguilleur s'appelle <code>index.php</code>,
	  une requête s'écrit de la façon suivante&nbsp;:
	  <code>http://server/chemin/index.php?page=une-page</code>.
	  L'aiguilleur affichera alors le contenu de <code>une-page.php</code>
	  après le bandeau en haut. Notez qu'il ne faut pas mettre l'extension <code>.php</code>
    dans l'URL.</p>
  
	<p>Pour utiliser l'infrastructure, vous devez définir trois fichiers <code>php</code> au minimum&nbsp;:</p>
	<ul>
		<li>Un aiguilleur. Ce fichier contient donc la définition de quelques variables et délègue la gestion
			du cours à un fichier fourni par l'infrastructure de cours.</li>
		<li>Un fichier de navigation contenant le menu de navigation du module, agrémenté de quelques
			marqueurs pour générer le plan général du module ou l'ensemble des supports de TPs.</li>
		<li>Une page par défaut. Cette page est affichée par défaut si le paramètre
			<code>page</code> n'est pas renseigné. Lorsqu'on clique sur le titre du cours, on arrive aussi sur cette
			page.</li>
	</ul>
  
	<exercice title="L'aiguilleur">
		<p>L'aiguilleur se présente sous la forme suivante&nbsp;:</p>

		<code class='code-block'><?php highlight_file(dirname(__FILE__) . '/index.php'); ?></code>

		<p>Les principales variables sont&nbsp;:</p>
		<ul>
			<li><code>$root</code> contient le répertoire dans lequel se trouve l'aiguilleur.
				Il ne devrait jamais être nécessaire de modifier cette ligne.</li>
			<li><code>$web</code> indique où se trouve la partie web
				relativement à root. Dans notre cas, la partie web se trouve en '../infra-web-cours'.</li>
			<li><code>$module</code> donne le nom du module. Cette information est nécessaire pour créer le bandeau en haut
				de la page et pour générer le titre de la page.</li>
      <li><code>$planning</code> donne le planning de l'année en cours
        (voir l'aide <a href='?page=edt'>ici</a>).</li>
			<li>Finalement, la variable <code>$nav</code> indique le fichier de navigation du module
				sans l'extension '.php' (voir après) et la variable <code>$main</code> la page
				par défaut du module sans l'extension '.php' 
				(dans notre cas la page <code><?= $main ?></code>).</li>
		</ul>
	</exercice>

	<exercice title='Le fichier de navigation'>
		<p>Le fichier de navigation sert d'abord à générer les menus. Le fichier
      de navigation utilisé dans cette aide est donné ici&nbsp;:
		  <code class='code-block' prettify lang='html'><?php highlight_file(dirname(__FILE__) . '/nav.php'); ?></code></p>
    
		<p>Les menus et sous-menus sont donnés par les listes (<code>&lt;ul&gt;</code> et <code>&lt;li&gt;</code>).
		  Outre le fait qu'elle est affichée en tant que menu,
		  la liste <code>&lt;ul&gt;</code> avec la classe <code>chapitres</code> a aussi un rôle particulier.
		  Chaque élément de cette liste définit un chapitre (une séance).
		  Ces chapitres permettent de&nbsp;:</p>
		<ul>
			<li>Générer le page contenant tous les TPs an agrégeant 
        tous les contenus associés aux balises <code>&lt;a&gt;</code>
				ayant la classe <code>all-in-one</code> dans le fichier de navigation. Vous pouvez voir
        le résultat de cette agréagation <a href='?page=all'>ici</a>. Le
        code de cette page qui agrège l'ensemble des sujets est assez simple&nbsp;:
		    <code class='code-block' prettify lang='html'><?php highlight_file(dirname(__FILE__) . '/all.php'); ?></code></li>
			<li>Générer un tableau contenant le séquencement pédagogique du module (voir la suite).</li>
		</ul>
    
		<p>Par défaut, un chapitre est donc affiché à la fois en tant que sous-menu et dans la page de séquencement.
		  On peut changer ce comportement avec deux attributs qu'on peut adjoindre
		  à une balise &lt;li&gt;&nbsp;:</p>
		<ul>
			<li>L'attribut <code>ignore</code> indique que le chapitre <code>&lt;li&gt;</code> doit être totalement
				ignoré, comme dans&nbsp;:
				<code class='code-block' prettify lang='html'>&lt;li ignore&gt;Ma partie du cours que je ne veux pas rendre visible&lt;/li&gt;</code>
				Cet attribut permet de masquer certaines parties du cours si on souhaite ne les rendre
				visibles qu'au fur et à mesure de la progression du cours.</li>
			<li>L'attribut <code>only-schedule</code> permet d'indiquer que le chapitre doit
				apparaître dans la page schedule, mais pas dans les menus, ce qui permet d'alléger les menus,
				ou de traiter différemment des chapitres à faire en hors présentiel.</li>
      <li>L'attribut <code>only-nav</code> a le comportement inverse, le chapitre
        ne sera affiché que dans le menu.</li>
		</ul>
    
		<p>On peut aussi facilement gérer l'affichage de certains éléments en jouant avec les classes <code>current-week</code>
      et <code>min-week</code>. Le fichier
		  de navigation donné en exemple illustre le fonctionnement&nbsp;: seules
		  les balises avec une classe <code>min-week</code> avec un attribut <code>data-min-week</code> supérieur
      à 2 (c'est-à-dire supérieur à l'attribut <code>data-current-week</code> de la balise avec la classe <code>current-week</code>)
		  sont affichées.</p>

		<p>Pour illustrer, voici le séquencement pédagogique associé au fichier de navigation donné en exemple&nbsp;:</p>

		<?php include($root . '/schedule-example.php'); ?>

    <p>Le code permettant de générer ce séquencement est le suivant&nbsp;:</p>
		<code class='code-block' prettify lang='html'><?php highlight_file(dirname(__FILE__) . '/schedule-example.php'); ?></code></li>

		<p>Les balises <code>chapitre-*</code> servent à générer le séquencement et
		  les menus. Les informations données dans <code>chapitre-id</code> et <code>chapitre-title</code>
		  servent à générer le menu de premier niveau dans <code>Supports</code> et la
		  liste donnée dans la balise <code>chapitre-content</code> le menu de deuxième niveau.</p>

		<p>Vous avez enfin deux balises optionnelles qui vous sont proposées pour les parties séquencements&nbsp;:</p>
		<ul>
			<li>Si vous voulez un titre court dans le menu et un titre long dans le séquencement, il suffit
				d'ajouter une balise <code>chapter-short</code> comme dans l'exemple.</li>
			<li>Si vous voulez afficher des informations complémentaires dans le séquencement mais pas dans
				le menu de navigation, vous pouvez utiliser la balise <code>expanded-version</code> comme
				dans l'exemple.</li>
		</ul>
	</exercice>

	<exercice title="Les options de l'aiguilleur">
		<p>L'aiguilleur prend jusqu'à trois arguments&nbsp;:</p>
		<ul>
			<li><code>page=fichier</code>&nbsp;: indique le fichier qu'on souhaite afficher à l'intérieur
				de l'aiguilleur. Le chemin du fichier est relatif au
				répertoire racine (<code>$root</code>) et le suffixe <code>.php</code>
				est implicite. Si ce paramètre est omis, il prend la valeur <code>$main</code></li>
			<li><code>soluce=[true|false]</code>&nbsp;: indique si on souhaite afficher
				la solution. Si ce paramètre est omis, <code>soluce</code> vaut <code>false</code></li>
			<li><code>wrap=[true|false]</code>&nbsp;: indique si on souhaite afficher
				les solutions déjà déroulées ou si on souhaite n'afficher qu'un bouton permettant
				d'afficher les solutions. J'ai fait un chouette contre-sens quand j'ai conçu l'infrastructure,
        <code>wrap</code> à vrai indique qu'un menu est déroulé :).
        Pour une présentation d'une solution aux étudiants,
				on préférera laisser ce paramètre à <code>false</code>, alors que pour imprimer
				un corrigé, on préférera passer ce paramètre à <code>true</code>. Si ce paramètre
				est omis, il vaut <code>false</code> par défaut.</li>
			<li><code>preprint=[true|false]</code>&nbsp;: déroule automatiquement les balises
				<code>details</code> et <code>tip</code>, ce qui est utile pour l'impression d'un fascicule.
		</ul>
	
		<p>Pour une utilisation de tous les jours&nbsp;:</p>
		<ul>
			<li><code>&page=ma-page</code>&nbsp;: pour les étudiants quand ils surfent (pas de corrigé, détails/tip pas déroules),</li>
			<li><code>&page=ma-page&soluce=true</code>&nbsp;: pour les enseignants en version vidéo-projeté 
			  quand on veut pouvoir faire apparaître les détails, tips et solutions au fur et à mesure 
			  (bouton solution, solutions/details/tip pas déroulés),</li>
			<li><code>&page=ma-page&preprint=true</code>&nbsp;: fascicule de l'étudiant à imprimer
			  (pas de corrigé, détails/tip déroulés),</li>
			<li><code>&page=ma-page&preprint=true&soluce=true&wrap=true</code>&nbsp;: fascicule de l'enseignant à imprimer
			  (corrigés, détails et tips sont tous déroulés).</li>
		</ul>
	</exercice>
</chapitre>

<chapitre title="Arborescence du projet">
  <p>Ca chapitre décrit comme extraire certains répertoires du projet afin d'embarquer
    votre propre copie de <code>infra-web-cours</code> à l'intérieur de votre cours.
    Si vous reposez sur la version installée sur <code>www-inf.telecom-sudparis.eu/cours</code>
    ou si vous conservez l'arborescence du projet inchangée, vous n'avez pas besoin
    des explications qui suivent.</p>
  
  <exercice title="La variable wppath">
    <p>Le projet <code>infra-web-cours</code> est constitué de deux sous-parties.
      La première, appelée sous-partie wordpress, est utilisée à la fois par l'infrastructure
      pour les cours, mais aussi par
      le wordpress du département de façon à unifier la présentation des sites webs des équipes avec
      celles des pages de cours.
      La seconde, appelée sous-partie cours, ajoute des spécificités pour gérer les cours
      (typiquement la balise <code>&lt;schedule&gt;</code>).</p>

    <p>Si on suppose que le chemin vers la racine du projet <code>infra-web-cours</code>
      est défini dans la variable <code>INFRA_WEB_COURS</code>,
      alors <code>$INFRA_WEB_COURS/wp</code> contient la sous-partie wordpress
      et <code>$INFRA_WEB_COURS/infra-web-cours</code> contient la sous-partie cours.
      Comme la sous-partie cours repose sur la sous-partie wordpress, la sous-partie cours
      trouve le chemin vers la sous-partie wordpress dans la variable <code>wppath</code>.
      Par défaut, cette variable pointe vers la sous-partie wordpress se trouvant dans le projet,
      ce qui devrait correspondre à 99% des cas d'utilisations.
      Toutefois, si par hasard vous avez besoin de stocker la sous-partie wordpress
      dans un répertoire différent, il faut alors modifier <code>wppath</code>.
      Pour cela, il suffit de copier le fichier <code>config.sample.php</code> se trouvant
      dans le répartoire <code>$INFRA_WEB_COURS/infra-web-cours/current</code> en <code>config.php</code>
      dans le même répertoire et d'ajuster la variable <code>wppath</code> en fonction
      de votre abroescence.</p>
  </exercice>
</chapitre>

<!-- Local Variables: -->
<!-- mode: web -->
<!-- mode: flyspell -->
<!-- ispell-dictionary: "french" -->
<!-- coding: utf-8 -->
<!-- End: -->
