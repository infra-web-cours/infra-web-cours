
<?php	include($root . '/' . $web . '/planning-interface.php'); ?>

<chapitre title='Synthèse des heures enseignées'>
  <!-- l'argument de displaySynthesis permet de regrouper les différents cours suivant -->
  <!-- leur type (Cours, CI, CF etc...) -->
  <!-- la valeur devant la liste (3 pour cours ou 1.5 pour CI) permet d'indiquer le temps -->
  <!-- que dure une séance. Ca permet de savoir le nombre de créneaux qu'il faut valider dans AER -->
  <!-- Par exemple, pour les CI, en CSC3102, on a des déclarations par tranche de 1h30 comme ici -->
  <!-- alors que pour CSC3101 c'est par tranche de 3h -->
  

	<?php $schedule->displaySynthesis([ 
    'Cours' => [ 3, [ 'CM1' ] ],
    'CI' => [ 1.5, [ 'CI1', 'CI2', 'CI3', 'CI4', 'CI5', 'CI6', 'CI7', 'CI8', 'CI9' ] ],
    'CF' => [ 1, [ 'CF1', 'CF2' ] ]
  ]); ?>
</chapitre>

<!-- Local Variables: -->
<!-- mode: web -->
<!-- mode: flyspell -->
<!-- ispell-dictionary: "french" -->
<!-- coding: utf-8 -->
<!-- End: -->
