
<chapitre title='Présentation'>
  <p>Bienvenue dans l'aide de l'infrastructure web permettant de créer des cours&nbsp;!</p>

	<schedule>
		<li>
			<chapter-head></chapter-head>
			<chapter-head>Sujets</chapter-head>
			<chapter-head>Supports</chapter-head>
			<chapter-head>Notions clés</chapter-head>
		</li>
		<generate-schedule></generate-schedule>
	</schedule>
</chapitre>
<!-- Local Variables: -->
<!-- mode: web -->
<!-- mode: flyspell -->
<!-- ispell-dictionary: "french" -->
<!-- coding: utf-8 -->
<!-- End: -->
