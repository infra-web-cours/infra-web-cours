
<script>forceSoluce = true;</script> <!-- force l'affichage des solutions -->

<chapitre title='Code source'>
  <p>Voici le code de la page que vous observez&nbsp;:</p>
  <code class='code-block' prettify lang='html'><?php highlight_file(dirname(__FILE__) . '/new.php'); ?></code>
</chapitre>

<chapitre title='Tout chapitre doit avoir un titre'>
  <exercice title="Le titre d'un exercice est optionnel">
    <objectif>
      <ul>
        <li>Bien comprendre les balises de infra-web-cours</li>
      </ul>
    </objectif>

    <partie raw>Quelques remarques avant de commencer</partie>
  
    <p>Une partie permet de délimiter des zones dans un énoncé. Avec le raw, on ne numérote pas, comme 
      avec la balise du dessus.</p>
  
    <partie>Utilisation de l'environnement des figures</partie>

    <figenv>
      <p>Pour référencer une figure, il faut définir un figenv. Un figenv
        ne permet que de référencer une unique figure. Si on en veut plusieurs
        (ou référencer une figure de n'importe où dans le fichier), on ne peut
        aujourd'hui pas. Bref, ce n'est quand même pas aussi chouette que du latex :)
        Toujours est-il qu'on peut référencer la figure <figref></figref>
        qui se trouve dans le figenv.</p>

      <figure>
        <figcontent>
          <a target="_blank" href='<?= makeRelative(__FILE__, "gates.png"); ?>'>
            <img src='<?= makeRelative(__FILE__, "gates.png"); ?>' width='50%'></img>
          </a>
        </figcontent>
        <figcap><figref></figref>Ceci est un caption.</figcap>
      </figure>

      <p>On peut bien sûr référencer aussi la figure <figref></figref> après la figure.</p>
    </figenv>

    <remarque>Pour faire une remarque, on utilise la balise remarque</remarque>
    
    <attention>Parfois, il faut vraiment alerter les élèves, dans ce cas, on a une balise attention</attention>

    <partie>Dans un exercice, on peut même poser des questions aux étudiants :)</partie>

    <question title='Ceci est une question (titre optionnel)'>
      <p>Quand on veut faire du code en ligne, on utilise la balise
        <code>code</code>. Par défaut, le code en ligne n'est pas coloré.
        On peut changer ce comportement avec le paramètre prettify utilisée conjointement avec
        le paramètre lang qui permet de spécifier le langage de programmation,
        comme ici&nbsp;:</p>
      <code prettify lang='java'>int x = 42; /* ceci est du code Java */</code>.
      
      <p>On peut aussi définir du code sous la forme de blocs, en ajoutant l'option class=code-block.
        De la même façon, le code n'est pas coloré, mais on peut changer ce comportement
        avec prettify et lang.</p>
      <code class='code-block' prettify lang='scheme'>/* le c++, c'est chouette ! */
template &lt;typename T&gt;
int class::code_en_cpp(T x, int y) {
  return x &lt;&lt; y;
}</code>

      <remarque>Dans du code en ligne, les &lt; et &gt; sont interprétés par html,
        il faut donc mettre &amp;lt; et &amp;gt;. Idem pour les &amp;. Si on veut
        les afficher, il faut mettre &amp;amp;</remarque>

      <p>Enfin, si on utilise l'option url de code, on inclut un fichier externe. Dans
        ce cas, la syntaxe est automatiquement
        colorée, les &amp;, &lt; et &gt; sont automatiquement transformés, et le langage est automatiquement identifié
        (on peut empêcher la coloration avec l'attribut <code>noprettify</code> et imposer un autre langage
        avec l'attribut <code>lang</code>).</p>
        <code url='sample.c'></code></p>

      <solution>
        Et oui, ceci est une solution :)
      </solution>
    </question>
    
    <bravo>Féclitations, vous savez utiliser la balise bravo :)</bravo>
  </exercice>
</chapitre>

<chapitre title="On cache toutes les solutions à partir d'ici">
  <no-soluce></no-soluce>
  <p>Aucune des solutions atteignables à partir du <code>chapitre</code> englobant ne sont affichées car on utilise
    la balise <code>no-soluce</code>. Vous pouvez la placer à la racine d'un document pour cacher toutes les solutions.</p>
  
  <exercice title='Exercice avec solution cachée'>
    <solution>Une solution jamais envoyée au client.</solution>
  </exercice>
</chapitre>

<chapitre title='Cacher les solutions à grain fin'>
  <exercice title='Exercice avec solution cachée' no-soluce>
    <solution>Jamais envoyé au client.</solution>
  </exercice>
  <exercice title='Exercice avec solution envoyée'>
    <solution>Oh, la belle solution&nbsp;!</solution>
  </exercice>
</chapitre>

<!-- Local Variables: -->
<!-- mode: web -->
<!-- indent-tabs-mode: nil -->
<!-- mode: flyspell -->
<!-- ispell-local-dictionary: "french" -->
<!-- coding: utf-8 -->
<!-- End: -->
