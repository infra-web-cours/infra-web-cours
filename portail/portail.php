
<article>
	<p>Cette page recense des modules coordonnés ou faisant partie de voies d'appronfondissement
	coordonnées par des membres du
	<a href='https://www.inf.telecom-sudparis.eu/'>département d'informatique</a>
	de <a href='https://www.telecom-sudparis.eu/'>Télécom SudParis</a>.
	Les modules indiqués en bleu sont des modules
	pour lesquels un lien permet d'accéder en ligne aux ressources pédagogiques indépendemment
	de la plateforme <a href='https://moodle.ip-paris.fr/'>moodle</a>.
	Pour la plupart des autres modules, les étudiants peuvent se référer aux ressources pédagogiques
	mises à disposition sur la plateforme <a href='https://moodle.ip-paris.fr/'>moodle</a>.</p>

	<p>Pour une liste exhaustive des modules CSC et plus généralement des modules
	dispensés à Télécom SudParis, vous pouvez aussi vous référer à la page
	<a href='https://enseignements.telecom-sudparis.eu/'>suivante</a>
	permettant d'accéder à l'ensemble des fiches programmes.</p>

	<chapitre title="Voies d'appronfondissement et masters">
    <p>Le <a href='https://www.inf.telecom-sudparis.eu/'>département d'informatique</a>
		propose quatre VAPs ou voies d'appronfondissement (fin du BAC+4 et BAC+5) 
		à <a href='https://www.telecom-sudparis.eu/'>Télécom SudParis</a>&nbsp;:</p>
		<ul>
			<li><a href='https://asr.telecom-sudparis.eu/'>ASR</a>&nbsp;: Architectes de Services 
        informatiques Répartis (<a href="https://www-inf.telecom-sudparis.eu/COURS/ASR/">site interne</a>)</li>
			<li><a href='https://vapdsi.telecom-sudparis.eu/'>DSI</a>&nbsp;: Intégration et Déploiement de Systèmes d'Information</li>
			<li><a href='https://jin.telecom-sudparis.eu/'>JIN</a>&nbsp;: Jeux vidéo / Interactions et collaborations Numériques</li>
			<li><a href='<?= $vapia_url ?>'>IA</a>&nbsp;: Intelligence Artificielle</li>
                        <li><a href="https://www-inf.telecom-sudparis.eu/COURS/maia/Supports">MAIA</a>&nbsp;: Modèles et Applications de l'Intelligence Artificielle (apprentissage)</li>
		</ul>

    <p>Le <a href='https://www.inf.telecom-sudparis.eu/'>département d'informatique</a>
		est aussi en charge de l'organisation pédagogique d'un parcours 
		  du <a href='https://www.ip-paris.fr/en/master/computer-science/'>master d'informatique</a>
      de <a href='https://www.ip-paris.fr/en'>IP Paris</a>&nbsp;:</p>
		<ul>
			<li><a href='<?= $pds_url ?>'>PDS</a>&nbsp;: Parallel
        and Distributed Systems</li>
		</ul>
	</chapitre>

	<chapitre title="Les modules coordonnés par le département d'informatique">
	<schedule>
		<li>
			<chapter-head>Institut</chapter-head>
			<chapter-head>Identifiant</chapter-head>
			<chapter-head>Année</chapter-head>
			<chapter-head>Nom</chapter-head>
			<chapter-head>Fiche programme</chapter-head>
		</li>
		<generate-schedule></generate-schedule>
	</schedule>
	</chapitre>
</article>

<!-- Local Variables: -->
<!-- mode: web -->
<!-- mode: flyspell -->
<!-- ispell-dictionary: "french" -->
<!-- coding: utf-8 -->
<!-- End: -->
