<?php
  $curtheme = 'new';

	$root = dirname(__FILE__);       /* root directory of the module */
	$web = '../infra-web-cours/'; /* relative path of the web infrastructure (technically, web files are in $root . '/' $web) */

	$myCss = 'style.css';

	$module = "Portail des enseignements du département d'informatique";  /* name of the module */
	$year = 'Année 2017 &ndash; 2018';                                      /* current year */

	$nav = 'nav';                /* page of the nav file (technically, nav is in $root . '/' . $nav . '.php') */
	$main = 'portail';           /* page of the main file (technically, main is in $root . '/' . $main . '.php') */

  $portail = 'Département informatique';
  $urlportail = 'https://www.inf.telecom-sudparis.eu/';

	include($root . '/' . $web . '/tsp.php');
?>