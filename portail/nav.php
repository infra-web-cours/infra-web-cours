
<?php
$tsp = "<a target='_blank' href='https://www.telecom-sudparis.eu/'>Telecom SudParis</a>";
$chps = "<a target='_blank' href='https://chps.uvsq.fr/'>Master CHPS</a>";

foreach(array("vapia" => array("VAP IA", "https://www-inf.telecom-sudparis.eu/COURS/vap-ia/"),
              "maia" => array("VAP MAIA", "https://www-inf.telecom-sudparis.eu/COURS/maia/Supports/"),
              "pds" => array("Master PDS", "https://cs.ip-paris.fr/courses/tracks/pds/"))
                as $k => $v) {
  ${$k . "_url"} = $v[1];
  ${$k} = "<a target='_blank' href='" . $v[1] . "''>" . $v[0] . "</a>";
}

$csn1 = "<a target='_blank' href='https://www.ip-paris.fr/master-1-computer-science-for-networks/'>Master CSN</a>";
$csn2 = "<a target='_blank' href='https://www.ip-paris.fr/master-2-computer-science-for-networks/'>Master CSN</a>";
$dataia = "<a target='_blank' href='https://www.ip-paris.fr/master-dataai/'>Master DataIA</a>";
$igd = "<a target='_blank' href='https://www.ip-paris.fr/master-interaction-graphics-design/'>Master IGD</a>";

$ccn = "<a target='_blank' href='https://www.universite-paris-saclay.fr/fr/formation/master/m2-informatique-pour-les-reseaux-de-communication-computer-science-for#presentation-m2'>Master CCN</a>";
$comnets = "<a target='_blank' href='https://www.telecom-sudparis.eu/en-tmp/formation/communication-networks-services/'>Master COMNETS</a>";
$cils = "<a target='_blank' href='https://www.universite-paris-saclay.fr/fr/formation/master/m2-conception-et-intelligence-des-logiciels-et-systemes-cils#presentation-m2'>Master CILS</a>";
$datascale = "<a target='_blank' href='https://www.universite-paris-saclay.fr/fr/education/master/m2-gestion-de-donnees-dans-un-monde-numerique-data-management-in-a-digital-world#presentation-m2'>Master DataScale</a>";

?>

<ul>
<!--	<?php if(!isset($newtheme) || !$newtheme) echo '<li>Arbre des enseignements<ul>'; ?> -->
	<li>
		Telecom SudParis
		<ul>
			<li>
				Première année
				<ul class='chapitres'>
					<li data-has-link>
						<chapter-id><?= $tsp ?></chapter-id>
						<chapter-title><a href='https://www-inf.telecom-sudparis.eu/COURS/CSC3101/Supports/fise/'>CSC3101</a></chapter-title>
						<span class='niveau'>1A</span>
						<chapter-content>Algorithmique et langage de programmation</chapter-content>
						<chapter-notion>
							<a href="https://enseignements.telecom-sudparis.eu/fiche.php?c=CSC3101">Fiche programme</a>
						</chapter-notion>
					</li>

					<li data-has-link>
						<chapter-id><?= $tsp ?></chapter-id>
						<chapter-title><a href='https://www-inf.telecom-sudparis.eu/COURS/CSC3102/Supports/'>CSC3102</a></chapter-title>
						<span class='niveau'>1A</span>
						<chapter-content>Introduction aux systèmes d’exploitation</chapter-content>
						<chapter-notion>
              <a href="https://enseignements.telecom-sudparis.eu/fiche.php?c=CSC3102">Fiche programme</a>
						</chapter-notion>
					</li>
							
					<li data-has-link>
						<chapter-id><?= $tsp ?></chapter-id>
						<chapter-title><a href='https://www-inf.telecom-sudparis.eu/cours/bd'>CSC3601</a></chapter-title>
						<span class='niveau'>1A</span>
						<chapter-content>Modélisation, bases de données et systèmes d'information</chapter-content>
						<chapter-notion>
							<a href="https://enseignements.telecom-sudparis.eu/fiche.php?c=CSC3601">Fiche programme</a>
						</chapter-notion>
					</li>

					<li>
						<chapter-id><?= $tsp ?></chapter-id>
						<chapter-title><a href='https://enseignements.telecom-sudparis.eu/fiche.php?m=21069&l=fr'>PRO3600</a></chapter-title>
						<span class='niveau'>1A</span>
						<chapter-content>Développement informatique</chapter-content>
						<chapter-notion>
							<a href="https://enseignements.telecom-sudparis.eu/fiche.php?c=PRO3600">Fiche programme</a>
						</chapter-notion>
					</li>
				</ul>
			</li>

			<li>
				Deuxième année
				<ul>
					<li>
						Tronc commun
						<ul class='chapitres'>
							<li data-has-link>
								<chapter-id><?= $tsp ?></chapter-id>
								<chapter-title><a href='https://www-inf.telecom-sudparis.eu/COURS/CSC4101/'>CSC4101</a></chapter-title>
								<span class='niveau'>2A</span>
								<chapter-content>Architectures et applications web</chapter-content>
								<chapter-notion>
									<a href="https://enseignements.telecom-sudparis.eu/fiche.php?c=CSC4101">Fiche programme</a>
								</chapter-notion>
							</li>
					    <li data-has-link>
						    <chapter-id><?= $tsp ?></chapter-id>
						    <chapter-title><a href='https://www-inf.telecom-sudparis.eu/COURS/cen'>ENV4101</a></chapter-title>
						    <span class='niveau'>2A</span>
						    <chapter-content>Consommation énergétique du numérique</chapter-content>
						    <chapter-notion>
							    <a href="https://enseignements.telecom-sudparis.eu/fiche.php?m=22987">Fiche programme</a>
						    </chapter-notion>
					    </li>
            </ul>
          </li>
					<li>
						Domaine Informatique
						<ul class='chapitres'>
							<li data-has-link>
								<chapter-id><?= $tsp ?></chapter-id>
								<chapter-title><a href='https://www-inf.telecom-sudparis.eu/COURS/CSC4102/'>CSC4102</a></chapter-title>
								<span class='niveau'>2A</span>
								<chapter-content>Introduction au Génie Logiciel pour applications Orientées Objet</chapter-content>
								<chapter-notion>
									<a href="https://enseignements.telecom-sudparis.eu/fiche.php?c=CSC4102">Fiche programme</a>
								</chapter-notion>
							</li>
							
							<li data-has-link>
								<chapter-id><?= $tsp ?></chapter-id>
								<chapter-title><a href='https://www-inf.telecom-sudparis.eu/COURS/CSC4103/Supports/'>CSC4103</a></chapter-title>
								<span class='niveau'>2A</span>
								<chapter-content>Programmation système</chapter-content>
								<chapter-notion>
                  <a href="https://enseignements.telecom-sudparis.eu/fiche.php?c=CSC4103">Fiche programme</a>
								</chapter-notion>
							</li>
						</ul>
					</li>

					<li>
						Modules d'ouverture
						<ul class='chapitres'>
							<li data-has-link>
								<chapter-id><?= $tsp ?></chapter-id>
								<chapter-title><a href='https://www-public.tem-tsp.eu/~gibson/Teaching/CSC4504/'>CSC4504</a></chapter-title>
								<span class='niveau'>2A</span>
								<chapter-content>Langages formels et applications</chapter-content>
								<chapter-notion>
									<a href="https://enseignements.telecom-sudparis.eu/fiche.php?c=CSC4504">Fiche programme</a>
								</chapter-notion>
							</li>
							<li data-has-link>
								<chapter-id><?= $tsp ?></chapter-id>
								<chapter-title><a href='https://www-inf.telecom-sudparis.eu/COURS/CSC4251_4252/'>CSC4251_4252</a></chapter-title>
								<span class='niveau'>2A</span>
								<chapter-content>Compilation&nbsp;: du langage de haut niveau à l'assembleur</chapter-content>
								<chapter-notion>
									<a href="https://enseignements.telecom-sudparis.eu/fiche.php?c=CSC4251">Fiche programme</a>
								</chapter-notion>
							</li>
						</ul>
					</li>
        </ul>
      </li>

      <li>
				VAP ASR
				<ul class='chapitres'>
          <li data-has-link only-nav>
						<a href='https://asr.telecom-sudparis.eu/'>Site externe</a>
          </li>

          <li data-has-link only-nav>
						<a href='https://www-inf.telecom-sudparis.eu/COURS/ASR/'>Site interne</a>
          </li>

					<li data-has-link>
						<chapter-id><?= $tsp ?></chapter-id>
						<chapter-title><a href='https://www-inf.telecom-sudparis.eu/COURS/CSC4508/Supports/'>CSC4508</a></chapter-title>
						<span class='niveau'>2A</span>
						<chapter-content>Systèmes d'exploitation</chapter-content>
						<chapter-notion>
							<a href="https://enseignements.telecom-sudparis.eu/fiche.php?c=CSC4508">Fiche programme</a>
						</chapter-notion>
					</li>

					<li data-has-link>
						<chapter-id><?= $tsp ?></chapter-id>
						<chapter-title><a href='https://www-inf.telecom-sudparis.eu/COURS/CSC4509/'>CSC4509</a></chapter-title>
						<span class='niveau'>2A</span>
						<chapter-content>Algorithmique et communications des applications réparties</chapter-content>
						<chapter-notion>
							<a href="https://enseignements.telecom-sudparis.eu/fiche.php?c=CSC4509">Fiche programme</a>
						</chapter-notion>
					</li>

					<li data-has-link>
						<chapter-id><?= $tsp ?></chapter-id>
						<chapter-title><a href='https://www-inf.telecom-sudparis.eu/COURS/CSC5001/Supports/'>CSC5001</a></chapter-title>
						<span class='niveau'>3A</span>
						<chapter-content>Systèmes Hautes Performances</chapter-content>
						<chapter-notion>
              <a href="https://enseignements.telecom-sudparis.eu/fiche.php?m=2272">Fiche programme</a>
						</chapter-notion>
					</li>
					
					<li data-has-link>
						<chapter-id><?= $tsp ?></chapter-id>
						<chapter-title><a href='https://www-inf.telecom-sudparis.eu/COURS/CSC5002/'>CSC5002</a></chapter-title>
						<span class='niveau'>3A</span>
						<chapter-content>Middleware pour applications réparties</chapter-content>
						<chapter-notion>
							<a href="https://enseignements.telecom-sudparis.eu/fiche.php?m=2276">Fiche programme</a>
						</chapter-notion>
					</li>

					<li>
						<chapter-id><?= $tsp ?></chapter-id>
						<chapter-title><a href='https://www-inf.telecom-sudparis.eu/COURS/CSC5003/Supports/'>CSC5003</a></chapter-title>
						<span class='niveau'>3A</span>
						<chapter-content>Web sémantique et infrastructures pour le big data</chapter-content>
						<chapter-notion>
							<a href="https://enseignements.telecom-sudparis.eu/fiche.php?c=CSC5003">Fiche programme</a>
						</chapter-notion>
					</li>
									
					<li data-has-link>
						<chapter-id><?= $tsp ?></chapter-id>
						<chapter-title><a href='https://github.com/otrack/cloud-computing-infrastructures'>CSC5004</a></chapter-title>
						<span class='niveau'>3A</span>
						<chapter-content>Infrastructures pour le Cloud</chapter-content>
						<chapter-notion>
							<a href="https://enseignements.telecom-sudparis.eu/fiche.php?m=2289">Fiche programme</a>
						</chapter-notion>
					</li>

					<li data-has-link>
						<chapter-id><?= $tsp ?></chapter-id>
						<chapter-title><a href='https://www-inf.telecom-sudparis.eu/COURS/ASR/'>CSC5005</a></chapter-title>
						<span class='niveau'>3A</span>
						<chapter-content>Projet ASR</chapter-content>
						<chapter-notion>
							<a href="https://enseignements.telecom-sudparis.eu/fiche.php?c=CSC5005">Fiche programme</a>
						</chapter-notion>
					</li>
				</ul>
			</li> <!-- ASR -->

			<li>
				VAP DSI
				<ul class='chapitres'>
          <li data-has-link only-nav>
						<a href='https://vapdsi.telecom-sudparis.eu/'>Site externe</a>
          </li>

					<li>
						<chapter-id><?= $tsp ?></chapter-id>
						<chapter-title><a href='https://enseignements.telecom-sudparis.eu/fiche.php?m=5772&l=fr'>CSC4521</a></chapter-title>
						<span class='niveau'>2A</span>
						<chapter-content>Génie Logiciel pour la conception d’un SI</chapter-content>
						<chapter-notion>
							<a href="https://enseignements.telecom-sudparis.eu/fiche.php?c=CSC4521">Fiche programme</a>
						</chapter-notion>
					</li>
          
					<li>
						<chapter-id><?= $tsp ?></chapter-id>
						<chapter-title><a href='https://enseignements.telecom-sudparis.eu/fiche.php?m=5773&l=fr'>CSC4522</a></chapter-title>
						<span class='niveau'>2A</span>
						<chapter-content>Projet SI : Réalisation et déploiement</chapter-content>
						<chapter-notion>
							<a href="https://enseignements.telecom-sudparis.eu/fiche.php?c=CSC4522">Fiche programme</a>
						</chapter-notion>
					</li>

					<li>
						<chapter-id><?= $tsp ?></chapter-id>
						<chapter-title><a href='https://enseignements.telecom-sudparis.eu/fiche.php?c=CSC8601'>CSC8601</a></chapter-title>
						<span class='niveau'>3A</span>
						<chapter-content>Architecture technique&nbsp;:&nbsp;Serveurs d'applications, gestion de données et Big Data</chapter-content>
						<chapter-notion>
							<a href="https://enseignements.telecom-sudparis.eu/fiche.php?c=CSC8601">Fiche programme</a>
						</chapter-notion>
					</li>

					<li>
						<chapter-id><?= $tsp ?></chapter-id>
						<chapter-title><a href='https://enseignements.telecom-sudparis.eu/fiche.php?c=CSC8602'>CSC8602</a></chapter-title>
						<span class='niveau'>3A</span>
						<chapter-content>Urbanisation et stratégie des SI</chapter-content>
						<chapter-notion>
							<a href="https://enseignements.telecom-sudparis.eu/fiche.php?c=CSC8602">Fiche programme</a>
						</chapter-notion>
					</li>
					
					<li data-has-link>
						<chapter-id><?= $tsp ?></chapter-id>
						<chapter-title><a href='https://www-inf.telecom-sudparis.eu/SIMBAD/courses/doku.php?id=labs'>CSC8603</a></chapter-title>
						<span class='niveau'>3A</span>
						<chapter-content>Architectures logicielles : services, microservices et BPM</chapter-content>
						<chapter-notion>
							<a href="https://enseignements.telecom-sudparis.eu/fiche.php?c=CSC8603">Fiche programme</a>
						</chapter-notion>
					</li>
          
					<li>
						<chapter-id><?= $tsp ?></chapter-id>
						<chapter-title><a href='https://enseignements.telecom-sudparis.eu/fiche.php?c=CSC8604'>CSC8604</a></chapter-title>
						<span class='niveau'>3A</span>
						<chapter-content>Cloud computing : Architecture et déploiement d'un SI</chapter-content>
						<chapter-notion>
							<a href="https://enseignements.telecom-sudparis.eu/fiche.php?c=CSC8604">Fiche programme</a>
						</chapter-notion>
					</li>

					<li>
						<chapter-id><?= $tsp ?></chapter-id>
						<chapter-title><a href='https://enseignements.telecom-sudparis.eu/fiche.php?c=PRO8601'>PRO8601</a></chapter-title>
						<span class='niveau'>3A</span>
						<chapter-content>Projet DSI</chapter-content>
						<chapter-notion>
							<a href="https://enseignements.telecom-sudparis.eu/fiche.php?c=PRO8601">Fiche programme</a>
						</chapter-notion>
					</li>
				</ul>
			</li> <!-- DSI -->

			<li>
				VAP JIN
				<ul class='chapitres'>
          <li data-has-link only-nav>
						<a href='https://jin.telecom-sudparis.eu/'>Site externe</a>
          </li>

					<li data-has-link>
						<chapter-id><?= $tsp ?></chapter-id>
						<chapter-title><a href='https://www-inf.telecom-sudparis.eu/COURS/CSC4508/Supports/'>CSC4508</a></chapter-title>
						<span class='niveau'>2A</span>
						<chapter-content>Systèmes d'exploitation</chapter-content>
						<chapter-notion>
							<a href="https://enseignements.telecom-sudparis.eu/fiche.php?c=CSC4508">Fiche programme</a>
						</chapter-notion>
					</li>

					<li data-has-link>
						<chapter-id><?= $tsp ?></chapter-id>
						<chapter-title><a href='https://www-inf.telecom-sudparis.eu/COURS/CSC4526/new_site/Supports/'>CSC4526</a></chapter-title>
						<span class='niveau'>2A</span>
						<chapter-content>Développement C++</chapter-content>
						<chapter-notion>
							<a href="https://enseignements.telecom-sudparis.eu/fiche.php?c=CSC4526">Fiche programme</a>
						</chapter-notion>
					</li>

					<li>
						<chapter-id><?= $tsp ?></chapter-id>
						<chapter-title><a href='https://enseignements.telecom-sudparis.eu/fiche.php?m=17182&l=fr'>CSC5061</a></chapter-title>
						<span class='niveau'>3A</span>
						<chapter-content>Systèmes interagissant en réseau</chapter-content>
						<chapter-notion>
							<a href="https://enseignements.telecom-sudparis.eu/fiche.php?c=CSC5061">Fiche programme</a>
						</chapter-notion>
					</li>
          
					<li>
						<chapter-id><?= $tsp ?></chapter-id>
						<chapter-title><a href='https://enseignements.telecom-sudparis.eu/fiche.php?m=17212&lg=fr'>CSC5062</a></chapter-title>
						<span class='niveau'>3A</span>
						<chapter-content>Interaction et capteurs</chapter-content>
						<chapter-notion>
							<a href="https://enseignements.telecom-sudparis.eu/fiche.php?c=CSC5062">Fiche programme</a>
						</chapter-notion>
					</li>

					<li>
						<chapter-id><?= $tsp ?></chapter-id>
						<chapter-title><a href='https://enseignements.telecom-sudparis.eu/fiche.php?m=17217&l=fr'>DIV5061</a></chapter-title>
						<span class='niveau'>3A</span>
						<chapter-content>Sciences humaines et sociales et design</chapter-content>
						<chapter-notion>
							<a href="https://enseignements.telecom-sudparis.eu/fiche.php?c=DIV5061">Fiche programme</a>
						</chapter-notion>
					</li>

					<li>
						<chapter-id><?= $tsp ?></chapter-id>
						<chapter-title><a href='https://enseignements.telecom-sudparis.eu/fiche.php?m=17214&l=fr'>IMA5061</a></chapter-title>
						<span class='niveau'>3A</span>
						<chapter-content>Modélisation et rendu</chapter-content>
						<chapter-notion>
							<a href="https://enseignements.telecom-sudparis.eu/fiche.php?c=IMA5061">Fiche programme</a>
						</chapter-notion>
					</li>
          
					<li>
						<chapter-id><?= $tsp ?></chapter-id>
						<chapter-title><a href='https://enseignements.telecom-sudparis.eu/fiche.php?m=17215&l=fr'>MGT5061</a></chapter-title>
						<span class='niveau'>3A</span>
						<chapter-content>Gestion de projet avancée</chapter-content>
						<chapter-notion>
							<a href="https://enseignements.telecom-sudparis.eu/fiche.php?c=MGT5061">Fiche programme</a>
						</chapter-notion>
					</li>

					<li>
						<chapter-id><?= $tsp ?></chapter-id>
						<chapter-title><a href='https://enseignements.telecom-sudparis.eu/fiche.php?m=17216&l=fr'>MGT5062</a></chapter-title>
						<span class='niveau'>3A</span>
						<chapter-content>Jeu d'entreprises et management de l'innovation et des technologies</chapter-content>
						<chapter-notion>
							<a href="https://enseignements.telecom-sudparis.eu/fiche.php?c=MGT5062">Fiche programme</a>
						</chapter-notion>
					</li>
          
					<li>
						<chapter-id><?= $tsp ?></chapter-id>
						<chapter-title><a href='https://enseignements.telecom-sudparis.eu/fiche.php?m=17213&l=fr'>CSC5065</a></chapter-title>
						<span class='niveau'>3A</span>
						<chapter-content>Projet JIN</chapter-content>
						<chapter-notion>
							<a href="https://enseignements.telecom-sudparis.eu/fiche.php?c=CSC5065">Fiche programme</a>
						</chapter-notion>
					</li>
				</ul>
			</li> <!-- DSI -->

      <li>
				VAP IA
				<ul class='chapitres'>
          <li data-has-link only-nav>
						<a href='<?= $vapia_url ?>'>Site externe</a>
          </li>
        </ul>
      </li> <!-- IA -->
    </ul>
  </li> <!-- TSP -->
  

  <li>
    IP Paris
    <ul>

      <li>
				Master CSN
        <ul>
          <li>
            Master 1
            <ul class='chapitres'>
				      <li>
					      <chapter-id><?= $csn1 ?></chapter-id>
					      <chapter-title><a href='https://enseignements.telecom-sudparis.eu/fiche.php?c=CSC7427'>CSC7427</a></chapter-title>
					      <span class='niveau'>M1</span>
					      <chapter-content>Principles of relational data management</chapter-content>
					      <chapter-notion>
						      <a href="https://enseignements.telecom-sudparis.eu/fiche.php?c=CSC7427">Fiche programme</a>
					      </chapter-notion>
				      </li>
				      <li>
					      <chapter-id><?= $csn1 ?></chapter-id>
					      <chapter-title><a href='https://enseignements.telecom-sudparis.eu/fiche.php?c=CSC7430'>CSC7430</a></chapter-title>
					      <span class='niveau'>M1</span>
					      <chapter-content>Principles of distributed data management</chapter-content>
					      <chapter-notion>
						      <a href="https://enseignements.telecom-sudparis.eu/fiche.php?c=CSC7430">Fiche programme</a>
					      </chapter-notion>
				      </li>
            </ul>
          </li>
          <li>
            Master 2
            <ul class='chapitres'>
				      <li data-has-link>
					      <chapter-id><?= $csn2 ?></chapter-id>
					      <chapter-title><a href='https://www-inf.telecom-sudparis.eu/COURS/CSC7321/'>CSC7321</a></chapter-title>
					      <span class='niveau'>M2</span>
					      <chapter-content>Middleware for Distributed Applications</chapter-content>
					      <chapter-notion>
						      <a href="https://enseignements.telecom-sudparis.eu/fiche.php?m=19540">Fiche programme</a>
					      </chapter-notion>
				      </li>
            </ul>
          </li>
        </ul>
      </li>

      <li>
				Master Data AI
        <ul class='chapitres'>
          <li>
					  <chapter-id><?= $dataia ?></chapter-id>
					  <chapter-title><a href='https://enseignements.telecom-sudparis.eu/fiche.php?m=2280&l=fr'>CSC5003</a></chapter-title>
					  <span class='niveau'>Master</span>
					  <chapter-content>Big Data Systems</chapter-content>
					  <chapter-notion>
						  <a href="https://enseignements.telecom-sudparis.eu/fiche.php?c=CSC5003">Fiche programme</a>
					  </chapter-notion>
          </li>
        </ul>
      </li>

      <li>
				VAP MAIA
        <ul class='chapitres'>
		<li data-has-link only-nav>
			<a href='<?= $maia_url ?>'>Site du parcours</a>
		</li>
		<li>
			<chapter-id><?= $maia ?></chapter-id>
			<chapter-title><a href='https://www-inf.telecom-sudparis.eu/COURS/CSC4538/Supports'>CSC4538</a></chapter-title>
			<span class='niveau'>2A</span>
			<chapter-content>Introduction à la science des données</chapter-content>
			<chapter-notion>
				<a href="https://enseignements.telecom-sudparis.eu/fiche.php?c=CSC4538">Fiche programme</a>
			</chapter-notion>
		</li>
		<li data-has-link>
			<chapter-id><?= $maia ?></chapter-id>
			<chapter-title><a href='https://www-inf.telecom-sudparis.eu/COURS/CSC8609/Supports/'>CSC8609</a></chapter-title>
			<span class='niveau'>3A</span>
			<chapter-content>Introduction au Machine Learning</chapter-content>
			<chapter-notion>
				<a href="https://enseignements.telecom-sudparis.eu/fiche.php?c=CSC8609">Fiche programme</a>
			</chapter-notion>
		</li>
		<li data-has-link>
			<chapter-id><?= $maia ?></chapter-id>
			<chapter-title><a href='https://www-inf.telecom-sudparis.eu/COURS/CSC8610/Supports/'>CSC8610</a></chapter-title>
			<span class='niveau'>3A</span>
			<chapter-content>Introduction au traitement d'image</chapter-content>
			<chapter-notion>
				<a href="https://enseignements.telecom-sudparis.eu/fiche.php?c=CSC8610">Fiche programme</a>
			</chapter-notion>
		</li>

        </ul>
      </li>

      <li>
				Master IGD
        <ul class='chapitres'>
				 	<li data-has-link>
				 		<chapter-id><?= $igd ?></chapter-id>
				 		<chapter-title><a href=''>CSC5061</a></chapter-title>
				 		<span class='niveau'>Master</span>
				 		<chapter-content>Multiplayer Online Games Development</chapter-content>
				 		<chapter-notion>
				 			<a href=''>Fiche programme</a>
				 		</chapter-notion>
          </li>
        </ul>
      </li>
      
      <li>
				Master PDS
        <ul class='chapitres'>
          <li data-has-link only-nav>
						<a href='<?= $pds_url ?>'>Site du parcours</a>
          </li>
					<li data-has-link>
						<chapter-id><?= $pds ?></chapter-id>
						<chapter-title><a href='https://www-inf.telecom-sudparis.eu/COURS/CSC4508/Supports/'>CSC4508</a></chapter-title>
						<span class='niveau'>Master</span>
						<chapter-content>Systèmes d'exploitation</chapter-content>
						<chapter-notion>
							<a href="https://enseignements.telecom-sudparis.eu/fiche.php?c=CSC4508">Fiche programme</a>
						</chapter-notion>
					</li>

					<li data-has-link>
						<chapter-id><?= $pds ?></chapter-id>
						<chapter-title><a href='https://www-inf.telecom-sudparis.eu/COURS/CSC5001/Supports/'>CSC5001</a></chapter-title>
						<span class='niveau'>Master</span>
						<chapter-content>Systèmes Hautes Performances</chapter-content>
						<chapter-notion>
              <a href="https://enseignements.telecom-sudparis.eu/fiche.php?m=2272">Fiche programme</a>
						</chapter-notion>
					</li>
					
					<li data-has-link>
						<chapter-id><?= $pds ?></chapter-id>
						<chapter-title><a href='https://www-inf.telecom-sudparis.eu/COURS/CSC5002/'>CSC5002</a></chapter-title>
						<span class='niveau'>Master</span>
						<chapter-content>Middleware pour applications réparties</chapter-content>
						<chapter-notion>
							<a href="https://enseignements.telecom-sudparis.eu/fiche.php?m=2276">Fiche programme</a>
						</chapter-notion>
					</li>

					<li>
						<chapter-id><?= $pds ?></chapter-id>
						<chapter-title><a href='https://enseignements.telecom-sudparis.eu/fiche.php?m=2280&l=fr'>CSC5003</a></chapter-title>
						<span class='niveau'>Master</span>
						<chapter-content>Web sémantique et infrastructures pour le big data</chapter-content>
						<chapter-notion>
							<a href="https://enseignements.telecom-sudparis.eu/fiche.php?c=CSC5003">Fiche programme</a>
						</chapter-notion>
					</li>
									
					<li data-has-link>
						<chapter-id><?= $pds ?></chapter-id>
						<chapter-title><a href='https://github.com/otrack/cloud-computing-infrastructures'>CSC5004</a></chapter-title>
						<span class='niveau'>Master</span>
						<chapter-content>Infrastructures pour le Cloud</chapter-content>
						<chapter-notion>
							<a href="https://enseignements.telecom-sudparis.eu/fiche.php?m=2289">Fiche programme</a>
						</chapter-notion>
					</li>

					<li data-has-link>
						<chapter-id><?= $pds ?></chapter-id>
						<chapter-title><a href='https://cs.ip-paris.fr/courses/chps/paam/'>CSC5101</a></chapter-title>
						<span class='niveau'>Master</span>
						<chapter-content>Advanced programming of multi-core architectures</chapter-content>
						<chapter-notion>
						</chapter-notion>
					</li>
        </ul>
      </li>

    </ul>
  </li>

  <li>
    Masters Paris-Saclay
    <ul>
			<li>
				Master CHPS
				<ul>
					<li>
						Master 2
						<ul class='chapitres'>
							<li data-has-link>
								<chapter-id><?= $chps ?></chapter-id>
								<chapter-title><a href='https://cs.ip-paris.fr/courses/chps/paam/'>PAAM</a></chapter-title>
								<span class='niveau'>M2</span>
								<chapter-content>Programmation avancée des architectures multicoeurs</chapter-content>
								<chapter-notion>
								</chapter-notion>
							</li>
						</ul>
					</li>
				</ul>
			</li>
      
			<li>
				Master Datascale
				<ul>
					<li>
						Master 2
						<ul class='chapitres'>
							<!-- Walid -->
							<li data-has-link>
								<chapter-id><?= $datascale ?></chapter-id>
								<chapter-title><a href='https://www-inf.telecom-sudparis.eu/SIMBAD/courses/doku.php?id=teaching_assistant:cloud_computing'>Cloud Computing</a></chapter-title>
								<span class='niveau'>M2</span>
								<chapter-content>Cloud Computing</chapter-content>
								<chapter-notion></chapter-notion>
							</li>

							<li data-has-link>
								<chapter-id><?= $datascale ?></chapter-id>
								<chapter-title><a href='https://www-inf.telecom-sudparis.eu/SIMBAD/courses/doku.php?id=teaching_assistant:workflow'>Gestion des processus métiers</a></chapter-title>
								<span class='niveau'>M2</span>
								<chapter-content>Gestion des processus métiers</chapter-content>
								<chapter-notion></chapter-notion>
							</li>

							<li data-has-link>
								<chapter-id><?= $datascale ?></chapter-id>
								<chapter-title><a href='https://www-inf.telecom-sudparis.eu/SIMBAD/courses/doku.php?id=teaching_assistant:web_services'>Architectures Orientées Services</a></chapter-title>
								<span class='niveau'>M2</span>
								<chapter-content>Architectures Orientées Services</chapter-content>
								<chapter-notion></chapter-notion>
							</li>
						</ul>
          </li>
				</ul>
			</li>
    </ul>
  </li> <!-- Paris-Saclay -->

<!--	<?php if(!isset($newtheme) || !$newtheme) echo '<ul><li>'; ?> -->
</ul>




<!-- Local Variables: -->
<!-- mode: web -->
<!-- mode: flyspell -->
<!-- ispell-dictionary: "french" -->
<!-- coding: utf-8 -->
<!-- End: -->
