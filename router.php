<?php

$wppath = '/cours/infra-web-cours/wp/';
$request = $_SERVER['DOCUMENT_ROOT'] . '/' . $_SERVER["REQUEST_URI"];

if(false) {
  echo "Loading: " . $request . '<br>';
  echo "Is dir: " . (is_dir($request) ? "yes" : "no") . '<br>';
  if(is_dir($request)) {
    echo "Contains index.html: " . (file_exists($request . '/index.html') ? 'yes' : 'no') . '<br>';
    echo "Contains index.php: " . (file_exists($request . '/index.php') ? 'yes' : 'no') . '<br>';
  }
  print_r($_SERVER);
}

if((strncmp($_SERVER["PHP_SELF"], $wppath, strlen($wppath)) == 0) || 
   !is_dir($request) ||
   file_exists($request . '/index.php') || 
   file_exists($request . '/index.html')) {
  return false;
} else {
  echo "<h2>Index of " . $_SERVER["REQUEST_URI"] . ":</h2>";
  foreach(scandir($request) as $cur) {
    echo "<a href='" . 
         preg_replace('|/+|', '/', $_SERVER["REQUEST_URI"] . '/' . $cur . (is_dir($request . '/' . $cur) ? '/' : '')) .
         "'>" . $cur . "</a><br>";
  }
  return true;
}

?>