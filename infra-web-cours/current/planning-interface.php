<?php

$dictionaries = array('fr' => array('Cours' => 'Cours',
                                    'Date' => 'Date',
                                    'Horaire' => 'Horaire',
                                    'Groupe' => 'Groupe',
                                    'Salle' => 'Salle',
                                    'Intervenant' => 'Intervenant',
                                    'à' => 'à',
                                    'Lien calendrier' => 'Lien vers le calendrier'),
                      'en' => array('Cours' => 'Course',
                                    'Date' => 'Day',
                                    'Horaire' => 'Time',
                                    'Groupe' => 'Group',
                                    'Salle' => 'Room',
                                    'Intervenant' => 'Teacher',
                                    'à' => 'to',
                                    'Lien calendrier' => 'Link to the calendar'));

//$language = 'en';

$language = array_key_exists($language, $dictionaries) ? $language : 'fr';
setlocale(LC_TIME, $language);

function intl($word) {
  global $language;
  global $dictionaries;
  return array_key_exists($word, $dictionaries[$language]) ? $dictionaries[$language][$word] : $word;
}

function error($msg) {
  throw new \Exception($msg);
}

function printDay($time) {
  global $language;
  return $time ? ucwords(IntlDateFormatter::formatObject((new DateTime())->setTimestamp($time->getTimestamp()), 'EEEE d/M', $language)) : '???';
  //return $time ? strftime("%A %e/%m", $time->getTimestamp()) : '???';
}

function printHour($time) {
  return $time ? date("G\hi", $time->getTimestamp()) : "??h??";
}

function icsSanitize($str) {
  return preg_replace('/([\,;])/','\\\$1', $str);
}

class MetaSlot {
	public $slotNames;
	public $teacherNames;

	public function __construct($slotNames, $teacherNames) {
		$this->slotNames = $slotNames;
		$this->teacherNames = $teacherNames;
	}
};

$ciMetaSlot = new MetaSlot([ "CI" ], [ "Responsable" ]);
$citpMetaSlot = new MetaSlot([ "CI", "TP" ], [ "Responsable", "Assistant" ]);

class Teacher {
	public $name;
	public $mail;
  public $color;

	public function __construct($name, $mail = null, $color = null) {
		$this->name = $name;
		$this->mail = $mail;
	}

	public static function tba() {
		return new Teacher(null, null);
	}

	public static function all() {
		return new Teacher("All", null);
	}

  public function asString($fg_color) {
    $name = $this->name;

    if(is_null($name)) {
      $name = 'TBA';
      $fg_color = $fg_color ? $fg_color : 'red';
    }

    if($fg_color)
      $name = '<span style="color: ' . $fg_color . ';">' . $name . '</span>';
    
    if(is_null($this->mail))
      return $name;
    else
		  return "<a style='text-decoration: none' href='mailto:" . $this->mail . "'>" . $name . "</a>";
  }
  
  public function __toString() {
    return $this->asString(null);
  }
}

$tba = Teacher::tba();
$all = Teacher::all();

function printTeachers($fg_color, $teachers, $sep = ", ", $last = " et ") {
  $res = "";
  if(!is_null($teachers)) {
    for($i=0; $i<count($teachers); $i++) {
      if($i > 0)
        $res .= $i == count($teachers) - 1 ? $last : $sep;
      $res .= is_a($teachers[$i], 'Teacher') ? $teachers[$i]->asString($fg_color) : $teachers[$i];
    }
  }
  return $res;
}

class Group {
  public $name;
  public $color;
  public $room;
  public $teachers;

  public function __construct($name, $color, $room, $teachers) {
    $this->name = $name;
    $this->color = $color;
    $this->room = $room;
    $this->teachers = $teachers;
  }

  public function asCells($n) {
    $res = "<tr style='background-color: " . $this->color . "'>";
    $res .= "<td style='width: 10%'>" . $this->name . "</td>";
    $w = 100 / $n;
    for($i=0; $i<$n; $i++)
      $res .= "<td style='width:" . $w . "%'>" . ($i < count($this->teachers) ? $this->teachers[$i] : "") . "</td>";
    $res .= "</tr>";
    return $res;
  }

  public function __toString() {
    return "Group " . $this->name . " in " . $this->room . " with " . printTeachers(null, $this->teachers);
  }
}

class SlotElement {
  private $duration;
  private $start;
  private $end;
  public  $nTeachers;

  private function updateDuration() {
    $this->duration = $this->start && $this->end ? $this->end->getTimestamp() - $this->start->getTimestamp() : 0;
  }

  public function __construct($day, $start, $end, $nTeachers) {
    $this->start = DateTime::createFromFormat("j/n/Y G:i", $day . " " . $start);
    $this->end = DateTime::createFromFormat("j/n/Y G:i", $day . " " . $end);
    $this->nTeachers = $nTeachers;
    $this->updateDuration();
  }

  public function getStart() {
    return $this->start;
  }

  public function getEnd() {
    return $this->end;
  }

  public function startHourStr() {
    return $this->start ? date("H\hi", $this->start->getTimestamp()) : "??:??";
  }

  public function endHourStr() {
    return $this->end ? date("H\hi", $this->end->getTimestamp()) : "??:??";
  }

  public function setDuration($duration) {
    $date = DateTime::createFromFormat("G:i", $duration);
    $this->duration = $date ? $date->getTimestamp() - DateTime::createFromFormat("G:i", "0:00")->getTimestamp() : 0;
  }

  public function getDuration() {
    return $this->duration;
  }

  public function __toString() {
    return printHour($this->start) . " " . intl('à') . " " . printHour($this->end);
  }
}

class Slot {
  public $day;
  public $slotElements;

  public function __construct($day) {
    $elements = array_slice(func_get_args(), 1, count(func_get_args()));
    $this->day = DateTime::createFromFormat('j/n/Y', $day);
    $this->slotElements = [];

    if(count($elements)%3 != 0)
      error("Slot::__construct(day, start, end, nTeachers, start, end, nTeachers...): not enough arguments");

    for($i=0; $i<count($elements); $i+=3) {
      $start = $elements[$i];
      $end = $elements[$i+1];
      $nTeachers = $elements[$i+2];
      $e = new SlotElement($day, $start, $end, $nTeachers);
      $this->slotElements[] = $e;
    }
  }

  public static function createSingleSlot($day, $start, $end, $duration = false, $nTeachers = 1) {
    $res = new Slot($day, $start, $end, $nTeachers);
    if($duration)
      $res->setDuration(0, $duration);
    return $res;
  }

  public static function createMultiSlotD($day) {
    $elements = array_slice(func_get_args(), 1, count(func_get_args()));

    if(count($elements) % 4 != 0)
      error("wrong number of parameters start/stop/duration/nTeachers");
    
    $res = new Slot($day);

    for($i=0; $i<count($elements); $i+=4) {
      $slotElement = new SlotElement($day, $elements[$i], $elements[$i+1], $elements[$i+3]);
      $slotElement->setDuration($elements[$i+2]);
      $res->slotElements[] = $slotElement;
    }

    return $res;
  }

  public function setDuration($index, $duration) {
    $this->slotElements[$index]->setDuration($duration);
  }

  public function __toString() {
    $res = is_null($this->day) ? "??/??/????" : printDay($this->day);
    $first = true;

    foreach($this->slotElements as $e) {
      $res .= $first ? " " : " et ";
      $res .= $e->__toString();
      $first = false;
    }

    return $res;
  }
}

class LabElement {
  public $lab;
  public $slotElement;
  public $room;
  public $teachers;

  public function __construct($lab, $slotElement, $room, $teachers) {
    $this->lab = $lab;
    $this->slotElement = $slotElement;
    $this->room = $room;
    $this->teachers = $teachers;
  }

  public function __toString() {
    return (is_null($this->room) ? "???" : $this->room) . " with " . printTeachers(null, $this->teachers);
  }
}

class Lab {
  public $halfBloc;
  public $color;
  public $fg_color;
  public $labElements;

  public function __construct($halfBloc, $color) {
    $this->halfBloc = $halfBloc;
    $this->color = $color;
    $this->labElements = [];
  }

  public function add($slotElement, $room, $teachers) {
    $this->labElements[] = new LabElement($this, $slotElement, $room, $teachers);
  }

  public function setRoom($pos, $room) {
    if($pos == -1)
      foreach($this->labElements as $e)
        $e->room = $room;
    else
      $this->labElements[$pos]->room = $room;
  }

  public function setTeachers($pos, $teachers) {
    $this->labElements[$pos]->teachers = $teachers;
  }

  public function __toString() {
    $res = "";
    for($i=0; $i<count($this->labElements); $i++) {
      if($i > 0)
        $res .= ", ";
      $res .= $this->labElements[$i];
    }
    return $res;
  }
}

class HalfBloc {
  public $bloc;
  public $slot;
  public $labs;

  public function __construct($bloc, $slot) {
    $this->bloc = $bloc;
    $this->slot = $slot;
    $this->labs = [];
  }

  public function get($name) {
    return array_key_exists($name, $this->labs) ? $this->labs[$name] : null;
  }

  public function nbRows() {
    return count($this->labs);
  }

  public function createLab($name, $color) {
    $res = new Lab($this, $color);
    $this->labs[$name] = $res;
    return $res;
  }

  public function add($name, $color, $room, $teachers) {
    if(!is_null($this->get($name)))
      error('lab ' . $name . " is already associated to " . $this->slot);

    $lab = $this->createLab($name, $color);

    foreach($this->slot->slotElements as $e) {
      $lab->add($e, $room, array_slice($teachers, 0, $e->nTeachers));
    }

    return $this;
  }

  public function __toString() {
    $res = $this->slot . ": <ul>";
    foreach($this->labs as $name => $lab)
      $res .= "<li>" . $name . " in " . $lab . "</li>";
    return $res . "</ul>";
  }
}

class Bloc {
  public $schedule;
  public $name;
  public $halfBlocs;

  public function __construct($schedule, $name) {
    $this->schedule = $schedule;
    $this->name = $name;
    $this->halfBlocs = [];
  }

  public function nbRows() {
    $res = 0;
		foreach($this->halfBlocs as $halfBloc)
			$res += $halfBloc->nbRows();
    return $res;
  }

  public function checkDoesNotExists($name) {
    foreach($this->halfBlocs as $halfBloc)
      if(!is_null($halfBloc->get($name)))
        error("group " . $name . " already associated to bloc " . $this->name);
  }

  public function createHalfBloc($slot) {
    $halfBloc = new HalfBloc($this, $slot);
    $this->halfBlocs[] = $halfBloc;
    return $halfBloc;
  }

  public function add($slot, $name, $color, $room) {
    $teachers = array_slice(func_get_args(), 4, count(func_get_args()));

    $this->checkDoesNotExists($name);
    $this->createHalfBloc($slot)->add($name, $color, $room, $teachers);
    return $this;
  }

  public function addGroups($slot, $groups) {
    $halfBloc = $this->createHalfBloc($slot);
    
    foreach($groups as $group) {
      $this->checkDoesNotExists($group->name);
      $halfBloc->add($group->name, $group->color, $group->room, $group->teachers);
    }
    
    return $this;
  }

  public function getLab($name) {
    foreach($this->halfBlocs as $halfBloc) {
      $lab = $halfBloc->get($name);
      if(!is_null($lab))
        return $lab;
    }
    error("unable to find the group " . $name);
  }

  public function setRoom($group, $room, $pos = -1) {
    $this->getLab($group->name)->setRoom($pos, $room);
    return $this;
  }

  public function replaceTeacher($group, $labIndex, $teacherIndex, $teacher) {
    $labElements = $this->getLab($group->name)->labElements;

    if(count($labElements) < $labIndex)
      error("not enough labElements");
    
    if(count($labElements[$labIndex]->teachers) < $teacherIndex)
      error("not enough teachers");

    $labElements[$labIndex]->teachers[$teacherIndex] = $teacher;

    return $this;
  }

  public function addTeacher($group, $labIndex, $teacher) {
    $labElements = $this->getLab($group->name)->labElements;

    if(count($labElements) < $labIndex)
      error("not enough labElements");

    $labElements[$labIndex]->teachers[] = $teacher;

    return $this;
  }

  public function setTeachersAt($group, $labIndex) {
    $labElements = $this->getLab($group->name)->labElements;

    if(count($labElements) < $labIndex)
      error("not enough labElements");

    $labElements[$labIndex]->teachers = array_slice(func_get_args(), 2, count(func_get_args()));
  }

  public function setTeachersColorExplicit($fg_color, $group, $teachers) {
    foreach($this->halfBlocs as $halfBloc) {
      $lab = $halfBloc->get($group->name);
      if(!is_null($lab)) {
        $elements = $halfBloc->slot->slotElements;
        for($i = 0; $i < count($elements); $i++)
          $lab->setTeachers($i, array_slice($teachers, 0, $elements[$i]->nTeachers));
        if($fg_color)
          $lab->fg_color = $fg_color;
        return $this;
      }
    }
    error("unable to find the group " . $name);
  }

  public function setTeachersColor($fg_color, $group) {
    $teachers = array_slice(func_get_args(), 2, count(func_get_args()));
    return $this->setTeachersColorExplicit($fg_color, $group, $teachers);
  }
  
  public function setTeachers($group) {
    $teachers = array_slice(func_get_args(), 1, count(func_get_args()));
    return $this->setTeachersColorExplicit(null, $group, $teachers);
  }

  public function __toString() {
    $res = $this->name . "<ul>";
    foreach($this->halfBlocs as $halfBloc)
      $res .= "<li>" . $halfBloc . "</li>";
    return $res . "</ul>";
  }
}

class Summary {
  public $teacher;
  public $labElements;
  public $elements;

	public function __construct($teacher) {
    $this->teacher = $teacher;
    $this->elements = [];
  }

  public function __toString() {
    return "Summary&lt;" . $this->teacher . "&gt;";
  }
}

class Schedule {
  public $year;
  public $coordinators;
  public $groups;
  public $blocs;

  public function __construct($year) {
    $this->year = $year;
    $this->coordinators = array_slice(func_get_args(), 1, count(func_get_args()));
    $this->groups = [];
    $this->blocs = [];
  }

  public function createGroup($name, $color, $room) {
    $teachers = array_slice(func_get_args(), 3, count(func_get_args()));
    $res = new Group($name, $color, $room, $teachers);
    $this->groups[] = $res;
    return $res;
  }

  public function createBloc($name) {
    $res = new Bloc($this, $name);
    $this->blocs[] = $res;
    return $res;
  }

  public function displayCoordinators() {
    return printTeachers(null, $this->coordinators);
  }

  public function displayGroups($metaSlot) {
		$res = "<table class='groupe'><thead><tr><th>" . intl('Groupe') . "</th>";
		foreach($metaSlot->teacherNames as $cur)
			$res .= "<th>" . $cur . "</th>";
		$res .= "</tr></thead><tbody>";

		foreach($this->groups as $group) {
			$res = $res . $group->asCells(count($metaSlot->teacherNames));
		}
		return $res . "</tbody></table>";
  }

  public function getSummaries() {
    global $tba;
    global $all;

    $summaries=[];
    $summaries[$all->name] = new Summary($all);
    
    foreach($this->blocs as $bloc)
      foreach($bloc->halfBlocs as $halfBloc)
        foreach($halfBloc->labs as $lab)
          foreach($lab->labElements as $labElement){
            foreach($labElement->teachers as $teacher){
              if($teacher !== $tba && is_a($teacher, 'Teacher')) {
                $slotElement = $labElement->slotElement;

                if($slotElement->getDuration() > 0) {
                  if(!array_key_exists($teacher->name, $summaries))
                    $summaries[$teacher->name] = new Summary($teacher);
                  $summaries[$teacher->name]->labElements[] = $labElement;
                }
	      }
	    }
            $summaries[$all->name]->labElements[] = $labElement;
	  }

    /* sort by teacher name */
    uksort($summaries, 
           function($n1, $n2) { return strcmp($n1, $n2); });

    /* sort the slotElements of the teachers */
    foreach($summaries as $name => $summary)
      usort($summary->elements, 
            function($e1, $e2) { 
              return $e1->slotElement->start->getTimestamp() - $e2->slotElement->start->getTimestamp(); });
    
    return $summaries;
  }

  public function displaySchedule($metaSlot) {
	  $nbCols = count($metaSlot->slotNames);

	  /* Cours Date | Heure Groupe Salle Intervenants | Heure Groupe Salle Intervenant */
	  /* Small Norm   Norm  Small  Small  Large         Norm  Small  Small  ExtraLarge */
	  $small = 1;
	  $normal = 1.6;
	  $large = 3.2;
	  $extralarge = 3.2;
	  $dosmall = "";
	  $donormal = "";
	  $dolarge = "";
	  $doextralarge = "";

		$w = $small + $normal + $normal + $small + $small + $large; /* cours date, first slot */
		$w +=  ($nbCols - 1) * ($normal + $small + $small + $extralarge);
		$dosmall =      " style='width:" . 100*$small/$w      . "%'";
		$donormal =     " style='width:" . 100*$normal/$w     . "%'";
		$dolarge =      " style='width:" . 100*$large/$w      . "%'";
		$doextralarge = " style='width:" . 100*$extralarge/$w . "%'";

	  /* table header */
	  $res = "<table class='planning'>" .
			     "<thead>" .
			     "  <tr>" .
			     "    <th rowspan='2'" . $dosmall . ">" . intl('Cours') . "</th>" .
			     "    <th rowspan='2'" . $donormal . ">" . intl('Date') . "</th>";

	  for($i=0; $i<$nbCols; $i++) {
		  $cur = $metaSlot->slotNames[$i];
		  $res .= "    <th colspan='4'>" . $cur . "</th>";
	  }
	  
    $res .= "  <tr>";
	  for($i=0; $i<$nbCols; $i++)
		  $res .= "    <th" . $donormal . ">" . intl('Horaire') . "</th>" .
				      "    <th" . $dosmall . ">" . intl('Groupe') . "</th>" .
				      "    <th" . $dosmall . ">" . intl('Salle') . "</th>" .
				      "    <th" . ($i == 0 ? $dolarge : $doextralarge) . ">" . intl('Intervenant') . "</th>";
	  $res .= "  </tr></thead><tbody>";

	  /* generate the blocs */
	  foreach($this->blocs as $bloc) {
		  /* the bloc name (printed a single time per bloc) */
		  $blocString = "<td rowspan='" . $bloc->nbRows() . "'>" . $bloc->name . "</td>";

		  foreach($bloc->halfBlocs as $halfBloc) {
        /* number of rows in this halfBloc */
			  $nbRows = $halfBloc->nbRows();
        $rowSpan = " rowspan='" . $nbRows . "'";

        $day = "<td" . $rowSpan . ">" . printDay($halfBloc->slot->day) . "</td>";

        $first = true;
        foreach($halfBloc->labs as $groupName => $lab) {
          /*"unify the number of columns */
	        $c = " colspan='" . (count($metaSlot->slotNames) / count($lab->labElements)) . "'";

				  $res .= "<tr>";
          
				  /* col 1: bloc name */
				  $res .= $blocString;
				  $blocString = "";

          /* col 2: day */
          $res .= $day;
          $day = "";

          foreach($lab->labElements as $labElement) {
            /* col 3: time */
            $res .= $first ?
                    "<td" . $c . $rowSpan . ">" . $labElement->slotElement . "</td>" :
                    "";

            /* 3 columns per labElement */
            $res .= "<td" . $c . " style='background-color: " . $lab->color . "'>" . $groupName . "</td>";
            $res .= "<td" . $c . " style='background-color: " . $lab->color . "'>" . $labElement->room . "</td>";
            $res .= "<td" . $c . " style='background-color: " . $lab->color . "'>" . 
                    printTeachers($lab->fg_color, $labElement->teachers) . 
                         "</td>";
          }
				  
          $res .= "</tr>";

          $first = false;
			  }
		  }
	  }
    
    return $res . "</tbody></table>";
  }

  public function displayICSSelector() {
	  global $moduleId;
	  global $planningNew;
	  global $root;
	  global $web;
	  
    $res =  '<div style="text-align: center">';
    $res .= '  <select onchange="selectics(this)">';
    $res .= '    <option value="" disabled selected style="display:none;">' . intl('Lien calendrier') . '</option>';

    foreach($this->getSummaries() as $summary) {
      $res .= '  <option value="' . $summary->teacher->name . '">' . $summary->teacher->name . '</option>';
    }

    $res .= '  </select>' .
            '  <span class="icsbox">...</span>' .
			      '</div><br>';

    return $res;
  }

  public function generateICS($name) {
    global $moduleId;

    $summaries = $this->getSummaries();
    if(!array_key_exists($name, $summaries))
      error("teacher: " . $name . " does not exist");

	  $res = [ 'BEGIN:VCALENDAR',
					   'VERSION:2.0',
					   'PRODID:www-inf.telecom-sudparis.eu',
					   'CALSCALE:GREGORIAN',
             'NAME:' . $moduleId,
             'DESCRIPTION: ' . $moduleId,
					   'REFRESH-INTERVAL;VALUE=DURATION:PT12H'
		];

    foreach($summaries[$name]->labElements as $labElement) {
      $start = $labElement->slotElement->getStart();
      $end = $labElement->slotElement->getEnd();

      if($start && $end) {
		    $start->setTimezone(new DateTimeZone('UTC'));
		    $end->setTimezone(new DateTimeZone('UTC'));
		    $res = array_merge($res, [ 'BEGIN:VEVENT',
						                       'DTSTART:' . $start->format('Ymd\THis\Z'),
						                       'DTEND:' . $end->format('Ymd\THis\Z'),
						                       'DTSTAMP:' . (new DateTime('now'))->format('Ymd\THis\Z'),
						                       'LOCATION:' . icsSanitize($labElement->room),
						                       'UID:' . uniqid(),
						                       'SUMMARY:' . icsSanitize($moduleId) . ' - ' . $labElement->lab->halfBloc->bloc->name,
						                       'END:VEVENT' ]);
		  }
    }

	  $res[] = 'END:VCALENDAR';

    ob_get_clean();
    ob_start();
    header('Content-type: text/calendar; charset=utf-8');
    $nameWithoutSpace = str_replace(" ", "_", $name);
    header('Content-Disposition: inline; filename=calendar_' . $nameWithoutSpace . '.ics');
    ob_clean();
	  echo implode("\r\n", $res);
    exit(0);
  }
  
  public function displaySynthesis($format) {
    $summaries = $this->getSummaries();

    echo '<table class="groupe">';
    echo "  <thead>";
    echo "    <tr>";
    echo "      <td><b>Enseignant</b></td>";
    foreach($format as $className => $list)
      echo "    <td><b>" . $className . " (séances de " . $list[0] . "h)</b></td>";
    echo "    </tr>";
    echo "  </thead>";
    echo "  <tbody>";
  
    foreach($summaries as $name => $summary) {
      echo "<tr>";
      echo "  <td>" . $summary->teacher . "</td>";
      //$hours = getHours($schedule, $teacher_name, $format, $debug);

      $hours = [];
      $unknown = 0;

      foreach($format as $className => $list)
        $hours[$className] = 0;

      foreach($summary->labElements as $labElement) {
        $name = $labElement->lab->halfBloc->bloc->name;
        $duration = $labElement->slotElement->getDuration();
        $found = false;

        foreach($format as $className => $list) {
          if(in_array($name, $list[1])) {
            if($found)
              error("inconsistent format");
            $found = true;
            $hours[$className] += $duration;
//            echo $summary->teacher . " => " . floor($duration / 3600) . 'h' . 
//               strftime('%M', $duration) . " - " . $hours[$className] . "<br>";
          }
        }

        if(!$found)
          $unknown += $duration;
      }

      foreach($hours as $className => $duration) {
        echo '<td>';
        if($duration > 0)
          echo floor($duration / 3600) . 'h' . sprintf("%02d", (floor($duration / 60) % 60)) .
               ' (' . number_format($duration / ($format[$className][0] * 3600), 1, ".", "") . ' séances)';
        echo '</td>';
      }

      if($unknown > 0)
        echo '<td>Warning: ' . number_format($unknown/3600, 1, '.', '') . ' hours not included</td>';
      echo '</tr>';
    }
    echo "</tbody>";
    echo '</table>';
  }

  public function __toString() {
    $res = "<h1>Groups</h1><ul>";
    foreach($this->groups as $group)
      $res .= "<li>" . $group . "</li>";
    $res .= "</ul><h1>Blocs</h1><ul>";
    foreach($this->blocs as $bloc)
      $res .= "<li>" . $bloc . "</li>";
    $res .= "</ul>";
    return $res;
  }
}

include($root . '/' . $web . '/teachers.php');

/*
 *    legacy
 */
class Team {
  public $schedule;

  public function __construct($coordinators) {
    $this->schedule = new Schedule("");
    $this->schedule->coordinators = $coordinators;
  }

  public function createGroup($name, $teachers) {
    $colors = [ 'Cyan', 'Cornsilk', 'LightPink',  'Khaki', 'PaleGreen',
                'PeachPuff', 'PowderBlue', 'Thistle', 'Cornsilk' ];
    $res = $this->schedule->createGroup($name, $colors[count($this->schedule->groups)], "???");
    $res->teachers = $teachers;
    return $res;
  }

  public function displayCoordinators() {
    return "Responsables : " . $this->schedule->displayCoordinators();
  }

  public function displayTeam($metaSlot = null) {
    global $citpMetaSlot;
    if(!$metaSlot)
      $metaSlot = $citpMetaSlot;
    return $this->schedule->displayGroups($metaSlot);
  }
}

function decho($str) {
  //echo $str;
}

function fromLegacySchedule($oldSchedule, $team) {
  decho('Schedule: <ul>');
  foreach($oldSchedule as $blocName => $obloc) {
    decho('<li>bloc: ' . $blocName . '<ul>');
    $nbloc = $team->schedule->createBloc($blocName);

    foreach($obloc as $ohalfBloc) {
      if(!array_key_exists('groups', $ohalfBloc) || !array_key_exists('slots', $ohalfBloc))
        error("unexpected halfbloc");

      $day = array_key_exists('day', $ohalfBloc) ? $ohalfBloc['day'] : '???';
      $groups = $ohalfBloc['groups'];
      $oslots = $ohalfBloc['slots'];

      $nslot = new Slot($day);

      foreach($oslots as $oslotElement) {
        $start = array_key_exists('start', $oslotElement) ? $oslotElement['start'] : '???';
        $end = array_key_exists('end', $oslotElement) ? $oslotElement['end'] : '???';
        $nTeachers = array_key_exists('nteachers', $oslotElement) ? $oslotElement['nteachers'] : 1;
        $nslot->slotElements[] = new SlotElement($day, $start, $end, $nTeachers);
      }

      $nhalfBloc = $nbloc->createHalfBloc($nslot);

      decho('<li>' . $nhalfBloc . '<ul>');
      foreach($groups as $groupDescriptor) {
        if(!array_key_exists('group', $groupDescriptor))
          error("unable to find group");
        $group = $groupDescriptor['group'];
        $room = array_key_exists('room', $groupDescriptor) ? $groupDescriptor['room'] : '???';
        $name = $group instanceof Group ? $group->name : 'Promo';
        $color = $group instanceof Group ? $group->color : 'LightGray';
        $teachers = array_key_exists('teachers', $groupDescriptor) ? $groupDescriptor['teachers'] : null;
        $teachers = is_null($teachers) ? ($group instanceof Group ? $group->teachers : null) : $teachers;
        
        decho('<li>group name: ' . $name . '</li>');
        decho('<li>room: ' . $room . '</li>');
        decho('<li>color: ' . $color . '</li>');
        decho('<li>teachers: ' . printTeachers(null, $teachers) . '</li>');

        $lab = $nhalfBloc->createLab($name, $color);

        decho('<li>create slotElements: <ul>');
        for($i = 0; $i < count($oslots); $i ++) {
          $nslotElement = $nslot->slotElements[$i];
          $oslotElement = $oslots[$i];

          $tmpRoom = $room;
          $tmpTeachers = is_null($teachers) ? null : array_slice($teachers, 0, $nslotElement->nTeachers);
          decho('<li>nslotElement: ' . $nslotElement . ' - ' . $nslotElement->nTeachers . '</li>');
          decho('<li>teachers sliced: ' . printTeachers(null, $tmpTeachers) . '</li>');

          if(array_key_exists('teachers', $oslotElement)) {
            $tmpTeachers = array_slice($oslotElement['teachers'], 0, $nslotElement->nTeachers);
            decho('<li>teachers: ' . printTeachers(null, $tmpTeachers) . '</li>');
          }

          if(array_key_exists('override', $oslotElement))
            foreach($oslotElement['override'] as $entry) {
              if(!array_key_exists('group', $entry) || !($entry['group'] instanceof Group))
                error('malformed override');

              if($entry['group']->name === $name) {
                if(array_key_exists('teachers', $entry))
                  $tmpTeachers = array_slice($entry['teachers'], 0, $nslotElement->nTeachers);
                if(array_key_exists('room', $entry))
                  $tmpRoom = $entry['room'];
                decho('<li><b>override</b> for group: ' . 
                      $name . ' - ' . printTeachers(null, $tmpTeachers) . ' - ' . $tmpRoom . '</li>');
              }
            }

          global $tba;
          if(is_null($tmpTeachers)) {
            $tmpTeachers = [];
            for($i = 0; $i < $nslotElement->nTeachers; $i++)
              $tmpTeachers[] = $tba;
          }

          $lab->add($nslotElement, $tmpRoom, $tmpTeachers);
        }
        decho('</ul></li>');
      }
      decho('</ul></li>');
    }
    decho('</ul>');
  }
  decho('</ul>');

  return $team->schedule;
}

function displaySchedule($oldSchedule, $team, $metaSlot = null) {
  global $citpMetaSlot;
  if(!$metaSlot)
    $metaSlot = $citpMetaSlot;
  $team->schedule->blocs = [];

  $schedule = fromLegacySchedule($oldSchedule, $team);
  //echo $schedule->displayICSSelector();
  echo $schedule->displaySchedule($metaSlot);
}

include($root . '/' . $planning);

?>
