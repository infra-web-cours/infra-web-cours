<?php 

$language = isset($language) ? $language : 'fr';

if(!ini_get('date.timezone') ) {
	date_default_timezone_set('Europe/Paris');
}

if(isset($_REQUEST['ics'])) {
  include("planning-interface.php");
  $schedule->generateICS($_GET['ics']);
} else
  include('pager.php'); 
?>
