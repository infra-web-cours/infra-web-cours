<?xml version="1.0" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <!-- simply forward other nodes  -->
  <xsl:template match="@*|node()">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()" />
    </xsl:copy>
  </xsl:template>

  <!-- ignore ignore attribute  -->
  <xsl:template match="/nav//node()[@ignore]"/>

  <!-- include nav in generate-schedule tags  -->
  <xsl:template match="/main/nav"/>
  
  <xsl:template match="generate-schedule">
    <xsl:copy-of select="/main/nav//node()[contains(@class,'chapitre')]/node()"/>
  </xsl:template>
  
  <!-- simple tags rewritting  -->
  <xsl:template match="figref">
    <span class='{local-name()}'>
      <xsl:apply-templates select="@*|node()" />
    </span>
  </xsl:template>

  <xsl:template match="/main|objectif|bravo|attention|enconstruction|remarque|partie|figenv|figure|figcontent|figcontent|figcap|chapter-head|all-in-one">
    <div class='{local-name()}'>
      <xsl:apply-templates select="@*|node()" />
    </div>
  </xsl:template>

  <xsl:template match="schedule">
    <ul class='{local-name()}'>
      <xsl:apply-templates select="@*|node()" />
    </ul>
  </xsl:template>
  
  <xsl:template match="all-in-one">
    <div class='{local-name()}-container'>
      <xsl:apply-templates select="@*|node()" />
    </div>
  </xsl:template>
  
  <!-- fix first paragraph inside question -->
  <xsl:template match="question/p[1]">
    <p class='inline-paragraph'>
      <xsl:apply-templates select="@*|node()" />
    </p>
    <p></p>
  </xsl:template>
  
  <!-- rewrite tags that create toogles -->
  <xsl:template match="chapitre">
    <xsl:call-template name="rewrite-toggle">
      <xsl:with-param name="heading">h2</xsl:with-param>
      <xsl:with-param name="container">div</xsl:with-param>
    </xsl:call-template>
  </xsl:template>

  <xsl:template match="exercice">
    <xsl:call-template name="rewrite-toggle">
      <xsl:with-param name="heading">h3</xsl:with-param>
      <xsl:with-param name="container">div</xsl:with-param>
    </xsl:call-template>
  </xsl:template>
  
  <xsl:template match="question">
    <xsl:call-template name="rewrite-toggle">
      <xsl:with-param name="heading">h4</xsl:with-param>
      <xsl:with-param name="container">div</xsl:with-param>
    </xsl:call-template>
  </xsl:template>

  <!-- no soluce -->
  <xsl:template match="@no-soluce"/>
  
  <xsl:template match="no-soluce">
    <xsl:apply-templates select="@*|node()" />
  </xsl:template>
    
  <xsl:template match="solution">
    <xsl:if test="not(ancestor::*/no-soluce) and not(ancestor::*[@no-soluce])">
      <xsl:call-template name="rewrite-toggle">
        <xsl:with-param name="heading">div</xsl:with-param>
        <xsl:with-param name="container">div</xsl:with-param>
      </xsl:call-template>
    </xsl:if>
  </xsl:template>
  
  <xsl:template match="tip">
    <xsl:call-template name="rewrite-tips-details"/>
  </xsl:template>

  <xsl:template match="details">
    <xsl:call-template name="rewrite-tips-details"/>
  </xsl:template>
  
  <xsl:template name="rewrite-tips-details">
    <xsl:choose>
      <xsl:when test="@class[contains(.,'as-paragraph')]|descendant::*[local-name()='p']|descendant::*[local-name()='ul']|descendant::*[local-name()='table']|descendant::*[contains(local-name(),'fig')]">
        <xsl:call-template name="rewrite-toggle">
          <xsl:with-param name="heading">div</xsl:with-param>
          <xsl:with-param name="container">div</xsl:with-param>
        </xsl:call-template>
      </xsl:when>
      <xsl:otherwise>
        <xsl:call-template name="rewrite-toggle">
          <xsl:with-param name="heading">span</xsl:with-param>
          <xsl:with-param name="container">span</xsl:with-param>
        </xsl:call-template>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>

  <xsl:template name="rewrite-toggle">
    <xsl:param name='heading'/>
    <xsl:param name='container'/>

    <xsl:variable name="id" select="concat(concat(local-name(), '-'), generate-id())" />
    <xsl:variable name="attrs" select="@*[not(name() = 'title') and not(name() = 'class') and not(name() = 'raw')]"/>
    <xsl:variable name='classes' select="@class"/>
    
    <xsl:element name='{$container}'>
      <xsl:attribute name='class'><xsl:value-of select="local-name()"/>-container <xsl:value-of select="$classes"/></xsl:attribute>
      <xsl:copy-of select="$attrs"/>

      <xsl:element name='{$heading}'>
        <xsl:attribute name='class'>
          <xsl:value-of select="local-name()"/>-toggle
          toggle
          <xsl:if test="@raw">raw</xsl:if>
          <xsl:value-of select="$classes"/>
        </xsl:attribute>
        <xsl:attribute name='data-toggle-to'><xsl:value-of select="$id"/></xsl:attribute>
        <xsl:copy-of select="$attrs"/>

        <xsl:value-of select="./@title" disable-output-escaping="yes"/>
      </xsl:element>
      
      <xsl:element name='{$container}'>
        <xsl:attribute name='class'><xsl:value-of select="local-name()"/>-content <xsl:value-of select="$classes"/></xsl:attribute>
        <xsl:attribute name='id'><xsl:value-of select="$id"/></xsl:attribute>
        <xsl:copy-of select="$attrs"/>

        <xsl:apply-templates select="node()" />
      </xsl:element>
      
    </xsl:element>
  </xsl:template>

  <!-- code -->
  <xsl:template match="code">
    <xsl:variable name='orig-classes' select="@class"/>
    <xsl:variable name="classes">
      <xsl:choose>
        <xsl:when test="@url and not(@only-ref)"><xsl:value-of select="@class"/> code-block</xsl:when>
        <xsl:otherwise><xsl:value-of select="@class"/></xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    <xsl:variable name="attrs" select="@*[not(name() = 'prettify') and not(name() = 'noprettify') and not(name() = 'lang') and not(name() = 'url') and not(name() = 'only-ref') and not(name() = 'class')]"/>
    <xsl:variable name="container">
      <xsl:choose>
        <xsl:when test="contains($classes,'code-block')">div</xsl:when>
        <xsl:otherwise>span</xsl:otherwise>
      </xsl:choose>
    </xsl:variable>
    
    <xsl:element name='{$container}'>
      <xsl:attribute name="class">code <xsl:value-of select="$classes"/></xsl:attribute>
      <xsl:copy-of select="$attrs"/>
      
      <xsl:if test="@url">
        <xsl:variable name="file-name">
          <xsl:call-template name="substring-after-last">
            <xsl:with-param name="string" select="@url"/>
            <xsl:with-param name="char" select="'/'"/>
          </xsl:call-template>
        </xsl:variable>

        <xsl:variable name="reftag">
          <xsl:choose>
            <xsl:when test="@only-ref">span</xsl:when>
            <xsl:otherwise>div</xsl:otherwise>
          </xsl:choose>
        </xsl:variable>

        <xsl:element name="{$reftag}">
          <xsl:attribute name='class'>code-url</xsl:attribute>
          <a href="{@url}" download="{$file-name}"><xsl:value-of select="$file-name"/></a>
        </xsl:element>
      </xsl:if>

      <xsl:if test="not(@only-ref)">
        <xsl:element name='{$container}'>
          <xsl:attribute name="class">code-container</xsl:attribute>
          <xsl:choose>
            <xsl:when test="@noprettify"></xsl:when>
            <xsl:when test="@prettify|@url"><xsl:attribute name="data-code-prettify"><xsl:value-of select="@lang"/></xsl:attribute></xsl:when>
            <xsl:otherwise></xsl:otherwise>
          </xsl:choose>

          <xsl:choose>
            <xsl:when test="@*[name() = 'url']">
              <xsl:attribute name="data-code-url"><xsl:value-of select="@url"/></xsl:attribute>
            </xsl:when>
            <xsl:otherwise>
              <xsl:apply-templates select="node()" />
            </xsl:otherwise>
          </xsl:choose>
        </xsl:element>
      </xsl:if>
    </xsl:element>
  </xsl:template>

  <xsl:template name="substring-after-last">
    <xsl:param name="string"/>
    <xsl:param name="char"/>

    <xsl:choose>
      <xsl:when test="contains($string, $char)">
        <xsl:call-template name="substring-after-last">
          <xsl:with-param name="string" select="substring-after($string, $char)"/>
          <xsl:with-param name="char" select="$char"/>
        </xsl:call-template>
      </xsl:when>
      <xsl:otherwise>
        <xsl:value-of select="$string"/>
      </xsl:otherwise>
    </xsl:choose>
  </xsl:template>
</xsl:stylesheet>
