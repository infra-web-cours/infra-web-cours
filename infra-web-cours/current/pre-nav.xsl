<?xml version="1.0" ?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="@*|node()">
    <xsl:copy>
      <xsl:apply-templates select="@*|node()" />
    </xsl:copy>
  </xsl:template>

  <xsl:template match="/nav//node()[@ignore]"/>
  
  <xsl:template match="chapter-id|chapter-short|chapter-title|chapter-content|chapter-notion|expanded-version">
    <div class='{local-name()}'>
      <xsl:apply-templates select="@*|node()" />
    </div>
  </xsl:template>
</xsl:stylesheet>
