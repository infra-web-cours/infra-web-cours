<?php

$gael = new Teacher("Gaël Thomas", "gael.thomas@telecom-sudparis.eu");
$pierre = new Teacher("Pierre Sutra", "pierre.sutra at telecom-sudparis.eu");
$elisabeth = new Teacher("Élisabeth Brunet", "elisabeth.brunet at telecom-sudparis.eu ");
$badran = new Teacher("Badran Raddaoui", "badran.raddaoui at telecom-sudparis.eu");
$eric = new Teacher("Éric Lallet", "eric.lallet at telecom-sudparis.eu");
$amina = new Teacher("Amina Guermouche", "amina.guermouche at telecom-sudparis.eu");
$michel = new Teacher("Michel Simatic", "michel.simatic at telecom-sudparis.eu");
$michel_s = $michel; // retrocompatibilité
$walid = new Teacher("Walid Gaaloul", "walid.gaaloul at telecom-sudparis.eu");
$pascal = new Teacher("Pascal Hennequin", "pascal.hennequin at telecom-sudparis.eu");
$chantal = new Teacher("Chantal Taconet", "chantal.taconet at telecom-sudparis.eu");
$denis = new Teacher("Denis Conan", "denis.conan at telecom-sudparis.eu");
$francois = new Teacher("François Trahay", "francois.trahay at telecom-sudparis.eu");
$amel_m = new Teacher("Amel Mammar", "amel.mammar at telecom-sudparis.eu");
$amel_b = new Teacher("Amel Bouzeghoub", "amel.bouzeghoub at telecom-sudparis.eu");
$djamel = new Teacher("Djamel Belaid", "djamel.belaid at telecom-sudparis.eu");
$paul = new Teacher("Paul Gibson", "paul.gibson at telecom-sudparis.eu");
$mohamed = new Teacher("Mohamed Sellami", "mohamed.sellami at telecom-sudparis.eu");
$olivier_b = new Teacher("Olivier Berger", "olivier.berger at telecom-sudparis.eu");
$olivier_l = new Teacher("Olivier Levillain", "olivier.levillain at telecom-sudparis.eu");
$olivier = $olivier_b; // retrocompatibilité
$mathieu = new Teacher("Mathieu Bacou", "mathieu.bacou at telecom-sudparis.eu");
$aina = new Teacher("Aina Rasoamanana", "aina.rasoamanana at telecom-sudparis.eu");
$boubacar = new Teacher("Boubacar Kane", "boubacar.kane at telecom-sudparis.eu");
$nikolaos = new Teacher("Nikolaos Papadakis", "nikolaos.papadakis at telecom-sudparis.eu");
$chourouk = new Teacher("Chourouk Belheouane", "chourouk.belheouane at telecom-sudparis.eu");
$denisse = new Teacher("Denisse Munante", "denisse_yessica.munante_arzapalo at telecom-sudparis.eu");
$georgios = new Teacher("Georgios Bouloukakis", "georgios.bouloukakis at telecom-sudparis.eu");
$julien = new Teacher("Julien Romero", "julien.romero at telecom-sudparis.eu");
$marie = new Teacher("Marie Reinbigler", "marie.reinbigler at telecom-sudparis.eu");
$sophie = new Teacher("Sophie Chabridon", "sophie.chabridon at telecom-sudparis.eu");
$sven = new Teacher("Sven Dziadek", "sven.dziadek at telecom-sudparis.eu");
$bruno = new Teacher("Bruno Defude", "bruno.defude at telecom-sudparis.eu");
$jana = new Teacher("Jana Toljaga", "jana.toljaga at telecom-sudparis.eu");
$catherine = new Teacher("Catherine Guelque", "guelque at telecom-sudparis.eu");
$hadrien = $catherine; // retrocompatibilité
$jeff = new Teacher("Jean-François Dumollard", "jean-francois.dumollard at telecom-sudparis.eu");
$jules = new Teacher("Jules Risse", "jules.risse at telecom-sudparis.eu");

/* vacataires */
$alexis_l = new Teacher("Alexis Lescouet", "alexis.lescouet at telecom-sudparis.eu");
$alexis = $alexis_l; // retrocompatibilité
$abdallah = new Teacher("Abdallah Sobehy", "abdallah.sobehy at telecom-sudparis.eu");
$nabila = new Teacher("Nabila Belhaj", "nabila.belhaj at telecom-sudparis.eu");
$suba = new Teacher("Subashiny Tanigassalame", "subashiny.tanigassalame at telecom-sudparis.eu");
$anatole = new Teacher("Anatole Lefort", "anatole.lefort at telecom-sudparis.eu");
$vacataire = new Teacher("Vacataire", "vacataire at telecom-sudparis.eu");
$alexis_c = new Teacher("Alexis Colin", "alexis.colin at telecom-sudparis.eu");
$yohan = new Teacher("Yohan Pipereau", "yohan.pipereau at telecom-sudparis.eu");
$ayoub = new Teacher("Ayoub Ben Ameur", "ayoubbenameur09 at gmail.com");
$adam = new Teacher("Adam Chader", "adam.chader at telecom-sudparis.eu");
$adam_oumar = new Teacher("Adam Oumar Abdel-Rahman", "adam_oumar.abdel_rahman at telecom-sudparis.eu");
$arthur = new Teacher("Arthur Tran-Van", "arthur_tran-van at telecom-sudparis.eu");
$houssam = new Teacher("Houssam Hajj Hassan", "houssam.hajj_hassan at telecom-sudparis.eu");
$nickson = new Teacher("Nickson Mwamsojo", "nickson_mwamsojo at telecom-sudparis.eu");
$arthur_j = new Teacher("Arthur Jovart", "arthur.jovart at telecom-sudparis.eu");

?>
