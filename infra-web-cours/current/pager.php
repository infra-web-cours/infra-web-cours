<?php

error_reporting(E_ERROR);

function makeAbsolute($from, $dest) {
	return realpath(dirname($from) . '/' . $dest);
}

function commonPrefix($s1, $s2) {
	if(strlen($s1) > strlen($s2))
		return commonPrefix($s2, $s1);

	for($i = 0; $i < strlen($s1) && $s1[$i] === $s2[$i]; $i++);

	return substr($s1, 0, $i);
}

function makeRelativeFromAbsolute($s2) {
	global $root;
//	$s1 = realpath($root);
	$s1 = realpath(dirname($_SERVER["SCRIPT_FILENAME"]));
  $s2 = realpath($s2);
  
	$common = commonPrefix($s1, $s2);
	$pos = strrpos($common, "/");
	$e1 = substr($s1, $pos);
	$res = substr($s2, $pos+1);

	$n = substr_count($e1, "/");

	for($i=0; $i<$n; $i++)
		$res = "../" . $res;

  //echo "s1: " . $s1 . "<br>";
  //echo "s2: " . $s2 . "<br>";
  //echo "res: " . $res . "<br>";
  
  return $res;
}

function makeRelative($from, $dest) {
	return makeRelativeFromAbsolute(makeAbsolute($from, $dest));
}

function on_students_list($path, $func, $data = null) {
  $i = 0;
  $file = fopen($path, "r");
  if(!$file)
    error_reporting(E_WARNING, "unable to open: " . $path);

  while(($entry = fgetcsv($file))) {
    call_user_func($func, $entry, $i, $data);
    $i++;
  }

  fclose($file);
}

$page = isset($_REQUEST['page']) ? $_REQUEST['page'] : $main;
$debug = isset($_REQUEST['debug']) ? true : false;
$corporate = isset($corporate) ? $corporate : 'tsp';
$corporate2 = isset($_REQUEST['c']) ? $_REQUEST['c'] : $corporate;

switch($corporate2) {
    case 'ipparis':
    case 'tsp': $corporate = $corporate2;
}

if(stream_resolve_include_path(dirname(__FILE__) . '/config.php'))
  include('config.php');
else
  include('config.sample.php');

$wppath = realpath($wppath);
$wplink = makeRelativeFromAbsolute($wppath);
$weblink = makeRelativeFromAbsolute($root . '/' . $web);

set_include_path(get_include_path() . PATH_SEPARATOR . $wppath);

/* WP API */
$urlportail_default = 'https://www-inf.telecom-sudparis.eu/COURS/infra-web-cours/portail/';
$portail_default = 'Portail informatique';

$urlportail = isset($urlportail) ? $urlportail : $urlportail_default;
$portail = isset($portail) ? $portail : $portail_default;

function str_to_dom($html) {
  $dom = new DOMDocument();
  libxml_use_internal_errors(true);
  $html = mb_convert_encoding($html, 'HTML-ENTITIES', "UTF-8");
  //$html = htmlspecialchars_decode(htmlentities($html));
  $dom->loadHTML($html, LIBXML_HTML_NOIMPLIED | LIBXML_HTML_NODEFDTD);
  libxml_clear_errors();

  return $dom;
}

function apply_xslt($dom, $xslt) {
  $xsl = new DomDocument();
  $xsl->load(dirname(__FILE__) . '/' . $xslt);
  $proc = new XSLTProcessor();
  $proc->importStyleSheet($xsl); // attach the xsl rules
  
  return $proc->transformToDoc($dom);
}

function dom_to_str($dom) {
  return $dom->saveHTML($dom->documentElement);
}

ob_start();
include($root . '/'. $nav . '.php');
$navContent = ob_get_clean();
$dom_nav = apply_xslt(str_to_dom('<nav>' . $navContent . '</nav>'), 'pre-nav.xsl');

function get_bloginfo($id) {
  global $module;
  global $wplink;
  global $portail;
  global $main;

  switch($id) {
    case 'template_directory': return $wplink;
    case 'wpurl': return '?page=' . $main;
    case 'name': return $module;
    case 'description': return $portail;
    default: return "unable to resolve get_bloginfo(" . $id . ")";
  }
}

function get_theme_mod($id) {
  global $urlportail;

  switch($id) {
    case 'tagline_link': return $urlportail;
    default: return '';
  }
}

function wp_nav_menu() {
  global $dom_nav;
  echo dom_to_str(apply_xslt($dom_nav, 'nav-nav.xsl'));
}

function get_home_url() {
  return htmlspecialchars($_SERVER["PHP_SELF"]);
}

function wp_head() {
  global $corporate;
  global $wplink;
  global $weblink;
  global $myCss;
  global $module;
  global $page;
  global $main;
?>
  <title><?= $module . ($page === $main ? '' : ' - ' . basename($page)) ?></title>
  <link rel='stylesheet' type='text/css' href='//fonts.googleapis.com/css?family=Open+Sans&#038;'/>
  <link rel='stylesheet' type='text/css' href='https://use.fontawesome.com/releases/v5.7.0/css/all.css'/>
  <link rel='stylesheet' type='text/css' href='<?= $wplink ?>/bootstrap/css/bootstrap-reboot.min.css'/>
  <link rel='stylesheet' type='text/css' href='<?= $wplink ?>/style.css'/>
  <link rel='stylesheet' type='text/css' href='<?= $wplink . '/' . $corporate ?>/style.css'/>
  <link rel='stylesheet' type='text/css' href='<?= $weblink ?>/tsp.css'/>
<?php if(isset($myCss)) { ?>
  <link rel='stylesheet' type='text/css' href='<?= $myCss ?>'/>
<?php } 
}

function wp_footer() {
  global $weblink;
  global $wplink;
?>
  <script src="https://cdn.jsdelivr.net/gh/google/code-prettify@master/loader/run_prettify.js"></script>
  <script src='<?= $wplink ?>/menu.js'></script>
  <script src='<?= $weblink ?>/tsp.js'></script>
<?php
}

if(isset($preload))
  foreach($preload as $file)
    require_once($file);

include('header.php'); ?>

<div class='content cours'>
  <?php

  if(file_exists($root . '/' . $page . '.php')) {
    ob_start();
    include($root . '/' . $page . '.php');

    $soluceRules = $root . '/' . 'soluce-rules.php';

    $noSoluce = '';

    if(file_exists($soluceRules)) {
      include($soluceRules);
      $noSoluce = hideSoluce($page) ? '<no-soluce></no-soluce>' : '';
    }
    
    $dom = str_to_dom('<main id="main-content">' . $noSoluce . ob_get_clean() . '</main>');

    if($dom->getElementsByTagName('generate-schedule')->length > 0) {
      $schedule_nav = apply_xslt($dom_nav, "schedule-nav.xsl");
      $dom->documentElement->appendChild($dom->importNode($schedule_nav->documentElement, true));
    }
  
    //echo '<pre>' . htmlspecialchars(dom_to_str($dom)) . '</pre>';
    $html = dom_to_str(apply_xslt($dom, 'page.xsl'));
    //echo '<pre>' . htmlspecialchars($html) . '</pre>';
    echo $html;
  } else {
    echo "<h2 style='color: red'>Sorry, the requested resource doesn't exist</h2>";
  }
  ?>
</div>

<?php include('footer.php'); ?>
