<chapitre title='Test chapitre'>
  <partie>Une partie</partie>
  <exercice title='Test exercice'>Ceci est un exercice</exercice>
  <exercice raw title='Test exercice (raw)'>
    <question title='Test question'>Ceci est une question</question>
    <question raw title='Test question (raw)'>Ceci est une autre question</question>
    <question title='Test question avec paragraph'>
      <p>Voici un paragraphe</p><p>Et un second</p>
      <p>Un conseil dans une phrase <tip>Conseil!</tip></p>
      <p>Un conseil dans une phrase <tip>Et un autre <tip>Conseil!</tip></tip> dans une phrase</p>
      <tip class='as-paragraph'>
        <p>Un conseil de type paragraph tout seul joli <tip>Un sous-conseil en ligne</tip></p>
        <tip class='as-paragraph'>Un sous-conseil en paragraph</tip>
      </tip>
      <p>Un détail en ligne <details>Voici le détail</details></p>
      <details class='as-paragraph'>Un détail en paragraphe</details>

      <remarque>C'est une remarque</remarque>
      <attention>Attention</attention>
      <objectif>Apprendre à faire des trucs</objectif>
      <enconstruction></enconstruction>

      <bravo>Fécilitation, tout marche comme sur des roulettes&nbsp;!</bravo>
      
      <solution>
        Ceci est une solution
      </solution>
    </question>
  </exercice>
</chapitre>

<chapitre title='Test code'>
  Test code
    
  <pre>Miaou</pre>
  
  <code>Ceci est du code en ligne</code>
  <p></p>

  <code prettify lang="C">int main(int argc, char** argv)</code>
  <p></p>

  <code prettify>int main(int argc, char** argv)</code>
  <p></p>

  <code class='code-block'>Code en bloc</code>

  <code class='code-block' prettify>int main(int argc, char** argv) {
  printf("Code bloc prettify\n");
  return 0;
}</code> 

  <code class='code-block' prettify lang="C">int main(int argc, char** argv) {
  printf("Code bloc prettify\n");
  return 0;
}</code> 

  <code url='test.c'></code>

  <code url='test.c' lang="C"></code>

	<code url='<?= makeRelative(__FILE__, "/test.c"); ?>'></code>
</div>
</chapitre>

<script>forceSoluce = 1;</script> 

<chapitre title='Test no-soluce'>
  <exercice title='no-soluce comme balise'>
    <no-soluce></no-soluce>
    <question>Question sans réponse<solution>Pas visible</solution></question>
    <question>Question sans réponse<solution>Pas visible</solution></question>
  </exercice>
  <exercice title='avec solution'>
    <question>Question avec réponse<solution>Visible</solution></question>
    <question>Question avec réponse<solution>Visible</solution></question>
  </exercice>
  <exercice title='no-soluce comme attribut' no-soluce>
    <question no-soluce>Question sans réponse<solution>Pas visible</solution></question>
    <question>Question avec réponse<solution>Visible</solution></question>
  </exercice>
</chapitre>

<!-- Local Variables: -->
<!-- mode: web -->
<!-- mode: flyspell -->
<!-- ispell-dictionary: "french" -->
<!-- coding: utf-8 -->
<!-- End: -->

