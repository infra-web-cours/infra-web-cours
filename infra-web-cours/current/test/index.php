<?php
	 //header("Status: 301 Moved Permanently", false, 301);
	 //header("Location: http://www.exemple.net/repertoire/page.php");
	 //exit();

  //$theme = 'old';

	$root = dirname(__FILE__);       /* root directory of the module */
	$web = '../..'; /* relative path of the web infrastructure (technically, web files are in $root . '/' $web) */

  $language = 'fr';

	$moduleId = 'Test';
	$module = $moduleId . ' &ndash; Module de test';  /* name of the module */
	$year = 'Année 2020 &ndash; 2021';                /* current year */

	$planning = 'planning-2020.php';

	$nav = 'nav';                /* page of the nav file (technically, nav is in $root . '/' . $nav . '.php') */
	$main = 'test';              /* page of the main file (technically, main is in $root . '/' . $main . '.php') */

	include($root . '/' . $web . '/tsp.php');
?>

<!-- Local Variables:                  -->
<!-- mode: web                         -->
<!-- mode: flyspell                    -->
<!-- ispell-local-dictionary: "french" -->
<!-- coding: utf-8                     -->
<!-- End:                              -->
