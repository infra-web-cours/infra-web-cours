<?php

$default_theme = 'current';

if(!isset($theme) || !stream_resolve_include_path(tsp_path($root . '/' . $web . '/' . $theme . '/tsp.php')))
  $theme = $default_theme;

$web = $web . '/' . $theme;

include($root . '/' . $web . '/tsp.php');  

?>
