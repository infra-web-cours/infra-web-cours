<?php

$gael = new Teacher("Gaël Thomas", "gael.thomas at telecom-sudparis.eu");
$pierre = new Teacher("Pierre Sutra", "pierre.sutra at telecom-sudparis.eu");
$elisabeth = new teacher("Élisabeth Brunet", "elisabeth.brunet at telecom-sudparis.eu ");
$badran = new Teacher("Badran Raddaoui", "badran.raddaoui at telecom-sudparis.eu");
$eric = new Teacher("Éric Lallet", "eric.lallet at telecom-sudparis.eu");
$amina = new Teacher("Amina Guermouche", "amina.guermouche at telecom-sudparis.eu");
$michel_s = new Teacher("Michel Simatic", "michel.simatic at telecom-sudparis.eu");
$michel_g = new Teacher("Michel Gardie", "michel.gardie at telecom-sudparis.eu");
$walid = new Teacher("Walid Gaaloul", "walid.gaaloul at telecom-sudparis.eu");
$pascal = new Teacher("Pascal Hennequin", "pascal.hennequin at telecom-sudparis.eu");
$chantal = new Teacher("Chantal Taconet", "chantal.taconet at telecom-sudparis.eu");
$denis = new Teacher("Denis Conan", "denis.conan at telecom-sudparis.eu");
$christian = new Teacher("Christian Parrot", "christian.parrot at telecom-sudparis.eu");
$francois = new Teacher("François Trahay", "francois.trahay at telecom-sudparis.eu");
$amel_m = new Teacher("Amel Mammar", "amel.mammar at telecom-sudparis.eu");
$djamel = new teacher("Djamel Belaid", "djamel.belaid at telecom-sudparis.eu");
$paul = new teacher("Paul Gibson", "paul.gibson at telecom-sudparis.eu");
$mohamed = new teacher("Mohamed Sellami", "mohamed.sellami at telecom-sudparis.eu");
$olivier_b = new teacher("Olivier Berger", "olivier.berger at telecom-sudparis.eu");
$olivier_l = new teacher("Olivier Levillain", "olivier.levillain at telecom-sudparis.eu");
$olivier = $olivier_b; // retrocompatibilité

/* vacataires */
$alexis = new Teacher("Alexis Lescouet", "alexis.lescouet at telecom-sudparis.eu");
$abdallah = new teacher("Abdallah Sobehy", "abdallah.sobehy at telecom-sudparis.eu");
$nabila = new teacher("Nabila Belhaj", "nabila.belhaj at telecom-sudparis.eu");
$suba = new Teacher("Subashiny Tanigassalame", "subashiny.tanigassalame at telecom-sudparis.eu");
$anatole = new Teacher("Anatole Lefort", "anatole.lefort at telecom-sudparis.eu");
$vacataire = new teacher("Vacataire", "vacataire at telecom-sudparis.eu");

?>
