<?php

error_reporting(E_ALL);

function makeAbsolute($from, $dest) {
	return realpath(dirname($from) . '/' . $dest);
}

function commonPrefix($s1, $s2) {
	if(strlen($s1) > strlen($s2))
		return common_prefix($s2, $s1);

	for($i = 0; $i < strlen($s1) && $s1[$i] === $s2[$i]; $i++);

	return substr($s1, 0, $i);
}

function makeRelative($from, $dest) {
	global $root;

//  $dest = makeAbsolute($from, $dest);
//  return '.' . str_replace(realpath($root), '', $dest);


	$s1 = realpath($root);
	$s2 = makeAbsolute($from, $dest);

	$common = commonPrefix($s1, $s2);
	$pos = strrpos($common, "/");
	$e1 = substr($s1, $pos);
	$res = substr($s2, $pos+1);

	$n = substr_count($e1, "/");

	for($i=0; $i<$n; $i++)
		$res = "../" . $res;

  return $res;
}

$page = isset($_REQUEST['page']) ? $_REQUEST['page'] : $main;
$debug = isset($_REQUEST['debug']) ? true : false;
?>

<html>
  <head>
		<meta charset="utf-8"/>
    <meta http-equiv='content-type' content='text/html; charset=utf-8' />
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
		<link rel="stylesheet" type="text/css" href="https://rawgit.com/google/code-prettify/master/loader/prettify.css">
		<script type="text/javascript" src="https://rawgit.com/google/code-prettify/master/loader/prettify.js"></script>
    <link rel='stylesheet' type='text/css' href='<?= $web ?>/tsp.css'>
		<?php
		if(isset($myCss)) {
			echo '<link rel="stylesheet" type="text/css" href="' . $myCss . '">';
		}
		?>
    <meta name='author' content='Gaël Thomas'>
    <title><?= $module ?></title>
  </head>
  
  <body>
		<header>
			<portail>
				<a href='http://www-inf.telecom-sudparis.eu/COURS/infra-web-cours/portail/' width='300ptx'>
					Portail informatique
				</a>
			</portail>
			<year><?= $year ?></year>
			<navicon></navicon>
			<main-title><h1><?php echo "<a href='?page=" . $main . "'>" . $module . "</a>"; ?></h1></main-title>
			<logo-area><a href='https://www.telecom-sudparis.eu/en/'><logo-tsp></logo-tsp></a></logo-area>
		</header>

		<nav-layer>
			<nav>
				<?php include($root . '/'. $nav . '.php'); ?>
			</nav>
		</nav-layer>

		<veil></veil>

		<section id='main-content'>
			<?php include($page . '.php'); ?>
		</section>

		<footer>
		</footer>
  </body>

  <script src='<?= $web ?>/tsp.js'></script>
</html>

<!-- Local Variables:                  -->
<!-- mode: web                         -->
<!-- mode: flyspell                    -->
<!-- ispell-local-dictionary: "french" -->
<!-- coding: utf-8                     -->
<!-- End:                              -->
