
function newHTTPRequest() {
	var xhttp;

	if (window.XMLHttpRequest) {
		/* IE7+, Firefox, Chrome, Opera, Safari */
		xhttp=new XMLHttpRequest();
	} else {
		/* code for IE6, IE5 */
		xhttp=new ActiveXObject('Microsoft.XMLHTTP');
	}

	return xhttp;
}

function doURL(url) {
	return url + '?timestamp=' + new Date().getTime();
}

function doCodes(node) {
	var elements = node.getElementsByTagName('code');

	for(var i=0; i<elements.length; ++i) {
		var cur = elements[i];
		var href = cur.getAttribute('href');
		var url = cur.getAttribute('url');
		var lang = cur.getAttribute('lang');
		var noprettify = cur.getAttribute('noprettify');
		var prettify = cur.getAttribute('prettify');
		var content = document.createElement('pre');

		cur.removeAttribute('href');
		cur.removeAttribute('url');
		content.className = 'code';

		if(url) {
			var xhttp = newHTTPRequest();
			var location = doURL(url);

			xhttp.open('GET', location, true);
			xhttp.onload = function(xhttp, content) {
				return function(e) {
				  if(xhttp.status == 200 || xhttp.status == 0) {
						content.innerHTML = 
							xhttp.responseText.replace(/\t/g, '  ').replace(/</g, '&lt;').replace(/>/g, '&gt;');

						if(typeof PR !== 'undefined' && (noprettify == null || prettify))
							content.innerHTML = PR.prettyPrintOne(content.innerHTML, lang);

				  } else {
						content.innerHTML = "<h1 style='color:red; text-align:center'>Error " + xhttp.status + ": unable to load code</h1>";
				  }
				};
			}(xhttp, content);
			xhttp.send();

			var ref = document.createElement('a');
			ref.href = location;
			var tokens = url.split('/');
			ref.download = tokens[tokens.length-1];
			ref.innerHTML = ref.download;
			ref.type = 'text/html';

			if(!cur.classList.contains('code-inline'))
				cur.classList.add('code-block');

			cur.appendChild(ref);
			cur.appendChild(content);
		} else {
			content.innerHTML = cur.innerHTML;
			cur.innerHTML = null;

			if(typeof PR !== 'undefined' && (prettify != null || noprettify))
				content.innerHTML = PR.prettyPrintOne(content.innerHTML, lang);

			if(!cur.classList.contains('code-block'))
				cur.classList.add('code-inline');

			if(href) {
				var sub = content;
				content = document.createElement('a');
				content.href = doURL(href);
				var tokens = href.split('/');
				content.download = tokens[tokens.length-1];
				content.appendChild(sub);

				content.innerHTML = content.download;
			}
			
			cur.appendChild(content);
		}
	}
}

function doWeeks(node, ignoreWeek) {
  if(ignoreWeek)
    return;

	var weeks = node.getElementsByTagName('week');

	for(var i=0; i<weeks.length; i++) {
		var cur = weeks[i].getAttribute('current');
		cur = cur == null ? 0 : cur;

		var mins = Array.prototype.slice.call(weeks[i].getElementsByTagName('min-week'), 0);

		for(var j=0; j<mins.length; j++) {
			var min = mins[j].getAttribute('value');
			min = min == null ? 0 : min;
			if(cur < min) {
				mins[j].parentNode.removeChild(mins[j]);
			}
		}
	}
}

function dotoggle(node, active, inactive) {
	if(node.classList.contains(active)) {
		node.classList.add(inactive);
		node.classList.remove(active);
	} else {
		node.classList.add(active);
		node.classList.remove(inactive);
	}
}

function collapse(e) {
	if(e.style.height === 'auto') {
		e.style.height = e.scrollHeight;
		(function(e) {
			var id = requestAnimationFrame(function() {
				e.style.height = 0;
				cancelAnimationFrame(id);
			});
		}(e));
	} else
		e.style.height = 0;
}

function uncollapse(e) {
	e.style.height = e.scrollHeight; 
}


function injectPortail(nav) {
	var portail = document.getElementsByTagName('portail');
	if(portail.length > 0) {
		var uls = nav.getElementsByTagName('ul');
		for(var i=0; i<uls.length; i++) {
			root = true;
			cur = uls[i];
			for(var tmp=cur.parentNode; root && tmp.tagName !== 'NAV'; tmp = tmp.parentNode) {
				if(tmp.tagName === 'UL')
					root = false;
			}

			if(root) {
				var portail2 = document.createElement('menu-portail');
				var li = document.createElement('li');
				li.innerHTML = portail[0].innerHTML;
				portail2.appendChild(li);
				cur.appendChild(portail2);
				return;
			}
		}
	}
}

function doNav(nav) {
	injectPortail(nav);

	var elmts = nav.getElementsByTagName('li');

	for(var i=0; i<elmts.length; i++) {
		var cur = elmts[i];
		var content = document.createElement('nav-content');
		var toggle = document.createElement('nav-toggle');

		for(var tmp=cur.parentNode; tmp.tagName !== 'NAV' && tmp.tagName !== 'CHAPTER-NOTION'; tmp = tmp.parentNode) {
			if(tmp.tagName === 'LI') {
				tmp.classList.add('has-children');
				tmp.firstChild.innerHTML = '<nav-toggle-icon>\u25BA</nav-toggle-icon>';
			}
		}

		toggle.innerHTML= '&nbsp;';
		content.innerHTML = ' ' + cur.innerHTML;
		cur.innerHTML = '';
		cur.appendChild(toggle);
		cur.appendChild(content);
		cur.classList.add('nav-inactive');
		
		cur.onmouseover = function(content) {
			return function(ev) {
				content.classList.add('focus');
			};
		}(content);

		cur.onmouseout = function(content) {
			return function(ev) {
				content.classList.remove('focus');
			};
		}(content);

		cur.onclick = function(nav, node) {
			return function(ev) {
				var parent;
				for(parent=node; parent.tagName !== 'NAV' && parent.tagName !== 'UL'; parent = parent.parentNode);

				parent.style.height = 'auto';

				var elmts = parent.getElementsByTagName('li');
				for(var i=0; i<elmts.length; i++)
					if(node.classList.contains('nav-active')) {
						elmts[i].classList.remove('grey');
					} else {
						elmts[i].classList.add('grey');
						elmts[i].classList.remove('nav-active');
						elmts[i].classList.add('nav-inactive');
						var uls = elmts[i].getElementsByTagName('ul');
						for(var j=0; j<uls.length; j++) {
							collapse(uls[j]);
						}
					}

				var elmts = node.getElementsByTagName('li');
				for(var i=0; i<elmts.length; i++) {
					if(node.classList.contains('nav-active')) {
						elmts[i].classList.add('nav-inactive');
						elmts[i].classList.remove('nav-active');
					}
					elmts[i].classList.remove('grey');
				}

				dotoggle(node, 'nav-active', 'nav-inactive');

				var elmts = node.getElementsByTagName('ul');
				for(var i=0; i<elmts.length; i++) {
					if(node.classList.contains('nav-active')) {
						var spot = 1;
						for(var p = elmts[i].parentNode; p !== node; p = p.parentNode) {
							spot &= p.tagName !== 'LI' || p.classList.contains('nav-active');
						}
						if(spot)
							uncollapse(elmts[i]);
					} else {
						collapse(elmts[i]);
					}
				}

				ev.stopPropagation();
			};
		}(nav, cur);
	}
}

function createNavToggle(node) {
	return function() {
		var navs = node.getElementsByTagName('nav-layer');
		for(var i=0; i<navs.length; i++) {
			dotoggle(navs[i], 'nav-active', 'nav-inactive');
			if(navs[i].classList.contains('nav-inactive')) {
				var lis = navs[i].getElementsByTagName('li');
				for(var j=0; j<lis.length; j++) {
					lis[j].classList.add('nav-inactive');
					lis[j].classList.remove('nav-active');
					lis[j].classList.remove('grey');
					var uls = lis[j].getElementsByTagName('ul');
					for(var k=0; k<uls.length; k++)
						uls[k].style.height = 0;
				}
			}
		}
		var veils = node.getElementsByTagName('veil');
		for(var i=0; i<veils.length; i++) {
			dotoggle(veils[i], 'nav-active', 'nav-inactive');
		}
	}
}

function doNavs(node) {
	var navicons = node.getElementsByTagName('navicon');
	for(var i=0; i<navicons.length; i++) {
		navicons[i].onclick = function(node) {
			return createNavToggle(node);
		}(node);
	}

	var veils = node.getElementsByTagName('veil');
	for(var i=0; i<veils.length; i++) {
		veils[i].onclick = function(node) {
			return createNavToggle(node);
		}(node);
	}

	var navs = node.getElementsByTagName('nav-layer');
	
	for(var i=0; i<navs.length; i++) {
		doNav(navs[i]);
	}
}

function doSchedule(node) {
	var doschedules = node.getElementsByTagName('generate-schedule');

	while(doschedules.length) {
		var parent = doschedules[0].parentNode;
		parent.removeChild(doschedules[0]);

		var navs = document.getElementsByTagName('nav');
	
		for(var j=0; j<navs.length; j++) {
			var chapitres = navs[j].getElementsByClassName('chapitres');

			for(var k=0; k<chapitres.length; k++) {
				for(var l=0; l<chapitres[k].childNodes.length; l++) {
					var cur = chapitres[k].childNodes[l];
					if(cur.nodeType == 1) {
						parent.appendChild(cur.cloneNode(true));
					}
				}
			}
		}
	}
}

function createToggle(node, tagName, display, active) {
	var elmts = node.getElementsByTagName(tagName);

  while(true) {
    var elmts = node.getElementsByTagName(tagName);

    if(elmts.length == 0)
      return;

		var toggle = document.createElement(tagName + '-toggle');
		var content = document.createElement(tagName + '-content');
    var elmt = elmts[0];

		if(elmt.getAttribute('unwrap') !== null)
			active = false;

		toggle.innerHTML = elmt.getAttribute('title');
		content.innerHTML = elmt.innerHTML;

		for(var j=0; j<elmt.attributes.length; j++) {
			toggle.setAttribute(elmt.attributes[j].name, elmt.attributes[j].value);
		}

		if(display) {
			toggle.onclick = function(toggle, content) {
				return function(ev) {
					dotoggle(content, 'active', 'inactive');
					dotoggle(toggle, 'on', 'off');
					ev.stopPropagation();
				}
			}(toggle, content);
		}

		content.classList.add(active ? 'active' : 'inactive');
		toggle.classList.add(active ? 'on' : 'off');

    newElmt = document.createElement(tagName + '-block');
		newElmt.appendChild(toggle);
		newElmt.appendChild(content);
		newElmt.classList.add(display ? 'active' : 'inactive');

    elmt.parentNode.replaceChild(newElmt, elmt);
	}
}

function createToggles(node, wrap, preprint, displaySoluce) {
	createToggle(node, 'chapitre', true, true);
	createToggle(node, 'exercice', true, true);
	createToggle(node, 'question', true, true);
	createToggle(node, 'solution', displaySoluce, wrap);
	createToggle(node, 'tip', true, preprint);
	createToggle(node, 'details', true, preprint);
}

function proceedPage(node, wrap, displaySoluce, preprint) {
	doSchedule(node);
	createToggles(node, wrap, preprint, displaySoluce);
	doCodes(node);
}

function proceedAllInOne(wrap, displaySoluce, preprint) {
	var all = document.getElementsByTagName('all-in-one');

	for(var i=0; i<all.length; i++) {
		if(!all[i].getAttribute('done')) {
			all[i].setAttribute('done', 'true');
			var cl = all[i].getAttribute('all-class');
			cl = !cl ? "all-in-one" : cl;

			var navs = document.getElementsByTagName('nav');
	
			for(var j=0; j<navs.length; j++) {
				var elmts = navs[j].getElementsByClassName(cl);

				for(var k=0; k<elmts.length; k++) {
					if(elmts[k].getAttribute('ignore') == null) {

						var xhttp = newHTTPRequest();
						var node = document.createElement('span');

						all[i].appendChild(node);

						xhttp.open('GET', elmts[k].href, true);
						xhttp.onload = function(xhttp, node) {
							return function(e) {
								var parser = new DOMParser();
								var doc = parser.parseFromString(xhttp.responseText, "text/html");
								var content = doc.getElementById('main-content');

								if(content)
									node.innerHTML = content.innerHTML;

								proceedPage(node, wrap, displaySoluce, preprint);
							}
						}(xhttp, node);

						try {
							xhttp.send();
						} catch(err) {
							if(document.location.protocol == 'file:') {
								node.innerHTML = "La politique de sécurité du navigateur est probablement trop sévère pour voir une page sans un serveur web. Essayez les solutions proposées <a href='https://github.com/mrdoob/three.js/wiki/How-to-run-things-locally'>ici</a>.";
							} else {
								node.innerHTML = "Erreur: " + e.message;
							}
						}
					}
				}
			}
		}
	}
}

function selectics(selectObject) {
	var boxes = document.getElementsByTagName('icsbox');
  var url = document.location.origin + 
            document.location.pathname + 
            "?ics=" + encodeURIComponent(selectObject.value) +
            "&unique=" + Date.now();

	for(var i=0; i<boxes.length; ++i) {
    boxes[i].style.display = 'inline-block';
    boxes[i].innerHTML = '<a href="' + url + '">' + url + '<a>';
  }
}

function bootstrap() {
	var qs = (function(a) {
		if (a == '') return {};
		var b = {};
		for (var i = 0; i < a.length; ++i) {
			var p=a[i].split('=', 2);
			if (p.length == 1)
				b[p[0]] = '';
			else
				b[p[0]] = decodeURIComponent(p[1].replace(/\+/g, ' '));
		}
		return b;
	})(window.location.search.substr(1).split('&'));

	var wrap = 'wrap' in qs && qs['wrap'] !== 'false';
  var ignoreWeek = 'ignoreWeek' in qs && qs['ignoreWeek'] !== false;
	var displaySoluce = (typeof forceSoluce !== 'undefined' && forceSoluce) || ('soluce' in qs && qs['soluce'] !== 'false');
	var preprint = 'preprint' in qs && qs['preprint'] !== 'false';
	var root = document.getElementsByTagName('body')[0];

	if(preprint)
		root.setAttribute('preprint', true);

	doWeeks(root, ignoreWeek);
	proceedPage(root, wrap, displaySoluce, preprint);
	doNavs(root);

	proceedAllInOne(wrap, displaySoluce, preprint);
}

document.body.onload = bootstrap;
