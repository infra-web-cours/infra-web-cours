#! /bin/bash

if [ -z "$1" ]; then
    root=~
else
    root="$1"
fi

port=8080

echo "----------------------------------------------------"
echo "Starting the server with root at '$root'"
echo "You just have to connect to localhost:$port"
echo "----------------------------------------------------"
php -S "[0::0]":$port -t "$root" $(dirname $0)/router.php

