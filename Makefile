
.PHONY: all tidy
.SECONDARY: 
.SUFFIXES:

all:

tidy:
	find . \( -iname "*~" -o -iname "\#*" \) -exec rm {} \;
