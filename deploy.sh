#! /bin/bash

if [ -z "$1" ]; then
		echo "Please, specify a user name"
		exit 1
else
		USER="$1"
fi

make tidy

#HOST=srvp1
HOST=www-inf.telecom-sudparis.eu
SRC=$(dirname $0)/
DEST=/var/www/html/cours/infra-web-cours/

rsync --rsh=ssh -arv --chmod=g+rw,Dg+x -O --delete-excluded --exclude='.git*' --exclude='*~' --exclude=".metadata" --exclude=".recommenders/" --exclude "bin" --exclude ".control" --exclude "javafx/*" $SRC $USER@$HOST:$DEST

./wp/$(basename $0)

ssh $USER@$HOST "chgrp -R inf-inf $DEST/*"

