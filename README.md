# Introduction

This project contains a toolkit to quickly develop web pages for our courses.

You can find the documentation at [https://www-inf.telecom-sudparis.eu/COURS/infra-web-cours/example/](https://www-inf.telecom-sudparis.eu/COURS/infra-web-cours/example/)